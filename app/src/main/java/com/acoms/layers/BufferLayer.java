package com.acoms.layers;

import android.util.Log;

import com.acoms.model.BasicBufferSize;
import com.acoms.model.BufferSize;
import com.acoms.model.Constants;
import com.acoms.model.MultiGeoPoint;
import com.acoms.utils.MathUtils;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.math.MathUtil;

import org.greenrobot.eventbus.EventBus;
import org.jeo.data.Query;
import org.jeo.data.mem.MemVector;
import org.jeo.data.mem.MemWorkspace;
import org.jeo.feature.Feature;
import org.jeo.feature.Features;
import org.jeo.feature.Schema;
import org.jeo.feature.SchemaBuilder;
import org.jeo.geom.CoordinatePath;
import org.jeo.geom.Geom;
import org.jeo.geom.GeomBuilder;
import org.jeo.map.CartoCSS;
import org.jeo.map.RGB;
import org.jeo.map.Rule;
import org.jeo.map.RuleList;
import org.jeo.map.Style;
import org.oscim.core.GeoPoint;
import org.oscim.jeo.JeoUtils;
import org.oscim.layers.JtsLayer;
import org.oscim.map.Map;
import org.oscim.renderer.bucket.LineBucket;
import org.oscim.renderer.bucket.MeshBucket;
import org.oscim.renderer.bucket.PolygonBucket;
import org.oscim.theme.styles.AreaStyle;
import org.oscim.theme.styles.LineStyle;
import org.oscim.utils.geom.SimplifyDP;
import org.oscim.utils.geom.SimplifyVW;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import timber.log.Timber;

/**
 * Created by A on 5/18/2016.
 */
public class BufferLayer extends JtsLayer {

    private static final int FEATURE_ID = 99;
    private final LineStyle lineStyle;
    private final AreaStyle areaStyle;

    private MemVector mData;

    protected double mDropPointDistance = 0.01;
    static final boolean dbg = true;
    //private GeomBuilder mGb;
    private double bufferDegrees;

    SimplifyDP mSimpDP = new SimplifyDP();
    SimplifyVW mSimpVW = new SimplifyVW();
    //private List<MultiGeoPoint> mPointList;
    private GeomBuilder geomBuilder;
    HashMap<Integer, List<GeoPoint>> pointMap;

    public BufferLayer(Map map, LineStyle lineStyle, AreaStyle areaStyle, double buffer) {
        super(map);

        initSchema();
        mRenderer = new Renderer();
        this.bufferDegrees = MathUtils.MetersToDecimalDegrees(buffer);
        this.lineStyle = lineStyle;
        this.areaStyle = areaStyle;

        this.geomBuilder = new GeomBuilder(4326);
        pointMap = new HashMap();
    }

    public void clearBuffer(){
        Feature f = findFeature(FEATURE_ID);
        if(f!=null)
            mData.remove(f);
    }

    private void initSchema(){

        @SuppressWarnings("resource")
        MemWorkspace mem = new MemWorkspace();
        Schema schema = new SchemaBuilder("BufferLayer")
                .field("geometry", com.vividsolutions.jts.geom.Geometry.class)
                .field("id", Integer.class)
                .schema();
        try {
            mData = mem.create(schema);
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            Timber.e(e, "BufferLayer error");
//            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Timber.e(e, "BufferLayer error");
//            return null;
        }
    }

    public double addLinePoint(final MultiGeoPoint point, final boolean new_point){

        if( point == null ) return 0.0f;

        // pasākums ir smags, tāpēc jaunā thread
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final boolean add_to_buffer = new_point;

                addMultiGeoPoint(point);

                Geometry line = createMultiline();
                if( line != null ) {

                    Geometry geom = line.buffer(bufferDegrees);
                    // TODO šitajam vajag union?

//                boolean bLine = createLine(geomBuilder);
//                Geometry geom = createBuffer(geomBuilder, bLine);
//
                    //double buferArea = geom.getArea()*10/Constants.BUFFER_SIZE_TO_MAP_SIZE;
                    double buferArea = MathUtils.DecimalDegreesToMetersArea(geom.getArea());

                    Feature f = findFeature(FEATURE_ID);
                    if (f != null)
                        f.put(geom);
                    else {
                        f = Features.create(null, mData.schema(), geom, FEATURE_ID);
                        mData.add(f);
                    }
                    mMap.updateMap(true);
                    if (add_to_buffer) {

                        String wkt = geom.toString();
                        EventBus.getDefault().post(new BufferSize(buferArea, wkt));
                    }
                }
            }
        };
        new Thread(runnable).start();

        return 0.0f;
    }

    public double addLineBuffer(final List<MultiGeoPoint> pointList, final boolean add_to_buffer){

        if( pointList == null || pointList.size() < 1) return 0.0f;
        pointMap =  new HashMap<>();

        // pasākums ir smags, tāpēc jaunā thread
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                for (MultiGeoPoint mp : pointList) {
                    addMultiGeoPoint(mp);
                }

                Geometry line = createMultiline( );
                if( line != null ) {

                    Geometry geom = line.buffer(bufferDegrees);
                    // TODO šitajam vajag union?

//                boolean bLine = createLine(geomBuilder);
//                Geometry geom = createBuffer(geomBuilder, bLine);
                    //double buferArea = geom.getArea() * 10 / Constants.BUFFER_SIZE_TO_MAP_SIZE;
                    double buferArea = MathUtils.DecimalDegreesToMetersArea(geom.getArea());

                    Feature f = findFeature(FEATURE_ID);
                    if (f != null)
                        f.put(geom);
                    else {
                        f = Features.create(null, mData.schema(), geom, FEATURE_ID);
                        mData.add(f);
                    }
                    mMap.updateMap(true);
                    if (add_to_buffer) {

                        String wkt = geom.toString();
                        EventBus.getDefault().post(new BufferSize(buferArea, wkt));
                    }
                }
            }
        };
        new Thread(runnable).start();

        return 0.0f;
    }

    public void addMultiLineBuffer(final List<List<GeoPoint>> previousPoints) {

        final List<List<GeoPoint>> prevPoints = previousPoints;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                double buferArea = 0.0;

                for (int i = 0; i < prevPoints.size(); i++) {

                    GeomBuilder builder = geomBuilder;//new GeomBuilder(4326);
                    List<GeoPoint> line = prevPoints.get(i);
                    for (int j = 0; j < line.size(); j++) {
                        builder.point(line.get(j).getLongitude(), line.get(j).getLongitude());
                    }

                    Geometry geom = builder.toLineString().buffer(bufferDegrees);
                    buferArea += geom.getArea();
                    Feature f = Features.create(null, mData.schema(), geom, i);
                    mData.add(f);
                }

                //mMap.updateMap(false);
                EventBus.getDefault().post(new BasicBufferSize(buferArea));
            }
        };
        new Thread(runnable).start();
    }

    private void addMultiGeoPoint(MultiGeoPoint point){

        int part = point.getPart();
        if(pointMap == null ) pointMap = new HashMap<>();
        if (!pointMap.containsKey(part)) {

            List<GeoPoint> list = new ArrayList<>();
            list.add(point.getPoint());
            pointMap.put(part, list);
        } else {
            pointMap.get(part).add(point.getPoint());
        }

    }

    private Geometry createMultiline() {

        //GeometryFactory geomFactory = geomBuilder.get().getFactory();
        Geometry geom = null;

        if(pointMap != null && pointMap.size() > 0){

            List<LineString> lines = new ArrayList<>();
            for (HashMap.Entry<Integer, List<GeoPoint>> trackPolygon : pointMap.entrySet()) {

                List<GeoPoint> lp = trackPolygon.getValue();
                Coordinate[] coords = new Coordinate[lp.size()];
                if(coords.length > 1 ) {
                    int i = 0;
                    for( GeoPoint p : lp) {
                        //coords[i++] = new Coordinate(p.getLongitude(),p.getLatitude());
                        geomBuilder.point(p.getLongitude(),p.getLatitude());
                    }

                    //LineString line = geomFactory.createLineString(coords);
                    LineString line = geomBuilder.toLineString();
                    lines.add(line);
                }

            }

            GeometryFactory geomFactory = new GeometryFactory();
            if(lines.size() > 0 ) {

                LineString[] arrLines = new LineString[lines.size()];
                arrLines = lines.toArray(arrLines);
                // TODO SRID??
                geom = geomFactory.createMultiLineString(arrLines);
                //geom = geomBuilder.toMultiLineString();
            }
        }

        return geom;
    }

//    private boolean createLine(GeomBuilder gb ){
//
//        if(mPointList == null || mPointList.size() < 1) return false;
//
//        for (int i = 0; i < mPointList.size(); i++) {
//            gb.point(mPointList.get(i).getPoint().getLongitude(), mPointList.get(i).getPoint().getLatitude());
//        }
//        return (mPointList.size()>1);
//    }
//
//    private Geometry createBuffer(GeomBuilder gb, boolean bLine){
//
//        Geometry geom;
//        if(bLine)
//            geom = gb.toLineString().buffer(buffer);
//        else
//            geom = gb.toPoint().buffer(buffer);
//        return geom;
//    }

    private Feature findFeature(int lineNum){

        Query q = new Query().filter("BufferLayer.id = " + String.valueOf(lineNum));
        try {
            for (Feature f : mData.cursor(q)) {
                return f;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e, "findFeature error");
        }
        return null;
    }

    @Override
    protected void processFeatures(Task t, Envelope b) {
        if (mDropPointDistance > 0) {
			/* reduce lines points min distance */
            mMinX = ((b.getMaxX() - b.getMinX()) / mMap.getWidth());
            mMinY = ((b.getMaxY() - b.getMinY()) / mMap.getHeight());
            mMinX *= mDropPointDistance;
            mMinY *= mDropPointDistance;
        }

        try {
            Query q = new Query().bounds(b);

            for (Feature f : mData.cursor(q)) {

                Geometry g = f.geometry();
                if (g == null)
                    continue;

                switch (Geom.Type.from(g)) {
                    case POINT:
                        addPoint(t, f, g);
                        break;
                    case MULTIPOINT:
                        for (int i = 0, n = g.getNumGeometries(); i < n; i++)
                            addPoint(t, f, g.getGeometryN(i));
                        break;
                    case LINESTRING:
                        addLine(t, f, g);
                        break;
                    case MULTILINESTRING:
                        for (int i = 0, n = g.getNumGeometries(); i < n; i++)
                            addLine(t, f, g.getGeometryN(i));
                        break;
                    case POLYGON:
                        addPolygon(t, f, g);
                        break;
                    case MULTIPOLYGON:
                        for (int i = 0, n = g.getNumGeometries(); i < n; i++)
                            addPolygon(t, f, g.getGeometryN(i));
                        break;
                    default:
                        break;
                }
            }
        } catch (IOException e) {
            log.error("Error querying layer " + mData.getName() + e);
        }
    }

    protected void addLine(Task t, Feature f, Geometry g) {

        if (((LineString) g).isClosed()) {
            addPolygon(t, f, g);
            return;
        }

        LineBucket ll = t.buckets.getLineBucket(2);
        if (ll.line == null) {
            ll.line = lineStyle;
            ll.setDropDistance(0.5f);
        }
        addLine(t, g, ll);
    }

    protected void addPolygon(Task t, Feature f, Geometry g) {

        LineBucket ll = t.buckets.getLineBucket(1);

        if (ll.line == null) {
            ll.line = lineStyle;
            ll.setDropDistance(0.5f);
        }

        MeshBucket mesh = null;
        if(areaStyle!=null) { // krāsojam?
            mesh = t.buckets.getMeshBucket(0);
            if (mesh.area == null)
                mesh.area =areaStyle;
        }
        //
        addPolygon(t, g, mesh, ll);
    }

    protected void addPolygon(Task t, Geometry g, MeshBucket mesh, LineBucket ll){

        mGeom.clear();
        mGeom.startPolygon();

        CoordinatePath p = CoordinatePath.create(g);
        if (mMinX > 0 || mMinY > 0)
            p.generalize(mMinX, mMinY);

        if (transformPath(t.position, mGeom, p) < 3)
            return;

        if (!mClipper.clip(mGeom))
            return;

        mSimpVW.simplify(mGeom, 0.1f);
        mSimpDP.simplify(mGeom, 0.5f);

        ll.addLine(mGeom);

        if(mesh != null ) {
            mesh.addConvexMesh(mGeom);
            //mesh.addMesh(mGeom);
        }
    }

    protected void addPoint(Task t, Feature f,  Geometry g) {

    }

}
