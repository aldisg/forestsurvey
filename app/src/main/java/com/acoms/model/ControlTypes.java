package com.acoms.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldis Grauze on 16.12.2015.
 */
public class ControlTypes {

    @SerializedName("ID")
    private int id;
    @SerializedName("Nosaukums")
    private String name;
    @SerializedName("Aktivs")
    private int active;

    public ControlTypes(int id, String name, int active ) {
        this.id = id;
        this.name = name;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getActive() {
        return active;
    }

    public boolean isActive() {
        return (active == 1);
    }
}
