package com.acoms.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aldis Grauze on 15.18.12.
 */
public class DangerousSpecie {

    private boolean isSelected;

    @SerializedName("ID")
    public int specieId;

    @SerializedName("Nosaukums")
    public String name;

    @SerializedName("Aktivs")
    public int active;

//    @SerializedName("Faili")
//    public HelpInfo[] faili;

    public DangerousSpecie(int specieId, String name) {
        this.isSelected = false;
        this.specieId = specieId;
        this.name = name;
    }

    public int getId() {
        return specieId;
    }

    public void setSpecieId(int specieId) {
        this.specieId = specieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isActive() {
        return (active==1);
    }
}
