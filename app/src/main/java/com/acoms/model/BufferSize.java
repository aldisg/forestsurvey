package com.acoms.model;

/**
 * Created by aldisgrauze on 14.08.2017.
 */

public class BufferSize {

    private double area;
    private String wkt;

    public BufferSize(double area, String wkt) {
        this.area = area;
        this.wkt = wkt;
    }

    public String getAreaString() {
        return String.format("%.3f ha",area);
    }

    public double getArea(){
        return area;
    }

    public String getWkt(){
        return wkt;
    }

}
