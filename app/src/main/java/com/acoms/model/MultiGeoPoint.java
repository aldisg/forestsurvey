package com.acoms.model;

import org.oscim.core.GeoPoint;

/**
 * Created by aldisgrauze on 09.08.2017.
 */

public class MultiGeoPoint {

    private GeoPoint point;
    private int part;

    public MultiGeoPoint(double lat, double lng, int part) {
        this.point = new GeoPoint(lat, lng);
        this.part = part;
    }

    public GeoPoint getPoint() {
        return this.point;
    }

    public int getPart() {
        return part;
    }
}
