package com.acoms.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aldisgrauze on 02.08.2017.
 */

public class ControlPointPicture implements Parcelable{

    private int ID = -1;

    public ControlPointPicture(int controlID, String fullPath, Bitmap bitmap) {
        ControlID = controlID;
        this.fullPath = fullPath;
        this.previewPic = bitmap;
    }
    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    private int ControlID;
    private String fullPath;
    private Bitmap previewPic;

    public String getName() {
        return fullPath.substring(fullPath.lastIndexOf("/")+1);
    }

    public Bitmap getBitmap() {
        return previewPic;
    }

    // Parcelable stufs ---------------------------------------------

    public static final Creator<ControlPointPicture> CREATOR = new Creator<ControlPointPicture>() {
        @Override
        public ControlPointPicture createFromParcel(Parcel in) {
            return new ControlPointPicture(in);
        }

        @Override
        public ControlPointPicture[] newArray(int size) {
            return new ControlPointPicture[size];
        }
    };


    protected ControlPointPicture(Parcel in) {
        ID = in.readInt();
        ControlID = in.readInt();
        fullPath = in.readString();
        previewPic = null;//in.readParcelable(Bitmap.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(ID);
        dest.writeInt(ControlID);
        dest.writeString(fullPath);
        //dest.writeParcelable(previewPic, flags);
    }
}
