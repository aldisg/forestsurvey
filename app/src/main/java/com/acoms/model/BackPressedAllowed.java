package com.acoms.model;

/**
 * Created by aldisgrauze on 03.08.2017.
 */

public class BackPressedAllowed {

    private boolean allowed;
    public BackPressedAllowed(boolean yes){
        this.allowed = yes;
    }

    public boolean isAllowed() {
        return allowed;
    }
}
