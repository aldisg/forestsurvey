package com.acoms.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by A on 3/27/2016.
 */
public class Samples {

    @SerializedName("ID")
    private int id;

    @SerializedName("Nosaukums")
    private String name;

    @SerializedName("Aktivs")
    private int active;

    public Samples(int id, String name, int active) {
        this.id = id;
        this.name = name;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public int getActive() {
        return active;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isActive() {
        return (active==1);
    }
}
