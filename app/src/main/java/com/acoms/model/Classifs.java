package com.acoms.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aldis Grauze on 15.16.12.
 */
public class Classifs {

    @SerializedName("ParbaudesVeidi")
    private List<ControlTypes> controlList;

    @SerializedName("PetamieAugi")
    private List<Specie> speciesList;

    @SerializedName("KaitigieOrganismi")
    private List<DangerousSpecie> dangerousSpeciesList;

    @SerializedName("Mervienibas")
    private List<Units> unitsList;

    @SerializedName("ParaugaVeidi")
    private List<Samples> samplesList;

    public List<Samples> getSamplesList() {
        return samplesList;
    }

    public List<Units> getUnitsList() {
        return unitsList;
    }

    public List<ControlTypes> getControlList() {
        return controlList;
    }

    public List<Specie> getSpeciesList() {
        return speciesList;
    }

    public List<DangerousSpecie> getDangerousSpeciesList() {
        return dangerousSpeciesList;
    }
}
