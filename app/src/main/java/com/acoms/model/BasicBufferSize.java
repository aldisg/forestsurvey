package com.acoms.model;

/**
 * Created by aldisgrauze on 16.08.2017.
 */

public class BasicBufferSize {

    private double area;
    public BasicBufferSize(double area) {
        this.area = area;
    }

    public String getAreaString() {
        return String.format("%.2f ha",area);
    }

    public double getArea(){
        return area;
    }
}
