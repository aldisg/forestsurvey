package com.acoms.model;

import android.database.Cursor;
import android.location.Location;

//import com.acoms.koanketa.RunDatabaseHelper;

import com.acoms.database.TrackDatabaseHelper;
import com.vividsolutions.jts.geom.MultiPoint;

import org.oscim.core.GeoPoint;
import org.oscim.layers.marker.MarkerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acoms on 26.04.16.
 */


public class AssignmentTrack {

    boolean state;
    private List<MultiGeoPoint> mPointList;
    private MultiGeoPoint mStartPoint, mEndPoint;

//    private double mTrackArea;
//
//    public void setArea(double area){
//        mTrackArea = area;
//    }
//
//    public double getTrackArea(){
//        return mTrackArea;
//    }

    public AssignmentTrack(TrackDatabaseHelper.LocationCursor cursor){

        mPointList = new ArrayList<>();

        if ((cursor != null) && (cursor.getCount() > 0)) {

            // iterate over the locations - if any
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                Location loc = cursor.getLocation();
                mPointList.add(new MultiGeoPoint(loc.getLatitude(), loc.getLongitude(), Math.round(loc.getBearing())));
                cursor.moveToNext();
            }

            if (mPointList.size() > 0) {
                mStartPoint = mPointList.get(0);
                mEndPoint = mStartPoint;
            }
            mEndPoint = mPointList.get(mPointList.size() - 1);
        }
    }

    public List<MultiGeoPoint> getTrackPoints() {
        return mPointList;
    }

    public void addTrackPoint(MultiGeoPoint geoPoint) {

        mPointList.add(geoPoint);
        if (mPointList.size() > 0)
            mStartPoint = mPointList.get(0);
        mEndPoint = geoPoint;
    }

    public MultiGeoPoint getStartPoint() {
        return mStartPoint;
    }

    public MultiGeoPoint getStopPoint() {
        return mEndPoint;
    }

}
