package com.acoms.model;

/**
 * Created by aldisgrauze on 11.08.2017.
 */

public class MapReadyEvent {

    boolean ready;
    public MapReadyEvent(boolean ready) {
        this.ready = ready;
    }

    public boolean isReady() {
        return ready;
    }
}
