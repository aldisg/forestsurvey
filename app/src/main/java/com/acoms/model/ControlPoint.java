package com.acoms.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.acoms.ApplicationController;
import com.acoms.database.AssignmentDatabaseContract;
import com.acoms.database.ClassifsDatabaseContract;
import com.acoms.utils.StringIntArrayUtils;
import com.cocoahero.android.geojson.Feature;
import com.cocoahero.android.geojson.Point;

import org.json.JSONException;
import org.json.JSONObject;
import org.oscim.core.GeoPoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by acoms on 29.12.15.
 */
public class ControlPoint implements Parcelable {

    int id;
    int number;
    int mainSpecieCode;
    int sampleTypeCode;
    int units;
    float sampleAmount;

    float lat;
    float lng;

    String decision;
    DangerousSpecieSample[] dangerous;
    public ArrayList<DangerousSpecieSample> specieList;
    public ArrayList<ControlPointPicture> pictureList;

    public int getNumber() {
        //return number;
        return id;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getMainSpecieCode() {
        return mainSpecieCode;
    }

    public void setMainSpecieCode(int mainSpecieCode) {
        this.mainSpecieCode = mainSpecieCode;
    }

    public int getSampleTypeCode() {
        return sampleTypeCode;
    }

    public void setSampleTypeCode(int sampleTypeCode) {
        this.sampleTypeCode = sampleTypeCode;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public int getSelectedDangerousCount() {
        int iRet = 0;
        if( dangerous != null ) {
            for(int i = 0; i < dangerous.length; i++){
                if(dangerous[i] != null && dangerous[i].isSelected()) iRet++;
            }
        }
        return iRet;
    }

    public DangerousSpecieSample[] getDangerous() {
        return dangerous;
    }

    public void setDangerousArray() {
        if(specieList != null && specieList.size() > 0) {
            this.dangerous = new DangerousSpecieSample[specieList.size()];
            for ( int i = 0; i < specieList.size(); i++ ){
                this.dangerous[i] = specieList.get(i);
            }
        }
        else this.dangerous = null;
    }

    public ControlPoint(int id, int number, int mainSpecieCode, int sampleSpecieCode, float sampleAmount, int sampleUnits,  String decision, DangerousSpecieSample[] dangerous, float lat, float lng, ArrayList<ControlPointPicture> pictureList) {
        this.id = id;
        this.number = number;
        this.mainSpecieCode = mainSpecieCode;
        this.sampleTypeCode = sampleSpecieCode;
        this.sampleAmount = sampleAmount;
        this.decision = decision;
        this.dangerous = dangerous;
        this.units = sampleUnits;
        this.lat = lat;
        this.lng = lng;
        if(pictureList != null)
            this.pictureList = pictureList;
        else this.pictureList = new ArrayList<>();
    }

    public ControlPoint(Parcel in){
        this.id = in.readInt();
        this.number = in.readInt();
        this.mainSpecieCode = in.readInt();
        this.sampleTypeCode = in.readInt();
        this.sampleAmount = in.readFloat();
        this.units = in.readInt();
        this.decision = in.readString();
        this.lat = in.readFloat();
        this.lng = in.readFloat();
        this.dangerous = (DangerousSpecieSample[])in.createTypedArray(DangerousSpecieSample.CREATOR);
        this.pictureList = new ArrayList<>();
        in.readList(this.pictureList, ControlPointPicture.class.getClassLoader());
    }

    public static ControlPoint ControlPointFromCursor(Cursor cursor){

        // tukšo ierakstu kontrole
        int specie =  cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_SPECIE));
        if( specie == 0 ) return null;

        //int _id = -1;//
        int _id = cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_ID));
        int number =  cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID));

        int type =  cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_TYPE));
        int units =  cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_UNITS));
        float amount = cursor.getFloat(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_AMOUNT));
        String decision = cursor.getString(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_COMMENTS));
        String dangerous = cursor.getString(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_DANGEROUS_SPECIES));

        float lat = cursor.getFloat(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_LAT));
        float lng = cursor.getFloat(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_LNG));

        String pictures = cursor.getString(cursor.getColumnIndex(AssignmentDatabaseContract.ControlPointsTable.CONTROL_PICTURES));
        ArrayList<ControlPointPicture> picsList = new ArrayList<>();

        if( !TextUtils.isEmpty(pictures)) {
            String[] pics = pictures.split(",");
            for( int i=0; i<pics.length; i++)
            {
                String path = pics[i];
                if(!TextUtils.isEmpty(path)){
                    ControlPointPicture pic = new ControlPointPicture(_id, path, null);
                    picsList.add(pic);
                }
            }
        }


        String[] savedSelection = null;
        if( dangerous != null && !dangerous.isEmpty())
                savedSelection = dangerous.split(",");

        // -------------------------------------------------------------------------------------------------------
        DangerousSpecieSample[] ds = null;

        List<Specie> species = ApplicationController.classifs.getSpecies();
        Specie currentSpecie = Specie.find( specie, species);
        if( currentSpecie != null ) {
            int[] koCodes = currentSpecie.getKoCodes();

            if (koCodes != null && koCodes.length > 0) {

                List<DangerousSpecie> specieSamples = ApplicationController.classifs.getDangerousSpecies();
                ds = new DangerousSpecieSample[koCodes.length];
                for (int l = 0; l < koCodes.length; l++) {

                    String sampleName = DangerousSpecieSample.findName(koCodes[l], specieSamples);
                    if (sampleName != null) {
                        boolean selected = false;
                        if (savedSelection != null && StringIntArrayUtils.findIntInStringArray(koCodes[l], savedSelection))
                            selected = true;
                        ds[l] = new DangerousSpecieSample(koCodes[l], sampleName, selected);
                    }
                }
                // --------------------------------------------------------------------------------------------------------
            }
        }



        return new ControlPoint(_id, number, specie, type, amount, units, decision, ds, lat, lng, picsList);
    }

    public ContentValues getContentValues() {

        ContentValues values = new ContentValues();
        //values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_ID, id);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID, number);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_SPECIE, mainSpecieCode);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_TYPE, sampleTypeCode);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_UNITS, units);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_AMOUNT, sampleAmount);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_COMMENTS, decision);

        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_LAT, lat);
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_LNG, lng);

        String loopDelim = "";
        StringBuilder sb = new StringBuilder();
        if(dangerous != null) {
            for (int i = 0; i < dangerous.length; i++) {
                DangerousSpecieSample ds = dangerous[i];
                if (ds != null && ds.isSelected()) {
                    sb.append(loopDelim);
                    sb.append(ds.getSpecieID());
                    loopDelim = ",";
                }
            }
        }
        String dangerousString = sb.toString();
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_DANGEROUS_SPECIES, dangerousString);

        List<String> pList = new ArrayList<>();
        for(ControlPointPicture p: pictureList) {
            pList.add(p.getFullPath());
        }
        values.put(AssignmentDatabaseContract.ControlPointsTable.CONTROL_PICTURES, TextUtils.join(",", pList));

        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(number);
        out.writeInt(mainSpecieCode);
        out.writeInt(sampleTypeCode);
        out.writeFloat(sampleAmount);
        out.writeInt(units);
        out.writeString(decision);
        out.writeFloat(lat);
        out.writeFloat(lng);

        out.writeTypedArray(dangerous, flags);
        out.writeList(pictureList);
    }

    public static final Parcelable.Creator CREATOR =
        new Parcelable.Creator() {
            public ControlPoint createFromParcel(Parcel in) {
                return new ControlPoint(in);
            }

            public ControlPoint[] newArray(int size) {
                return new ControlPoint[size];
            }
        };

    public String getSampleSpecieName() {

        List<Specie> species = ApplicationController.classifs.getSpecies();
        for (Specie s :
                species) {
            if(s.getId() == sampleTypeCode) return s.getName();
        }
        return "";
    }

    public String getMainSpecieName() {

        List<Specie> species = ApplicationController.classifs.getSpecies();
        for (Specie s :
                species) {
            if(s.getId() == mainSpecieCode) return s.getName();
        }
        return "";
    }

    public String getUnitName() {

        List<Units> units = ApplicationController.classifs.getUnits();
        for (Units u :
                units) {
            if(u.getId() == this.units) return u.getName();
        }
        return "";
    }

    public String getTypeCodeName() {

        List<Samples> samples = ApplicationController.classifs.getSamples();
        for (Samples s :
                samples) {
            if(s.getId() == this.sampleTypeCode) return s.getName();
        }
        return "";
    }

    public void setUnits(int id) {
        this.units = id;
    }

    public int getUnits()
    {
        return this.units;
    }

    public float getAmount() {
        return this.sampleAmount;
    }

    public void setAmount(float amount) {
        this.sampleAmount = amount;
    }

    public void setPosition(Location mLastLocation) {

        lat = (float) mLastLocation.getLatitude();
        lng = (float) mLastLocation.getLongitude();
    }

    public GeoPoint getPosition() {
        if(lat < 0.1 && lng < 0.1) return null;
        return new GeoPoint(lat,lng);
    }

    public String getGeoJSON() {

        // Create geometry
        Point point = new Point(lat, lng);

        // Create feature with geometry
        Feature feature = new Feature(point);

        // Set optional feature identifier
        feature.setIdentifier("Kontroles punkts");

        // Set optional feature properties
        feature.setProperties(new JSONObject());

        // Convert to formatted JSONObject
        JSONObject geoJSON = null;
        try {
            geoJSON = feature.toJSON();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return geoJSON.toString();
    }

    public int GetID() {
        return this.id;
    }

    public ArrayList<ControlPointPicture> getPictures() {
        return pictureList;
    }
}
