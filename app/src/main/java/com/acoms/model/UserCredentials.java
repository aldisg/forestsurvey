package com.acoms.model;

/**
 * Created by A on 4/12/2016.
 */
public class UserCredentials {

    public UserCredentials(){
        this.code = "";
        this.token = "";
    }

    public UserCredentials(String code, String token) {
        this.code = code;
        this.token = token;
    }

    public String code;
    public String token;
}
