package com.acoms.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aldis Grauze on 16.12.2015.
 */
public class Specie {

    @SerializedName("ID")
    public int id;

    @SerializedName("Nosaukums")
    public String name;

    @SerializedName("Aktivs")
    private int active;

    @SerializedName("KaitigieOrganismi")
    public List<SpeciesControl> speciesControlList;

    private int[] koCodes;

    public int[] getKoCodes() {
        return koCodes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Specie(int id, String name, int[] codes) {
        this.id = id;
        this.name = name;
        this.koCodes = codes;
    }

    // !! tas ir spineradateram
    @Override
    public String toString() {
        return name;
    }

    public static Specie find(int specie, List<Specie> species) {

        if(species == null) return null;
        for(Specie s :species){
            if(specie == s.getId()) return s;
        }
        return null;
    }

    public boolean isActive() {
        return (active==1);
    }
}
