package com.acoms.model;

import org.oscim.core.GeoPoint;

/**
 * Created by aldisgrauze on 12.08.2017.
 */

public class NewControlPoint {

    private final GeoPoint point;
    public NewControlPoint(GeoPoint point ) {
        this.point = point;
    }

    public GeoPoint getPoint() {
        return point;
    }
}
