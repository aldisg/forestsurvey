package com.acoms.model;

/**
 * Created by aldisgrauze on 12.08.2016.
 */

public class UserRest {
    public String name;
    public String objectId;
    public String userStatus;
    public String email;
    public String token;
}
