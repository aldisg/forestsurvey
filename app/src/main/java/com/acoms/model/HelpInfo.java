package com.acoms.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by aldisgrauze on 12.09.2017.
 */

public class HelpInfo {

    @SerializedName("Nosaukums")
    public String nosaukums;

    @SerializedName("Tips")
    public String tips;
}
