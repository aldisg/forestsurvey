package com.acoms.model;

import android.location.Location;
import android.util.Log;

import com.acoms.koanketa.GeomUtils;

/**
 * Created by aldisgrauze on 14.05.2016.
 */
public class Track {

    private long id;
    private int lap;

    private Location prevLocation;

    private int mArea;
    private int mDistance;

    private final int MAX_DIFFERENCE = 500;

    public Track(long id ){
        this.id = id;
        this.lap = 0;
    }

    public boolean isValidLocation(Location loc) {

        if(prevLocation == null ) prevLocation = loc;
        else {
            // TODO pagaidām rakstam visu sviestu bez filtrācijas
            //float prevAccuracy = prevLocation.getAccuracy();
//            float currAccuracy = loc.getAccuracy();
//            double distM = GeomUtils.getDist(prevLocation.getLatitude(), prevLocation.getLongitude(),loc.getLatitude(), loc.getLongitude())*1000;
//            Timber.i("accuracy  " + Float.toString(currAccuracy) + " dist " + Double.toString(distM ));
//
////            if(distM>currAccuracy) {
//            if( distM < MAX_DIFFERENCE ) {
//                Timber.i(" dist passed: " + Double.toString(distM ));
//                prevLocation = loc;
//                mDistance += distM;
//                recalcArea(distM);
//                return true;
//            }
        }
        return  true;
    }

    private void recalcArea(double dist){
        // TODO pagaidām primitīva laukuma aprēķināšana - tiek pieņemts, ka useris iet taisni
//        double res = 0.0f;
//        res = dist * 2.0f * BUFFER_SIZE;
//        mArea += res;
    }

    public void addLap() {
        lap++;
    }

    public int getLap() {
        return lap;
    }

}
