package com.acoms.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.text.TextUtils;
import android.util.JsonWriter;

import com.acoms.GeoJsonUtils.GeoJsonWriter;
import com.acoms.database.AssignmentDatabaseContract;
//import com.acoms.database.AssignmentDatabaseHelper;
import com.acoms.database.DatabaseHelper;
import com.acoms.database.TrackDatabaseHelper;
//import com.acoms.koanketa.RunDatabaseHelper;
import com.acoms.koanketa.TrackManager;
import com.acoms.utils.StringDateUtils;
import com.cocoahero.android.geojson.Feature;
import com.cocoahero.android.geojson.GeoJSON;
import com.cocoahero.android.geojson.GeoJSONObject;
import com.cocoahero.android.geojson.Geometry;
import com.cocoahero.android.geojson.LineString;
import com.cocoahero.android.geojson.MultiPolygon;
import com.cocoahero.android.geojson.Point;
import com.cocoahero.android.geojson.Polygon;
import com.cocoahero.android.geojson.Position;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.operation.overlay.PolygonBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import timber.log.Timber;

/**
 * Created by A on 3/13/2016.
 */
public class Assignment implements Serializable {
    private static final String TAG = "vaad Assignment";

//    <are:DarbaUzdevums>
//        <are:ID>?</are:ID>
//        <are:Nosaukums>?</are:Nosaukums>
//        <are:Aktivs>?</are:Aktivs>
//        <are:Stavoklis>0</are:Stavoklis>
//        <are:DarbaTips>?</are:DarbaTips>
//        <are:AtrasanasVieta>?</are:AtrasanasVieta>
//        <are:Apraksts>?</are:Apraksts>
//        <are:Datums>2016-04-03T00:00:00</are:Datums>
//        <are:Termins>2016-04-03T00:00:00</are:Termins>
//        <are:Uzdevejs>?</are:Uzdevejs>
//        <are:Izpilditaji>?</are:Izpilditaji>
//    </are:DarbaUzdevums>

    private int _id;
    private String id;

    private String taskNr;
    //private int trackLength;
    private String controlType = "Meža pārbaude";
    private double trackArea;
    private int currentPart;

//    public String getGeometryString() {
//        return geometryString;
//    }
//
//    public void setGeometryString(String geomString) {
//        this.geometryString = geomString;
//    }
//    private String geometryString;

    public String getName() {
        return name;
    }

    public Date getDateTo() {
        return dateTo;
    }

    private String name;     // Nosaukums;
//    private int active;
    private int state;       // Stavoklis;
    private int type;        // DarbaTips;
    private String geometry; // AtrasanasVieta - GeoJson;
    private String description; // Apraksts
    private Date date;       // Datums
    private Date dateTo;     // Termins
    private String assigner; // Uzdevejs
    private String executor; // Izpilditaji

    private List<ControlPoint> controlPoints;
    private List<MultiGeoPoint> trackPoints;

    // create fom
    public Assignment(){
    }

    public Assignment(SoapObject data){

        this.id = data.getPrimitivePropertyAsString("ID");

        String tmpString = data.getPrimitivePropertyAsString("Stavoklis");
        this.state = Integer.valueOf(tmpString);

//        tmpString = data.getPrimitivePropertyAsString("DarbaTips");
//        this.type = Integer.valueOf(tmpString);

        String geoJson = data.getPrimitivePropertyAsString("AtrasanasVieta");

        geoJson = geoJson.replace("var geojsonFeature = ","");
        geoJson = geoJson.replace(";","");
        this.geometry = geoJson;

        this.name = data.getPrimitivePropertyAsString("Nosaukums");
        this.description = data.getPrimitivePropertyAsString("Apraksts");

        String dateTo = data.getPrimitivePropertyAsString("Termins");
        this.dateTo = StringDateUtils.parseDateString(dateTo);

        String date = data.getPrimitivePropertyAsString("Datums");
        this.date = StringDateUtils.parseDateString(date);

        this.assigner = data.getPrimitivePropertyAsString("Uzdevejs");
        this.executor = data.getPrimitivePropertyAsString("Izpilditaji");

        this.controlPoints = new ArrayList<>();

        this.controlType = "Meža pārbaude";
    }

    public Assignment(int _id, String id, String name, int state, int type, String description, Date date, Date dateTo, String assigner, String executor, String geometry, String sequence, double area) {

        this._id = _id;
        this.id = id;
        this.name = name;
 //       this.active = active;
        this.state = state;
        this.type = type;
        this.description = description;
        this.date = date;
        this.dateTo = dateTo;
        this.assigner = assigner;
        this.executor = executor;
        this.geometry = geometry;
        this.controlPoints = new ArrayList<>();
        this.taskNr = sequence;
        this.controlType = "Meža pārbaude";
        this.trackArea = area;
        this.currentPart = -1;
    }

    // Fields -----------------------------------------------------------

    /**
     * Convenient method to get the objects data members in ContentValues object.
     * This will be useful for Content Provider operations,
     * which use ContentValues object to represent the data.
     *
     * @return
     */
    public ContentValues getContentValues() {

        ContentValues values = new ContentValues();
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID, _id);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR, id);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME, (String)name);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DESCR, description);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE, state);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ASSIGNER, assigner);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_EXECUTOR, executor);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TYPE, type);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATE, date.getTime());
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO, dateTo.getTime());
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK, geometry);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_SEQUENCE_NR, taskNr);
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_AREA, trackArea);
        return values;
    }

    public static Assignment createEmptyAssignment(int lastNr, String title, String descr, String inspectorId){

        // if survey is done without assignment - then id will be negative -1, -2, -3, ...
        String lastNrStr = String.valueOf(lastNr);
        return new Assignment(-1, "-" + lastNrStr, title + " " + lastNrStr, 0, 0, descr, new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()), "Inspektors", inspectorId, "", null, 0.0);
    }

    // Create a task object from a cursor
    public static Assignment fromCursor(Cursor curAssignments) {

        if(curAssignments.getCount() == 0) return null;

        int _id =  curAssignments.getInt(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID));
        String id = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR));
        String name = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME));
        int state =  curAssignments.getInt(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE));
        int type =  curAssignments.getInt(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TYPE));
        String description = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DESCR));
        String assigner = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ASSIGNER));
        String executor = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_EXECUTOR));
        String geojson = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK));
        long msecs = curAssignments.getLong(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATE));
        long msecsTo = curAssignments.getLong(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO));
        String sequenceNr = curAssignments.getString(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_SEQUENCE_NR));
        double area = curAssignments.getDouble(curAssignments.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_AREA));

        Assignment currTask = new Assignment(_id, id, name, state, type, description, new Date(msecs), new Date(msecsTo), assigner, executor, geojson, sequenceNr, area);

        // TODO nolasa controlpunktus un track




        return currTask;
    }

    public String getIdentifier(){
        return id;    
    }
    
    public static String getStatusString(int status){

//    0 - jauns
//    1 - atkārtots
//    2 - izpildē
//    3 - izpildīts
//    4 - pieņemts
//    10 - Anulēts

        String statuss = "Pievienots";
        switch(status){

            case AssignmentDatabaseContract.ASSIGNMENT_RECIEVED:
                statuss = "Jauns";
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_REASSIGNED:
                statuss = "Atkāŗtots";
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_IN_PROCESSING:
                statuss = "Izpildē";
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_DONE:
                statuss = "Izpildīts";
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_SYNCED:
                statuss = "Pieņemts";
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_CANCELED:
                statuss = "Anulēts";
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_CREATED:
                statuss = "Izveidots";
                break;

        }
        return statuss;
    }

    public int getPointNumber() {
        if(controlPoints == null ) return 0;
        return controlPoints.size();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Assignment assignment = (Assignment) o;
        return assignment.id.equals(id);

        //if (id != assignment.id) return false;
        //if (!name.equals(assignment.name)) return false;
        //return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result;// + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return name;// + " (" + dateTo + ")";
    }

    public void setStatus(int status) {
        this.state = status;
    }

    public int getStatus() {
        return state;
    }

    public String getTaskNr(){
        return taskNr;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setTaskNr(String taskNr) {
        this.taskNr = taskNr;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public GeoJSONObject getGeoJsonGeometry(){

        if( geometry == null || geometry.isEmpty() ) return null;
        try {
            GeoJSONObject geoJSON = GeoJSON.parse(geometry);
            return geoJSON;
        }
        catch (JSONException e) {
            //e.printStackTrace(); // tas nav GeoJSONs
        }
        return null;
    }

    public Location getTaskLocation() {

        // atgriež pēdējo apsekojuma punktu
        if(trackPoints != null && trackPoints.size() > 0) {
            MultiGeoPoint point = trackPoints.get(trackPoints.size()-1);
            if(point != null) {
                Location loc = new Location("");
                loc.setLatitude(point.getPoint().getLatitude());
                loc.setLongitude(point.getPoint().getLongitude());
                return  loc;
            }
        }
        else { // ja vēl nav neviena punkta tad atgriež nosūtījuma punktu

            GeoJSONObject geometry = getGeoJsonGeometry();
            if( geometry != null ) {
                String geomType = geometry.getType();

                // pagaidām ir tikai punkti
                if (geomType.equals("Feature")) {

                    Feature feature = (Feature) geometry;
                    Point point = (Point) feature.getGeometry();
                    Position pos = point.getPosition();
                    Location loc = new Location("");
                    loc.setLatitude(pos.getLatitude());
                    loc.setLongitude(pos.getLongitude());
                    return  loc;
                }
            }
        }
        return null;
    }

    public List<ControlPoint> getControlPointsMock(){

        // mock dati
        List<ControlPoint> cpList = new ArrayList<ControlPoint>();

//        cpList.add(new ControlPoint( 1, 103, 135, "Nav bīstams", "ok", new DangerousSpecieSample[2]));
//        cpList.add(new ControlPoint( 2, 98, 135, "Nav bīstams", "ok", new DangerousSpecieSample[0]));
//        cpList.add(new ControlPoint( 3, 103, 98, "Nav bīstams", "ok", new DangerousSpecieSample[1]));
//        cpList.add(new ControlPoint( 4, 135, 135, "Nav bīstams", "ok", new DangerousSpecieSample[5]));
//        cpList.add(new ControlPoint( 5, 110, 98, "Nav bīstams", "ok", new DangerousSpecieSample[3]));

        return cpList;
    }

    // control point functions --------------------------------------------
    public List<ControlPoint> getControlPoints(){
        return controlPoints;
    }

    public void addControlPoint(ControlPoint controlPoint) {
        controlPoints.add(controlPoint);
    }

    public void updateControlPoint(ControlPoint confirmedPoint) {
        for(int i = 0; i < controlPoints.size(); i++){
            if(controlPoints.get(i).getNumber() == confirmedPoint.getNumber()) {
                controlPoints.set(i,confirmedPoint);
                return;
            }
        }
    }

    public void deleteControlPoint(ControlPoint point) {
        for(int i = 0; i < controlPoints.size(); i++){
            if(controlPoints.get(i).getNumber() == point.getNumber()) {
                controlPoints.remove(i);
                return;
            }
        }
    }

    public int getId() {
        return _id;
    }

    public int saveData(Context context, Cursor cursor) {

        ContentValues cv = getContentValues();

        int iCps = 0;
        int parentId = -1;
        Uri uri = context.getContentResolver().insert(AssignmentDatabaseContract.CONTENT_URI, cv);
        parentId = Integer.valueOf(uri.getLastPathSegment());

        if(parentId != -1 ) {

            List<ControlPoint> cpList = this.getControlPoints();
            ContentValues[] cpcv = new ContentValues[cpList.size()];
            for (int i = 0; i < cpList.size(); i++) {
                cpList.get(i).setNumber(parentId);
                cpcv[i] = cpList.get(i).getContentValues();
            }
            Timber.i("ControlPoints DB insert id= " + String.valueOf(parentId) + " points count=" + String.valueOf(cpcv.length));
//            AssignmentDatabaseHelper dbHelper = AssignmentDatabaseHelper.getHelper(context);//new AssignmentDatabaseHelper(context);
//            iCps = dbHelper.insertControlPoints(parentId, cpcv);
            insertControlPoints(context,parentId, cpcv);
            // vajag CursorLoader refresh !!!!
            if(cursor != null)
                cursor.setNotificationUri(context.getContentResolver(), uri);
        }


        Timber.i("Save task: nr= " + String.valueOf(parentId) + " cp= " + String.valueOf(iCps) );
        return parentId;
    }

    public int setTaskStatus(Context context, int newStatus){

        ContentValues values = new ContentValues();
        values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE, newStatus);
        int rec = context.getContentResolver().update(AssignmentDatabaseContract.CONTENT_URI, values, AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + Integer.valueOf(getId()), null);
        return rec;
    }
    
    public void updateControlPoints(Cursor data) {

        List<ControlPoint> points = getControlPoints();
        Timber.i("ControlPoints getControlPoints " + String.valueOf(points.size()));

        int newPoints = 0;
        data.moveToFirst();
        while(data.moveToNext()) {
            points.add(ControlPoint.ControlPointFromCursor(data));
            newPoints++;
        }
        data.close();
        Timber.i("ControlPoints DB query record count: " + String.valueOf(newPoints));
    }

    public void setId(int id) {
        this._id = id;
    }

    // nodzes
    public void deleteTask(Context context) {

        try{

            int parentId = getId();
            int rec = context.getContentResolver().delete(AssignmentDatabaseContract.CONTENT_URI, AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + Integer.valueOf(parentId), null);
            int iCPoints = 0, iTrackPoints = 0;
            if(rec == 1) { // remove all related records
                iCPoints = deleteControlPoints(context, parentId);
                Timber.i("Punkti =" + iCPoints);

                TrackDatabaseHelper runDbHelper = new TrackDatabaseHelper(context);
                iTrackPoints = runDbHelper.deleteRun(parentId);
                Timber.i("TrackPunkti =" + iCPoints);
            }
            Timber.i("Dzēšana tasks= " + String.valueOf(rec) + " cp=" + String.valueOf(iCPoints) + " trackpoints=" + String.valueOf(iTrackPoints));
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public int insertTask(Context context){

        ContentValues cv = getContentValues();

        Uri uri = context.getContentResolver().insert(AssignmentDatabaseContract.CONTENT_URI, cv);
        int newId = Integer.valueOf(uri.getLastPathSegment());
        setId(newId);
        return newId;
    }

    public int getType() {
        return type;
    }

    public static Assignment fromFullCursor(Context context, Cursor data) {

        if(data == null || data.getCount() == 0) return null;

        data.moveToFirst();
             Assignment task = Assignment.fromCursor(data);

        do{
            ControlPoint cp = ControlPoint.ControlPointFromCursor(data);
            if(cp!=null) task.addControlPoint(cp);
        }while(data.moveToNext());
        //data.close();

        TrackDatabaseHelper.LocationCursor trackData = TrackManager.get(context).queryLocationsForRun(task.getId());
        //task.setTrackLength(trackData);
        task.storeTrackPoints(trackData);
        return task;
    }

    private void storeTrackPoints(TrackDatabaseHelper.LocationCursor data) {

        trackPoints = new ArrayList<>();
        int maxPart = -1;

        data.moveToFirst();

        LineString line = new LineString();
        while (data.moveToNext()) {

            Location loc = data.getLocation();
            line.addPosition(new Position(loc.getLatitude(), loc.getLongitude()));
            int newPart = Math.round(loc.getBearing());
            if (newPart > maxPart)
                maxPart = newPart;
            trackPoints.add(new MultiGeoPoint(loc.getLatitude(), loc.getLongitude(), newPart));

        }
        setPart(maxPart); //





//        // Create feature with geometry for SOAP -------------------------------------------------------
//        Feature feature = new Feature(line);
//
//        // Set optional feature identifier
//        feature.setIdentifier("Apsekojums");
//
//        JSONObject jsonBuffRad = new JSONObject();
//        try {
//            int iBuffSize = ApplicationController.preferences.getInt(Constants.BUFFER_SIZE_PREF, Constants.BUFFER_SIZE_M);
//            jsonBuffRad.put("BufRad", iBuffSize);
//            feature.setProperties(jsonBuffRad);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        // Set optional feature properties
////        feature.setProperties(new JSONObject());
//
//        // Convert to formatted JSONObject
//        JSONObject geoJSON = null;
//        try {
//            geoJSON = feature.toJSON();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        geometryString = "";
//        if (geoJSON != null) geometryString = geoJSON.toString();

    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTrackArea(double area){
        trackArea = area;
    }

    public double addTrackArea(double area) {
        trackArea += area;
        return trackArea;
    }

    public double getTrackArea() {
        return trackArea;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }

    // TODO - DB transakcijas janes kaut kur citur
    // ControlPoints
    public int insertControlPoints(Context context, int parentId, ContentValues[] cvs){

        int iRet = -1;
        try {

            SQLiteDatabase db = DatabaseHelper.getHelper(context).getWritableDatabase();
            db.beginTransaction();

            // nodzēš visus parentId
            db.delete(AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME, AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID + "=" + String.valueOf(parentId), null);
            for (ContentValues cv: cvs ) {
                iRet += db.insertWithOnConflict(AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return iRet;
    }

    public int deleteControlPoints(Context context,int parentId){

        int iRet = -1;
        try {

            SQLiteDatabase db = DatabaseHelper.getHelper(context).getWritableDatabase();
            db.beginTransaction();

            // nodzēš visus parentId
            iRet = db.delete(AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME, AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID + "=" + String.valueOf(parentId), null);

            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return iRet;
    }

    public String getControlType() {
        return controlType;
    }

    public void setDescr(String descr) {
        this.description = descr;
    }

    public void setInspector(String inspector) {
        this.executor = inspector;
    }

    public boolean isEditable() {
        if(state != AssignmentDatabaseContract.ASSIGNMENT_DONE && state != AssignmentDatabaseContract.ASSIGNMENT_SYNCED)
            return true;

        return false;
    }

    public List<MultiGeoPoint> getTrackPoints() {
        return this.trackPoints;
    }

    public int getCurrentPart() {
        return currentPart;
    }

    public void addPartCounter(){
        currentPart++;
    }

    public void setPart(int part) {
        this.currentPart = part;
    }

    public String getGeometryAsString() {

        if(TextUtils.isEmpty(geometry)) return geometry;

        com.vividsolutions.jts.geom.Geometry jtsGeometry = null;
        WKTReader reader = new WKTReader();
        try {
            jtsGeometry = reader.read(geometry);
        } catch (ParseException ex) {
            return geometry;
        }

        GeoJsonWriter writer = new GeoJsonWriter();
        writer.setEncodeCRS(false);
        String geoJson = writer.write(jtsGeometry);
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("type", "Feature"); // Set the first name/pair
            jsonObj.put("geometry", geoJson);
        }
        catch(JSONException ex){
        }

        String retString = jsonObj.toString().replaceAll("\\\\","").replace("}\"}", "}}").replace("geometry\":\"", "geometry\":");
        return retString;

    }
}


