package com.acoms.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by A on 3/26/2016.
 */
public class Units {

    public int getId() {
        return id;
    }

    public Units(int id, String name, int active) {
        this.id = id;
        this.name = name;
        this.active = active;
    }

    @SerializedName("ID")
    private int id;

    @SerializedName("Nosaukums")
    private String name;

    public int getActive() {
        return active;
    }

    @SerializedName("Aktivs")
    private int active;

    public String getName() {
        return name;
    }

     @Override
    public String toString() {
        return name;
    }

    public boolean isActive() {
        return (active==1);
    }
}
