package com.acoms.model;

/**
 * Created by aldisgrauze on 03.08.2017.
 */

public class BackPressedMessage {

    boolean pressed;

    public BackPressedMessage(boolean pressed) {
        this.pressed = pressed;
    }
}
