package com.acoms.model;

import org.oscim.theme.styles.AreaStyle;

/**
 * Created by AldisGrauze on 11.02.2016.
 */
public final class Constants {

    public static final String TASK_TYPE = "AKA";

    public static final String LAST_ID = "LastDocId";
    public static final String MAP_PATH = "mapPath";
    public static final String DOWNLOAD_REFERENCE = "downloadReference";
    public static final String LAST_MODE = "lastMode";

    public static final String ARCGIS_URL = "https://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/";
    public static final int ARCGIS_MINZOOM = 14;
    public static final int ARCGIS_MAXZOOM = 19;

    public static final float DEF_LAT = 56.9546f;
    public static final float DEF_LNG = 24.111694f;

    public static final int GPS_MIN_TIME = 5;
    public static final int GPS_MIN_DIST = 5;
    public static final int GPS_MIN_ACCURACY = 100;
    public static final String GPS_FREQ = "gps_frequence";
    public static final String GPS_ACCURACY = "gps_accuracy";

    //(attālums m * diametrs 50m + pi*25^2)/10000 ha
    // konstante ko izmanto attāluma pārrēķinam uz laukumu - tb teorētiskais buferis 25m uz katru pusi
    public static final float AREA_BUFFER_SINGLE_M = 25;
    public static final float AREA_BUFFER_ENDS = 3.1415f * (AREA_BUFFER_SINGLE_M*AREA_BUFFER_SINGLE_M);

    public static final int DEF_SYNC_FREQUENCY_MINS =1*24*60 ;

    public static final String APP_PREFERENCES = "com.acoms.koanketa";

    public static final String LAST_TASK_SYNC_TIME = "2001-01-01";
    public static final int BUFFER_SIZE_M = 25;
    public static final String BUFFER_SIZE_PREF = "buffer_size";
    public static final String BUFFER_FILL_PREF = "fill_polygon";

    // principā no grādiem uz m būtu jābūt šitā: angular_units * (PI/180) * 6378137
    //public static final double BUFFER_SIZE_TO_MAP_SIZE = 1./((Math.PI/180.)* 6378137.);

    // empīriski koeficienti
    //public static final double BUFFER_SIZE_TO_MAP_SIZE = 0.0000165;  // koeficients (kartes vienības)/m
    //public static final double BUFFER_SIZE_TO_MAP_SIZE = 0.0000165;  // koeficients (kartes vienības)/m
    //public static final double BUFFER_AREA_TO_HA = 484078.60279;

    public static final String APP_NAME = "vaad";
    public static final String ACTUAL_COUNTRY = "latvia";
    public static final String URL_MAPSFORGE = "http://download.mapsforge.org/maps/europe/";
    public static final String MAP_EXTENSION = ".map";

    public static final String DEMO_USERNAME = "demo";
    public static final String DEMO_USERTOKEN = "123456";
    public static final String DEMO_USERCODE = "1";
    public static final String LOG_FILE_NAME = "vaad-log";
    public static final String LOG_LATEST_FILE_NAME = "vaad-log-latest.csv";
    public static final String LOG_FILE_PATH = "latest_log_file";
    public static final String LOG_RECEIVER = "ID@vaad.gov.lv";
//    public static final String LOG_RECEIVER = "a.grauze@lvm.lv";
    public static final String MAX_LOG_SIZE = "3MB";

    public static final String LAST_POS_LAT = "last_pos_lat";
    public static final String LAST_POS_LNG = "last_pos_lng";
    public static final int MAX_PICTURES = 5;
    public static final String LOG_FILE_SIZE = "max_log_file_size";
}
