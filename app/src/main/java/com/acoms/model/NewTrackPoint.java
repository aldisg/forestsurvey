package com.acoms.model;

import android.location.Location;

/**
 * Created by aldisgrauze on 12.08.2017.
 */

public class NewTrackPoint {
    Location location;
    public NewTrackPoint(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return this.location;
    }
}
