package com.acoms.model;

import android.location.Location;

/**
 * Created by aldisgrauze on 13.08.2017.
 */

public class NewLocation {

    private final Location location;

    public NewLocation(Location loc) {
        location = loc;
    }

    public Location getLocation() {
        return location;
    }
}
