package com.acoms.model;

/**
 * Created by aldisgrauze on 12.08.2017.
 */

public class TaskDetailsLoaded {
    private Assignment task;
    public TaskDetailsLoaded ( Assignment task){
        this.task = task;
    }

    public Assignment getTask() {
        return task;
    }
}
