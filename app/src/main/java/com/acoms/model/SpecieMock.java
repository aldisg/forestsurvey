package com.acoms.model;

/**
 * Created by AldisGrauze on 13.02.2016.
 */
public class SpecieMock {

    private int code;
    private String name;
    private int[] koCodes;

    public int[] getKoCodes() {
        return koCodes;
    }

    public SpecieMock(int code, String name, int[] codes) {

        this.koCodes = codes;
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // !! tas ir spineradateram
    @Override
    public String toString() {
        return name;
    }
}
