package com.acoms.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldis Grauze on 15.16.12.
 */
public class SpeciesControl {

    @SerializedName("KaitigaisOrganismsID")
    public int dangerousID;

    @SerializedName("Aktivs")
    public int active;

}
