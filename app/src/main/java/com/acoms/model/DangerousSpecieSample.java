package com.acoms.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by AldisGrauze on 15.02.2016.
 */
public class DangerousSpecieSample implements Parcelable {

    private int specieID;
    private String specieName;
    private boolean isSelected;
    private boolean isFound;

    public int getSpecieID() {
        return specieID;
    }

    public String getSpecieName() {
        return specieName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isFound() {
        return isFound;
    }

    public void setIsFound(boolean isFound) {
        this.isFound = isFound;
    }

    public boolean isLabNcessary() {
        return isLabNcessary;
    }

    public void setIsLabNcessary(boolean isLabNcessary) {
        this.isLabNcessary = isLabNcessary;
    }

    private boolean isLabNcessary;

    public DangerousSpecieSample(int specieID, String specieName) {
        this.specieID = specieID;
        this.specieName = specieName;
    }

    public DangerousSpecieSample(int specieID, String specieName, boolean isSelected) {
        this.specieID = specieID;
        this.specieName = specieName;
        this.isSelected = isSelected;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(specieID);
        dest.writeString(specieName); // principā nevajag, jo ir id, bet var palīdzēt parādīt listview
        dest.writeInt(isSelected ? 1 : 0);
        dest.writeInt(isFound ? 1 : 0);
    }

    public DangerousSpecieSample(Parcel in){

        this.specieID = in.readInt();
        this.specieName = in.readString();
        this.isSelected = (in.readInt() == 0) ? false : true ;
        this.isFound = (in.readInt() == 0) ? false : true ;
    }

    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public DangerousSpecieSample createFromParcel(Parcel in) {
                    return new DangerousSpecieSample(in);
                }

                public DangerousSpecieSample[] newArray(int size) {
                    return new DangerousSpecieSample[size];
                }
            };


    public boolean equals(Object ob){
        if(!(ob instanceof DangerousSpecieSample)) return false;
        return(specieID==((DangerousSpecieSample)ob).specieID); //though technically the cast is
    }

    public static String findName(int koCode, List<DangerousSpecie> specieSamples) {
        if(specieSamples == null) return null;
        for( DangerousSpecie ds : specieSamples){
            if(ds.getId() == koCode) return ds.getName();
        }
        return null;
    }
}
