package com.acoms.koanketa;

//import android.support.v7.app.AppCompatActivity;

    import org.oscim.android.MapActivity;
    import org.oscim.android.MapView;
    import org.oscim.core.MapPosition;
    import org.oscim.core.Tile;
    import org.oscim.layers.tile.buildings.BuildingLayer;
    import org.oscim.layers.tile.vector.VectorTileLayer;
    import org.oscim.layers.tile.vector.labeling.LabelLayer;
    import org.oscim.map.Map;
    import org.oscim.theme.VtmThemes;
    import org.oscim.tiling.source.mapfile.MapFileTileSource;
    import org.oscim.tiling.source.mapfile.MapInfo;
    import org.oscim.tiling.source.oscimap4.OSciMap4TileSource;
//    import org.slf4j.Logger;
//    import org.slf4j.LoggerFactory;

    import android.content.Context;
    import android.content.SharedPreferences;
    import android.os.Bundle;

    import com.acoms.model.Constants;

    import java.util.prefs.Preferences;

public class MapVtmActivity extends MapActivity {
    //public static final Logger log = LoggerFactory.getLogger(MapVtmActivity.class);

    MapFileTileSource mTileSource;
    //MapView mMapView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_vtm);

        Map map = this.map();

        VectorTileLayer baseLayer = map.setBaseMap(new OSciMap4TileSource());
        map.layers().add(new BuildingLayer(map, baseLayer));
        map.layers().add(new LabelLayer(map, baseLayer));
        map.setTheme(VtmThemes.OSMARENDER);

        map.setMapPosition(56.95, 24.09, 1 << 16);

    }

    private void initOfflineMap(){

        if( mTileSource == null) {
            mTileSource = new MapFileTileSource();

            //String file = intent.getStringExtra(FilePicker.SELECTED_FILE);
            SharedPreferences prefs = this.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
            String file = prefs.getString(Constants.MAP_PATH, "");
            if ( file != "" &&  mTileSource.setMapFile(file)) {

                VectorTileLayer l = mMap.setBaseMap(mTileSource);
                mMap.setTheme(VtmThemes.DEFAULT);

                mMap.layers().add(new BuildingLayer(mMap, l));
                mMap.layers().add(new LabelLayer(mMap, l));

                MapInfo info = mTileSource.getMapInfo();
                if (info.boundingBox != null) {
                    MapPosition pos = new MapPosition();
                    pos.setByBoundingBox(info.boundingBox,
                            Tile.SIZE * 4,
                            Tile.SIZE * 4);
                    mMap.setMapPosition(pos);
                    //Samples.log.debug("set position {}", pos);
                } else if (info.mapCenter != null) {

                    double scale = 1 << 8;
                    if (info.startZoomLevel != null)
                        scale = 1 << info.startZoomLevel.intValue();

                    mMap.setMapPosition(info.mapCenter.getLatitude(),
                            info.mapCenter.getLongitude(),
                            scale);
                }
            }
        }
    }
}