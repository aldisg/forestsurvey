package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import android.support.v4.app.Fragment;

public class TrackMapActivity extends SingleFragmentActivity {
    public static final String EXTRA_RUN_ID = "RUN_ID";

    @Override
    protected Fragment createFragment() {
        long runId = getIntent().getLongExtra(EXTRA_RUN_ID, -1);
        if (runId != -1) {
            return TrackMapFragment.newInstance(runId);
        } else {
            return new TrackMapFragment();
        }
    }

}