package com.acoms.koanketa;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
//import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.acoms.account.AccountGeneral;
import com.acoms.model.Constants;

import timber.log.Timber;


public class SplashActivity extends BaseActivity {

    private TextView resultText;

    private AccountManager mAccountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_splash);

        resultText = (TextView) findViewById(R.id.textResult);
        resultText.setText(getResources().getString(R.string.auth_processing));

        mAccountManager = AccountManager.get(getBaseContext());
        getTokenForAccountCreateIfNeeded(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS);

    }

    private void startNextActivity(){

        Runnable mainRun = new Runnable() {
            @Override
            public void run() {
                nextActivity();
                finish();
            }
        };

        Handler startHandler = new Handler();
        startHandler.postDelayed(mainRun,1000);
    }

    public void nextActivity(){

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private void addNewAccount(String accountType, String authTokenType) {
        Timber.d("addNewAccount called");
        final AccountManagerFuture<Bundle> future = mAccountManager.addAccount(accountType, authTokenType, null, null, this, new AccountManagerCallback<Bundle>() {
            @Override
            public void run(AccountManagerFuture<Bundle> future) {
                try {
                    Bundle bnd = future.getResult();
                    Timber.d("AddNewAccount Bundle is " + bnd);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, null);
    }


    private void getTokenForAccountCreateIfNeeded(String accountType, String authTokenType) {
        final AccountManagerFuture<Bundle> future = mAccountManager.getAuthTokenByFeatures(accountType, authTokenType, null, this, null, null,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        Bundle bnd = null;
                        try {
                            bnd = future.getResult();
                            String authToken = bnd.getString(AccountManager.KEY_AUTHTOKEN);
                            if (authToken != null) {
                                String accountName = bnd.getString(AccountManager.KEY_ACCOUNT_NAME);
                                Timber.i("get token for " + accountName);
                                myApp.setConnectedAccount(new Account(accountName, AccountGeneral.ACCOUNT_TYPE));
                            }
                            //showMessage(((authToken != null) ? "SUCCESS!\ntoken: " + authToken : "FAIL"));
                            Timber.d("GetTokenForAccount Bundle is " + bnd);

                            //ClassifsDAO.getInstance().getTestSpecies(getApplicationContext(), "vaad_soap.json");

                            //getOtherClassifs();
                            startNextActivity();

                        } catch (Exception e) {
                            e.printStackTrace();
                            showMessage(e.getMessage());
                        }
                    }
                }
                , null);
    }

    private void showMessage(final String msg) {
        if (msg == null || msg.trim().equals(""))
            return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
