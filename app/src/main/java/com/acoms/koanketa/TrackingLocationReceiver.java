package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.acoms.model.Constants;

import timber.log.Timber;

public class TrackingLocationReceiver extends LocationReceiver {

    private static final String TAG = Constants.APP_NAME + " LocReceiver DB";

    @Override
    protected void onLocationReceived(Context c, Location loc) {

        if( loc == null ) return;
        TrackManager.get(c).insertLocation(loc);
    }

}

