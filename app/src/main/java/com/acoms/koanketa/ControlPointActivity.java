package com.acoms.koanketa;

import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import com.acoms.model.BackPressedAllowed;
import com.acoms.model.BackPressedMessage;
import com.acoms.model.ControlPoint;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


/**
 * Created by acoms on 28.12.15.
 */
public class ControlPointActivity extends SingleFragmentActivity{

    public static final String CURRENT_POINT = "CURRENT_POINT";
    public static final String CURRENT_POINT_EDITABLE = "CURRENT_POINT_EDITABLE";

    @Override
    protected Fragment createFragment() {

        Bundle data = getIntent().getExtras();
        ControlPoint pointData = (ControlPoint) data.getParcelable(CURRENT_POINT);
        boolean editable = data.getBoolean(CURRENT_POINT_EDITABLE);
        return ControlPointFragment.newInstance(pointData, editable);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(BackPressedAllowed res) {
        if(res.isAllowed())
            super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new BackPressedMessage(false));
    }

}