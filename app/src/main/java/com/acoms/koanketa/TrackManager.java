package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.acoms.ApplicationController;
import com.acoms.database.TrackDatabaseHelper;
import com.acoms.model.Constants;
import com.acoms.model.Track;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.List;

import timber.log.Timber;

import static com.acoms.koanketa.BaseActivity.myApp;

public class TrackManager {

    private static final String PREFS_FILE = "runs";
    private static final String PREF_CURRENT_RUN_ID = "TrackManager.currentRunId";

    public static final String ACTION_LOCATION = "com.acoms.koanketa.ACTION_LOCATION";

//    private static final String TEST_PROVIDER = "TEST_PROVIDER";
//    private static final long INTERVAL = 5000;
//    private static final long FASTEST_INTERVAL = 5000;

    private static TrackManager sTrackManager;
    private Context mAppContext;

    private TrackDatabaseHelper mHelper;
    private SharedPreferences mPrefs;

    private long mCurrentRunId;
    private Track mCurrentRun;
    private boolean startRequested;
    private int mActivePart;

    private TrackManager(Context appContext) {
        mAppContext = appContext;
        mHelper = new TrackDatabaseHelper(mAppContext);
        mPrefs = mAppContext.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mCurrentRunId = mPrefs.getLong(PREF_CURRENT_RUN_ID, -1);
    }

    public static TrackManager get(Context c) {
        if (sTrackManager == null)
            sTrackManager = new TrackManager(c.getApplicationContext());             // application context lai nav memory leeks
        return sTrackManager;
    }

    public boolean isStartRequested(){ return startRequested;}

    public void startLocationUpdates() {

        startRequested = myApp.startGPSService();
    }

    public void stopLocationUpdates() {

//        Timber.i("try to stop updates");
        myApp.stopGPSService();
        startRequested = false;
    }

    public void startTrackingRun(long id) {

        if( mCurrentRun == null ) mCurrentRun = new Track(id);
        mCurrentRunId = id;//run.getId(); // keep the ID
        mPrefs.edit().putLong(PREF_CURRENT_RUN_ID, mCurrentRunId).commit();

        // start location updates
        startLocationUpdates();
    }

//    public void stopRun() {
//
//        stopLocationUpdates();
//        //saveRun();
//
//        mCurrentRunId = -1;
//        mCurrentRun = null;
//
//        mPrefs.edit().remove(PREF_CURRENT_RUN_ID).commit();
//    }

    public void insertLocation(Location loc) {

        if (mCurrentRunId != -1 && mActivePart != -1) {
            Timber.i("backgroundMode save - mCurrentTrackId=" + mCurrentRunId + " mCurrentTrack=" + mActivePart + " save loc");
            mHelper.insertLocation(mCurrentRunId, mActivePart, loc);
        }
    }


    public TrackDatabaseHelper.LocationCursor queryLocationsForRun(long runId) {
        return mHelper.queryLocationsForRun(runId);
    }

    public boolean isRunStarted() {

        if( mCurrentRun == null )
            return false;
        return true;
    }

    public long getCurrentRunId(){
        return mCurrentRunId;
    }

    public int insertLocations(int runId, List<Location> locations) {
        return mHelper.insertLocations(runId, locations);
    }


    public void startBackgroundMonitoring(int iPart) {
        mActivePart = iPart;
        Timber.i("Background mode active");
    }

    public void stopBackgroundMonitoring() {
        mActivePart = -1;
        Timber.i("Background mode stopped");
    }


    //    private void broadcastLocation(Location location) {
//
//        //Timber.i("Broadcast location");
//        Intent broadcast = new Intent(ACTION_LOCATION);
//        broadcast.putExtra(LocationManager.KEY_LOCATION_CHANGED, location);
//        mAppContext.sendBroadcast(broadcast);
//    }

}
