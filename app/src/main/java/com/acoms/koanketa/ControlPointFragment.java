package com.acoms.koanketa;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.acoms.ApplicationController;
import com.acoms.adapters.SampleTypeAdapter;
import com.acoms.adapters.UnitsAdapter;
import com.acoms.database.AssignmentDatabaseContract;
import com.acoms.database.ClassifsDAO;
import com.acoms.model.BackPressedAllowed;
import com.acoms.model.BackPressedMessage;
import com.acoms.model.Constants;
import com.acoms.model.ControlPoint;
import com.acoms.model.ControlPointPicture;
import com.acoms.model.DangerousSpecie;
import com.acoms.model.DangerousSpecieSample;
import com.acoms.model.Samples;
import com.acoms.model.Specie;
import com.acoms.model.Units;
import com.acoms.utils.ClickToSelectEditText;
import com.acoms.adapters.SpeciesAdapter;
import com.acoms.utils.ImageFilePath;
import com.squareup.picasso.Picasso;
//import com.acoms.model.SpecieMock;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by acoms on 28.12.15.
 */
public class ControlPointFragment extends Fragment  {

    public static final String CURRENT_POINT = "CURRENT_POINT";
    public static final String NEW_POINT = "NEW_POINT";
    private static final int PICK_PHOTO_FOR_CONTROL_POINT = 223;
    private static final String CURRENT_POINT_EDITABLE = "CURRENT_POINT_EDITABLE";

    static String  TAG = "ControlPointFragment";
//    ActionMode actionMode;

    private ControlPoint mControlPoint;

    private TextView mAmount, mDecision;
    private KOCustomAdapter koAdapter;
    private List<DangerousSpecie> koAllSpecies;
    private ListView dangerousSpeciesList;
    private ClickToSelectEditText<Specie> speciesList;
    private ClickToSelectEditText<Units>  unitsList;
    private ClickToSelectEditText<Samples>  samplesList;

    private ClassifsDAO classifs;
    private Toolbar toolbar;
    private ListView pictureList;
    private List<String> controlPictures;
    private picturesCustomAdapter pictureAdapter;
    private boolean isChanged;

    private View view;
    //private Picasso.Builder builder;
    private ImageView picturesToggleImageView;
    private ImageView koToggleImageView;
    private boolean mInputEnabled;
    private FloatingActionButton addPict;

    public static ControlPointFragment newInstance(ControlPoint point, boolean editable) {

        ControlPointFragment cpf = new ControlPointFragment();
        Bundle args = new Bundle();
        args.putParcelable(CURRENT_POINT, point);
        args.putBoolean(CURRENT_POINT_EDITABLE, editable);
        cpf.setArguments(args);
        return cpf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        Bundle args = getArguments();
        if (args != null) {
            mControlPoint = args.getParcelable(CURRENT_POINT);
            mInputEnabled = args.getBoolean(CURRENT_POINT_EDITABLE);
        }
        else {
            mControlPoint = new ControlPoint(-1, 1, 0, 0, 0.f, 0, "", null, Constants.DEF_LAT, Constants.DEF_LNG, null);
            mInputEnabled = args.getBoolean(CURRENT_POINT_EDITABLE);
        }

        classifs = ApplicationController.classifs;
        isChanged = false;

//        builder = new Picasso.Builder(getActivity());
//        builder.listener(new Picasso.Listener()
//        {
//            @Override
//            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
//            {
//                exception.printStackTrace();
//            }
//        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_point, container, false);

        // toolbar ------------------------------------------------
        toolbar = (Toolbar) view.findViewById(R.id.toolbarPoint);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        String titleText = getResources().getString(R.string.sample_activity_title);
        activity.getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>" + titleText + " </font>"));
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHasOptionsMenu(true);

        // pamatsuga ----------------------------------------------
        List<Specie> spList = classifs.getSpecies();
        speciesList = (ClickToSelectEditText<Specie>) view.findViewById(R.id.specie);
        //Adapter must implement ListAdapter (e.g. extend ArrayAdapter<MyModel>)
        SpeciesAdapter adapter = new SpeciesAdapter(getContext(), android.R.layout.simple_list_item_1,spList);
        speciesList.setText(mControlPoint.getMainSpecieName());
        if(!mInputEnabled) speciesList.disableInput();
        speciesList.setAdapter(adapter);
        speciesList.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<Specie>() {
            @Override
            public void onItemSelectedListener(Specie item, int selectedIndex) {
                hideKeyboard();
                speciesList.setError(null);
                speciesList.setText(item.getName());
                mControlPoint.setMainSpecieCode(item.getId());
                filterKOAdapter(koAdapter, item.getKoCodes());
                isChanged = true;
            }
        });
        // daudzums -----------------------------------------------
        mAmount = (TextView)view.findViewById(R.id.amountText);
        float fAmount = mControlPoint.getAmount();
        if(fAmount > 0.01f)
            mAmount.setText(String.valueOf(fAmount));
        mAmount.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return !mInputEnabled;
            }
        });

            // mērvienības ----------------------------------------------
        List<Units> uList = classifs.getUnits();
        unitsList = (ClickToSelectEditText<Units>) view.findViewById(R.id.units);
        unitsList.setText(mControlPoint.getUnitName());

        //Adapter must implement ListAdapter (e.g. extend ArrayAdapter<MyModel>)
        UnitsAdapter a = new UnitsAdapter(getContext(), android.R.layout.simple_list_item_1,uList);
        if(!mInputEnabled) unitsList.disableInput();
        unitsList.setAdapter(a);
        unitsList.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<Units>() {
            @Override
            public void onItemSelectedListener(Units item, int selectedIndex) {
                if(!mInputEnabled) return;
                hideKeyboard();
                unitsList.setError(null);
                unitsList.setText(item.getName());
                mControlPoint.setUnits(item.getId());
                isChanged = true;
            }
        });

        // paraugu veidi ----------------------------------------------
        List<Samples> sList = classifs.getSamples();
        samplesList = (ClickToSelectEditText<Samples>) view.findViewById(R.id.sampleType);
        samplesList.setText(mControlPoint.getTypeCodeName());
        if(!mInputEnabled) samplesList.disableInput();

        //Adapter must implement ListAdapter (e.g. extend ArrayAdapter<MyModel>)
        SampleTypeAdapter s = new SampleTypeAdapter(getContext(), android.R.layout.simple_list_item_1,sList);
        samplesList.setAdapter(s);
        samplesList.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<Samples>() {
            @Override
            public void onItemSelectedListener(Samples item, int selectedIndex) {
                if(!mInputEnabled) return;
                hideKeyboard();
                samplesList.setError(null);
                samplesList.setText(item.getName());
                mControlPoint.setSampleTypeCode(item.getId());
                isChanged = true;
            }
        });


        // komentāri/lēmums -----------------------------------------
        mDecision = (TextView) view.findViewById(R.id.descrText);
        mDecision.setText(mControlPoint.getDecision());
        mDecision.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return !mInputEnabled;
            }
        });

        // kaitīgie organismi ---------------------------------------------------------------
        dangerousSpeciesList = (ListView) view.findViewById(R.id.KO_list);
        koAllSpecies = classifs.getDangerousSpecies();

        DangerousSpecieSample[] samples = mControlPoint.getDangerous();
        //if( mControlPoint.specieList == null )
            mControlPoint.specieList = new ArrayList<>();
        if( samples != null && samples.length > 0 ) {
            for (DangerousSpecieSample ds : samples) {
                if(ds != null ) mControlPoint.specieList.add(ds);
            }
        }
        koAdapter = new KOCustomAdapter(getActivity(),R.layout.ko_item, mControlPoint.specieList);
        dangerousSpeciesList.setAdapter(koAdapter);
        setListViewHeightBasedOnChildren(dangerousSpeciesList);

//        dangerousSpeciesList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                // TODO varbūt vajag helpu ????
//                //Toast.makeText(getActivity(), "Info par ", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

        ViewGroup dangItemsToggle = (ViewGroup) view.findViewById(R.id.dang_items_toggle);
        koToggleImageView = (ImageView)view.findViewById(R.id.toogle_ko_down);
        dangItemsToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if(dangerousSpeciesList.getVisibility() == View.VISIBLE ) {
                    dangerousSpeciesList.setVisibility(View.GONE);
                    koToggleImageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_36dp);
                }
                else {
                    dangerousSpeciesList.setVisibility(View.VISIBLE);
                    koToggleImageView.setImageResource(R.drawable.ic_keyboard_arrow_left_black_36dp);
                }
            }
        });

        pictureList = (ListView) view.findViewById(R.id.pictures_list_list);
        pictureAdapter = new picturesCustomAdapter(getActivity(),R.layout.picture_item, mControlPoint.pictureList);
        pictureList.setAdapter(pictureAdapter);
        setListViewHeightBasedOnChildren(pictureList);

        ViewGroup picturesToggle = (ViewGroup) view.findViewById(R.id.pictures_toggle);
        picturesToggleImageView = (ImageView)view.findViewById(R.id.toogle_pics_down);

        picturesToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if(pictureList.getVisibility() == View.VISIBLE ) {
                    pictureList.setVisibility(View.GONE);
                    picturesToggleImageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_36dp);
                }
                else {
                    pictureList.setVisibility(View.VISIBLE);
                    picturesToggleImageView.setImageResource(R.drawable.ic_keyboard_arrow_left_black_36dp);
                    // TODO ja saraksts parādās vaja scrollēt līdz apakšai
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            ((ScrollView)view).fullScroll(ScrollView.FOCUS_DOWN);
////                            ((ScrollView)view).smoothScrollBy(0,pictureList.getBottom());
//                        }});

                }
            }
        });

        addPict = (FloatingActionButton)view.findViewById(R.id.addPicture);
        addPict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mInputEnabled) return;
                if( pictureList.getCount() >= Constants.MAX_PICTURES ){
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.task_picture_title)
                            .setMessage(String.format(getContext().getResources().getString(R.string.task_picture_limit), Constants.MAX_PICTURES))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            })
                            .show();

                }
                else
                    pickImageThumbs();
            }
        });
        if(!mInputEnabled)
            addPict.setVisibility(View.INVISIBLE);

        return view;
    }


    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private boolean savePoint(){

        int result = RESULT_OK;
        if(mInputEnabled) {

            // validate input fields
            if (validatePointParams() == false) return false;

            mControlPoint.setDangerousArray();

            // daudzums
            String sAmount = mAmount.getText().toString();
            if (!sAmount.isEmpty())
                mControlPoint.setAmount(Float.parseFloat(sAmount));

            // piezīmes
            mControlPoint.setDecision(mDecision.getText().toString());

            isChanged = false;
        }
        else
            result =RESULT_CANCELED;

        Intent i = new Intent();
        i.putExtra(NEW_POINT, mControlPoint);
        getActivity().setResult(result, i);
        getActivity().finish();
        return true;
    }

    private boolean validatePointParams(){

        boolean bRet = true;
        String sErr = getResources().getString(R.string.required_field);

        String str = speciesList.getText().toString();
        if(str.equalsIgnoreCase("")) {
            speciesList.setError(sErr);
            bRet = false;
        }

        str = mAmount.getText().toString();
        if(str.equalsIgnoreCase("")) {
            mAmount.setError(sErr);
            bRet = false;
        }

        str = samplesList.getText().toString();
        if(str.equalsIgnoreCase("")) {
            samplesList.setError(sErr);
            bRet = false;
        }

        str = unitsList.getText().toString();
        if(str.equalsIgnoreCase("")) {
            unitsList.setError(sErr);
            bRet = false;
        }

        return bRet;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(mInputEnabled)
            inflater.inflate(R.menu.point_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                checkChangesAndExit();
                break;

            case R.id.actionSave:
                savePoint();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkChangesAndExit(){

        if(isChanged) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.control_point_save_question)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(savePoint())
                                EventBus.getDefault().post(new BackPressedAllowed(true));
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EventBus.getDefault().post(new BackPressedAllowed(true));
                        }
                    })
                    .show();
        }
        else
            EventBus.getDefault().post(new BackPressedAllowed(true));

    }

    private static boolean findIntinTntArr(int val, int[] intArr){

        boolean ret = false;
        for(int i = 0; i <intArr.length; i++)
            if( intArr[i] == val ) return true;
        return ret;
    }

    private void filterKOAdapter(KOCustomAdapter koAdapter, int[] koCodes) {

        List<DangerousSpecieSample> newList = new ArrayList<DangerousSpecieSample>();
        for (DangerousSpecie ds : koAllSpecies) {

             if( findIntinTntArr(ds.getId(), koCodes) )
                newList.add(new DangerousSpecieSample(ds.getId(),ds.getName()));
        }

          // šitais galīgi nav labais stils, bet notify nestrādā, jo pareizzo listu nevar aktualizēt
          //koAdapter.notifyDataSetChanged();
          dangerousSpeciesList.setAdapter(new KOCustomAdapter(getActivity(),R.layout.ko_item, newList));
          setListViewHeightBasedOnChildren(dangerousSpeciesList);
    }

    private class KOCustomAdapter extends ArrayAdapter<DangerousSpecieSample> {

        //private ArrayList<DangerousSpecieSample> specieList ;
        public KOCustomAdapter(Context context, int textViewResourceId,
                               List<DangerousSpecieSample> list) {
            super(context, textViewResourceId, list);

            mControlPoint.specieList = new ArrayList<>();
            mControlPoint.specieList.addAll(list);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            //Timber.v(String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.ko_item, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);
                if(mInputEnabled != false ) {
                    holder.name.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CheckBox cb = (CheckBox) v;
                            DangerousSpecieSample dSpecie = (DangerousSpecieSample) cb.getTag();
                            dSpecie.setIsSelected(cb.isChecked());
                        }
                    });
                }
                else holder.name.setClickable(false);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }


            DangerousSpecieSample specie = mControlPoint.specieList.get(position);
            holder.code.setText(" (" +  specie.getSpecieID() + ")");
            holder.name.setText(specie.getSpecieName());
            holder.name.setChecked(specie.isSelected());
            holder.name.setTag(specie);

            return convertView;

        }

    }

    private class picturesCustomAdapter extends ArrayAdapter<ControlPointPicture> {

        private List<ControlPointPicture> picsList;
        public picturesCustomAdapter(Context context, int textViewResourceId,
                               List<ControlPointPicture> list) {
            super(context, textViewResourceId, list);
            picsList = list;
        }

        private class ViewHolder {
            ImageView picture;
            TextView name;
            ImageView image;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {

                LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.picture_item, null);

                holder = new ViewHolder();
                holder.name = (TextView) convertView.findViewById(R.id.picture_path);;
                holder.picture = (ImageView) convertView.findViewById(R.id.picture_preview);
                holder.image = (ImageView) convertView.findViewById(R.id.delete_picture);
                convertView.setTag(holder);
                if(mInputEnabled) {
                    holder.image.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {

                            final View view = v;
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(R.string.app_name)
                                    .setMessage(R.string.picture_delete_question)
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ControlPointPicture dPicture = (ControlPointPicture) view.getTag();
                                            pictureAdapter.remove(dPicture);
                                            pictureAdapter.notifyDataSetChanged();
                                            setListViewHeightBasedOnChildren(pictureList);
                                            isChanged = true;
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, null)
                                    .show();
                        }
                    });
                }
                else
                    holder.image.setVisibility(View.INVISIBLE);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            ControlPointPicture picture = picsList.get(position);

            Picasso.with(getActivity())
            //builder.build()
                    .load(new File(picture.getFullPath()))
                    .resize(80, 80)
                    .centerCrop()
                    .into(holder.picture);

            holder.name.setText(picture.getName());
            holder.image.setTag(picture);

            return convertView;
        }

    }

    public void pickImageThumbs() {

        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        //intent.putExtra("crop", "false");
        intent.putExtra("scale", true);
        intent.putExtra("outputX", 600);
        //intent.putExtra("outputY", 600);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, PICK_PHOTO_FOR_CONTROL_POINT);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_CONTROL_POINT && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            String realPath = ImageFilePath.getPath(getActivity(), uri);
//                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

//            Timber.i("onActivityResult: file path : " + realPath);

            Bitmap bitmap = loadBitmap(uri);
            ControlPointPicture newPic = new ControlPointPicture( mControlPoint.GetID(), realPath, bitmap);
            pictureAdapter.add(newPic);
            pictureAdapter.notifyDataSetChanged();
            setListViewHeightBasedOnChildren(pictureList);
            isChanged = true;
            pictureList.setVisibility(View.VISIBLE);


        }
//        else
//            Toast.makeText(getActivity(), "Nevar nolasīt attēlu", Toast.LENGTH_SHORT).show();

    }

    private Bitmap loadBitmap(Uri uri){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        AssetFileDescriptor fileDescriptor =null;
        try {
            fileDescriptor = getContext().getContentResolver().openAssetFileDescriptor( uri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap actuallyUsableBitmap
                = BitmapFactory.decodeFileDescriptor(
                fileDescriptor.getFileDescriptor(), null, options);

//        try {
//
//            MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//        }
//        catch(OutOfMemoryError e){
//
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }

        return actuallyUsableBitmap;
    }


    @Subscribe
    public void onEvent(BackPressedMessage type) {
        checkChangesAndExit();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mAmount.getWindowToken(), 0);
    }

}
