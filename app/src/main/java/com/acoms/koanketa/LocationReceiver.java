package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.acoms.model.Constants;
import com.acoms.utils.KalmanLatLong;

import timber.log.Timber;

import static com.acoms.koanketa.BaseActivity.myApp;

public class LocationReceiver extends BroadcastReceiver {

    private static final String TAG = "vaad LocationReceiver";
    private static final int MINIMUM_DISTANCE = 1;
    private static final int MAX_DISTANCE = 80;
    private static final float MINIMUM_ACCURACY = 60.f;


    @Override
    public void onReceive(Context context, Intent intent) {

        Location loc = (Location)intent.getParcelableExtra(LocationManager.KEY_LOCATION_CHANGED);
        if( loc != null ) {

            Timber.i(" Got location: " + loc.getLatitude() + ", " + loc.getLongitude() + " acc= " + loc.getAccuracy() + " m bearing=" +   loc.getBearing() );
            if(checkPosition(loc)) {          //if(true){
                Timber.d("Location approved");
                oldLocation = loc;
                if(myApp!=null)
                    myApp.saveLastLocation(loc);  // TODO eventbus
                onLocationReceived(context, loc);
            }
            return;
        }

         //if we get here, something else has happened
        if (intent.hasExtra(LocationManager.KEY_PROVIDER_ENABLED)) {
            boolean enabled = intent.getBooleanExtra(LocationManager.KEY_PROVIDER_ENABLED, false);
            onProviderEnabledChanged(enabled);
        }


    }

    protected void onLocationReceived(Context context, Location loc) {
        if( loc != null )
            Timber.d("Accepted location from " + loc.getProvider() + ": " + loc.getLatitude() + ", " + loc.getLongitude());
    }

    protected void onProviderEnabledChanged(boolean enabled) {
        Timber.d("onProviderEnabledChanged " + (enabled ? "enabled" : "disabled"));
    }


    /////--------------------------------------------------------------

    private Location oldLocation;

    public boolean checkPosition(Location location) {

        int distance = MINIMUM_DISTANCE;
        if(location != null && oldLocation != null) {
            distance = Math.round(location.distanceTo(oldLocation));
        }

        // TODO GPS filtrs
        if (( location != null ) &&
                //(distance > MINIMUM_DISTANCE) &&
                //(distance < MAX_DISTANCE) &&
                //(location.hasSpeed()) &&
                //(location.getSpeed() > 0.1) &&
                //(averageTime < HUMANSPEED) &&
                //(location.hasAccuracy()) &&
                (location.getAccuracy() < MINIMUM_ACCURACY) &&
                (isBetterLocation(location, oldLocation)) ) // From Google example in http://developer.android.com/guide/topics/location/strategies.html#BestPerformance
                {
                    return true;
                }

        return false;
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }



    // aktualizācijas laiks sekundēs
    static final int TIME_DIFFERENCE_THRESHOLD = 1 * 1000;

    /**
     * Decide if new location is better than older by following some basic criteria.
     * This algorithm can be as simple or complicated as your needs dictate it.
     * Try experimenting and get your best location strategy algorithm.
     *
     * @param oldLocation Old location used for comparison.
     * @param newLocation Newly acquired location compared to old one.
     * @return If new location is more accurate and suits your criteria more than the old one.
     */
    boolean isBetterLocationOld(Location oldLocation, Location newLocation) {
        // If there is no old location, of course the new location is better.
        if (oldLocation == null) {
            return true;
        }

        // Check if new location is newer in time.
        boolean isNewer = newLocation.getTime() > oldLocation.getTime();

        // Check if new location more accurate. Accuracy is radius in meters, so less is better.
        boolean isMoreAccurate = newLocation.getAccuracy() < oldLocation.getAccuracy();
        if (isMoreAccurate && isNewer) {
            // More accurate and newer is always better.
            return true;
        } else
            //if (isMoreAccurate && !isNewer)
            {
            // More accurate but not newer can lead to bad fix because of user movement.
            // Let us set a threshold for the maximum tolerance of time difference.
            long timeDifference = newLocation.getTime() - oldLocation.getTime();

            // If time difference is not greater then allowed threshold we accept it.
            if (timeDifference > -TIME_DIFFERENCE_THRESHOLD) {
                return true;
            }
        }

        return false;
    }


    void doWorkWithNewLocation(Context context, Location location) {

        if(checkPosition(location)) {          //if(true){
            onLocationReceived(context, location);
            oldLocation = location;
        }
    }

}
