package com.acoms.koanketa;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.common.api.GoogleApiClient;

import com.acoms.ApplicationController;
import com.acoms.account.AccountGeneral;
import com.acoms.activities.SettingsToolbarActivity;
import com.acoms.fragments.TaskListFragment;
import com.acoms.model.Constants;

import timber.log.Timber;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private NavigationView navigationView;
    public static int activeMode = 0;

    private final int MY_PERMISSIONS_REQUEST_WRITE_MAP = 0;

    private static final String MAP_NAME = "0zgw23.01d";
    private static long downloadReference = -1;
    private static String mapPath;
    private static Activity thisActivity;
    private static boolean bRequestStarted = false;

    private TaskListFragment mTaskListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        thisActivity = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        registerReceiver(onComplete,
//                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        downloadReference = myApp.preferences.getLong(Constants.DOWNLOAD_REFERENCE, -1);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.GET_ACCOUNTS },
                        MY_PERMISSIONS_REQUEST_WRITE_MAP);
        }
        else checkOfflineBasemap(false);

//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    MY_PERMISSIONS_REQUEST_WRITE_MAP);
//        }
//
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.GET_ACCOUNTS},
//                    MY_PERMISSIONS_REQUEST_WRITE_MAP);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //TODO ja pasākums tiek pārtraukts
        //if(downloadReference != -1 )
        //    startDownloadReciever();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // last active mode
        myApp.preferences.getInt(Constants.LAST_MODE, R.id.forest_control);
        navigationView.setCheckedItem(R.id.forest_control);
        navigationView.setNavigationItemSelectedListener(this);

        // sāk ar sarakstu
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        mTaskListFragment = (TaskListFragment) Fragment.instantiate(MainActivity.this, "com.acoms.fragments.TaskListFragment");
        tx.replace(R.id.mainView, mTaskListFragment);
        tx.commit();
        checkOfflineBasemap(false);

        setVersionNumber();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_MAP: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    checkOfflineBasemap(false);

                } else {

                    // ja gadījumā permission nav tad kaut kas jādara
                }
                return;
            }

            // pārējās atļaujas

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //    getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            //stopDwonloadReciever();
        }
        catch(IllegalArgumentException ex){
            //Timber.e("downloadReciever: already unregistered!");
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        setUserName(false);
    }

    private static void startDownloadReciever() {
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        thisActivity.registerReceiver(downloadReceiver, filter);
        bRequestStarted = true;
        Timber.i("downloadReciever: started");
    }

    private void stopDwonloadReciever() {
        Timber.i("downloadReciever: stopped");
        if(bRequestStarted) {
            Timber.i("downloadReciever: unregistered");
            unregisterReceiver(downloadReceiver);
        }
        bRequestStarted = false;
    }
//    BroadcastReceiver onComplete=new BroadcastReceiver() {
//        public void onReceive(Context ctxt, Intent intent) {
//            findViewById(R.id.start).setEnabled(true);
//        }
//    };

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {

            case R.id.forest_control:
                activeMode = 0;
                getSupportActionBar().setTitle(item.getTitle());
                break;

//            case R.id.nav_gallery:
//                // filtrs pēc DU veida un cita darba veikšansa procedūra
//                navigationView.setCheckedItem(R.id.forest_control);
//                Toast.makeText(this, "Atvainojiet, šobrīd vēl nav realizēts", Toast.LENGTH_SHORT).show();
//                break;
//
//            case R.id.nav_slideshow:
//                // filtrs pēc DU veida un cita darba veikšansa procedūra
//                navigationView.setCheckedItem(R.id.forest_control);
//                Toast.makeText(this, "Atvainojiet, šobrīd vēl nav realizēts", Toast.LENGTH_SHORT).show();
//                break;

            case R.id.nav_manage:
                Intent i2 = new Intent(this, SettingsToolbarActivity.class);
                startActivity(i2);
                break;

            case R.id.nav_login:

                // we will use our authenticator
                final AccountManagerFuture<Bundle> future = AccountManager.get(getBaseContext()).addAccount(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, null, null, this, new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        try {
                            Bundle bnd = future.getResult();
                            String accountName = bnd.getString(AccountManager.KEY_ACCOUNT_NAME);
                            Timber.d("AddNewAccount " + accountName);
                            Account account = new Account(accountName, AccountGeneral.ACCOUNT_TYPE);
                            myApp.setConnectedAccount(account);
                            setUserName(true);

                            // TODO set new account sync on (will be off)
                            //ContentResolver.setSyncAutomatically(account, authority, true);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, null);

                break;

            case R.id.nav_send:
                Toast.makeText(this, getResources().getString(R.string.not_implemented), Toast.LENGTH_SHORT).show();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void checkOfflineBasemap(boolean bForce){

        boolean doDownload = bForce;
        String file = myApp.preferences.getString(Constants.MAP_PATH, "");
        if(!doDownload && TextUtils.isEmpty(file) && downloadReference == -1)
            doDownload = true;

        if(doDownload) {

            // TODO šeit vajag valsts izvēli - un iespējams arī iespēju Iestatījumos nomainīt
            String country = Constants.ACTUAL_COUNTRY; //"http://download.mapsforge.org/maps/europe/latvia.map";

            String url = Constants.URL_MAPSFORGE + country + Constants.MAP_EXTENSION;

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription(thisActivity.getResources().getString(R.string.basemap_downloading));
            request.setTitle(thisActivity.getResources().getString(R.string.app_full_name));
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            Timber.i(Constants.APP_NAME, "downloadReciever: offlineMap download started");
            startDownloadReciever();

            Toast.makeText(myApp, thisActivity.getResources().getString(R.string.basemap_download_started), Toast.LENGTH_LONG).show();
//            File f = getDir("basemap", Context.MODE_PRIVATE);

            // TODO temporary - vajag rakstit privātajā
            mapPath = thisActivity.getExternalFilesDir(null) + "/" + MAP_NAME;
            request.setDestinationInExternalFilesDir(thisActivity, null, MAP_NAME);

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) thisActivity.getSystemService(Context.DOWNLOAD_SERVICE);
            downloadReference = manager.enqueue(request);
            myApp.preferences.edit().putLong(Constants.DOWNLOAD_REFERENCE, downloadReference).commit();
        }
    }

    private void setUserName(boolean refresh) {

        View headerView = navigationView.getHeaderView(0);
        TextView userText = (TextView) headerView.findViewById(R.id.currentUser);
        userText.setText(myApp.getUser().getUsername());

        if(refresh)
            refreshList();
    }

    private void setVersionNumber() {

        PackageManager manager = getPackageManager();

        try {
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            String version = info.versionName;

            View headerView = navigationView.getHeaderView(0);
            TextView appText = (TextView) headerView.findViewById(R.id.appTittle);
            appText.setText(getResources().getString(R.string.app_full_name) + " v" + version);

        }catch(PackageManager.NameNotFoundException ex){

        }
    }

    private void refreshList(){

        // te var filtrēt arī pēc darbu veida
        if(mTaskListFragment != null) {
            //Bundle args = new Bundle();
            //args.putString(TaskListFragment.WORK_ID, myApp.getUser().getObjectId());
            mTaskListFragment.refreshList(null);
        }

    }

    private static BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            Timber.i("downloadRecieved: " + " downloadReference:  " + String.valueOf(downloadReference) + " referenceId: "  + String.valueOf(referenceId));
            if (downloadReference == referenceId) {
                Timber.i("mapPath: " + mapPath);
                myApp.preferences.edit().putString(Constants.MAP_PATH, mapPath).commit();
                downloadReference = -1;
                myApp.preferences.edit().putLong(Constants.DOWNLOAD_REFERENCE, downloadReference).commit();

                Toast.makeText(myApp, thisActivity.getResources().getString(R.string.basemap_download_finished), Toast.LENGTH_LONG).show();
            }
        }
    };

}

