package com.acoms.koanketa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.acoms.ApplicationController;

import java.util.List;

/**
 * Created by A on 3/26/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected static ApplicationController myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp = ApplicationController.getInstance();

        // varbūt vēlāk šito vajadzēs
//        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFragment);
//        setSupportActionBar(toolbar);

    }

    public ApplicationController getApp() { return myApp;}



    // back pressed interface ----------------------------------------------
    protected OnBackPressedListener onBackPressedListener;

    public interface OnBackPressedListener {
        void doBack();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null)
            onBackPressedListener.doBack();
        else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }


}
