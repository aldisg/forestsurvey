package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.acoms.model.Constants;

import timber.log.Timber;

public class LocationListCursorLoader extends SQLiteCursorLoader {
    private long mRunId;

    private static final String TAG = Constants.APP_NAME + " loader";

    public LocationListCursorLoader(Context c, long runId) {
        super(c);
        mRunId = runId;
        Timber.i("Load TrackId=" + runId);
    }

    @Override
    protected Cursor loadCursor() {
        return TrackManager.get(getContext()).queryLocationsForRun(mRunId);
    }
}