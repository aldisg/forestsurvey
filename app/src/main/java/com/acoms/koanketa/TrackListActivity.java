package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import android.support.v4.app.Fragment;

import com.acoms.fragments.TaskListFragment;

public class TrackListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {

        return new TaskListFragment();
    }

}