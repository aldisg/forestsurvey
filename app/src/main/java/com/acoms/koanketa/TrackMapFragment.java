package com.acoms.koanketa;

/**
 * Created by A on 11/8/2015.
 */
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acoms.ApplicationController;
import com.acoms.layers.BufferLayer;
import com.acoms.model.Assignment;
import com.acoms.model.AssignmentTrack;
import com.acoms.model.BufferSize;
import com.acoms.model.Constants;
import com.acoms.model.ControlPoint;
import com.acoms.model.MapReadyEvent;
import com.acoms.model.MultiGeoPoint;
import com.acoms.model.NewControlPoint;
import com.acoms.model.NewTrackPoint;
import com.acoms.model.TaskDetailsLoaded;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jeo.map.RGB;
import org.oscim.android.MapScaleBar;
import org.oscim.android.MapView;
import org.oscim.android.cache.TileCache;
import org.oscim.backend.canvas.Bitmap;
import org.oscim.backend.canvas.Color;
import org.oscim.core.BoundingBox;
import org.oscim.core.GeoPoint;
import org.oscim.core.MapPosition;
import org.oscim.core.Tile;

import org.oscim.jeo.JeoUtils;
import org.oscim.layers.Layer;
import org.oscim.layers.PathLayer;
import org.oscim.layers.marker.ItemizedLayer;
import org.oscim.layers.marker.MarkerItem;
import org.oscim.layers.marker.MarkerSymbol;
import org.oscim.layers.tile.bitmap.BitmapTileLayer;
import org.oscim.layers.tile.buildings.BuildingLayer;
import org.oscim.layers.tile.vector.VectorTileLayer;
import org.oscim.layers.tile.vector.labeling.LabelLayer;

import org.oscim.map.Map;
import org.oscim.theme.VtmThemes;
import org.oscim.theme.styles.AreaStyle;
import org.oscim.theme.styles.LineStyle;
import org.oscim.tiling.TileSource;
import org.oscim.tiling.source.bitmap.BitmapTileSource;
//import org.oscim.tiling.source.bitmap.DefaultSources;
import org.oscim.tiling.source.mapfile.MapFileTileSource;
import org.oscim.tiling.source.mapfile.MapInfo;
import org.oscim.tiling.source.oscimap4.OSciMap4TileSource;

import timber.log.Timber;
import static org.oscim.android.canvas.AndroidGraphics.drawableToBitmap;

public class TrackMapFragment extends Fragment  {

    private static final String ARG_RUN_ID = "RUN_ID";
    private static final int LOAD_LOCATIONS = 0;

    private static final String TAG = "vaad TrackMapFragment";
    private static final String PATH_LAYER = "path_layer";
    private static ApplicationController myApp;

    private MapView mMapView;

    private Map mMap;
    private MapFileTileSource mTileSource;

    //pagaidām arcgis
    private TileSource mBitmapTileSource;
    protected BitmapTileLayer mBitmapLayer;
    private final static boolean USE_CACHE = true;
    private TileCache mCache;

    private ItemizedLayer<MarkerItem> markerLayer, currentLocationLayer, taskLayer, controlPointLayer;
//    private PathLayer pathLayer;

    //private boolean isPaused;
    private Bundle currentArgs;
    private String mPositionText, mStartText, mStopText, mPointText;

    private List<ControlPoint> mControlPoints;
    private double buffSize;
    private Assignment mCurrentTask;

    public interface AreaChanged{
        void setArea(double area);
    }

    private AssignmentTrack mCurrentTrack;
    private boolean useMapRotation;
    private boolean initMap = false;
    private BufferLayer mBufferlayer;

    AreaStyle areaStyle = null;


    // eventi --------------------------------------------
    @Subscribe
    public void onEvent(TaskDetailsLoaded result) {
        if(result != null) {
            initTrackData(result.getTask());
        }
    }

    @Subscribe
    public void onEvent(NewTrackPoint point) {
        if(point != null) {
            int part = -1;
            if(mCurrentTask != null ) part = mCurrentTask.getCurrentPart();
            addTrackPoint(point.getLocation(), true, part);
        }
    }

    @Subscribe
    public void onEvent(NewControlPoint point) {
        if(point != null) {
            Timber.i("addControlPoint");
            addControlPoint(point.getPoint());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(BufferSize size){

        final Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            private long time = 0;

            @Override
            public void run()
            {
                updateMap(true);
            }
        }, 1000);

    }

    public void updateMap(boolean force){

        if( !force )
            mMap.updateMap(true);
        else {
            MapPosition pos = mMap.getMapPosition();
            pos.setX(pos.getX() + 0.0000001);
            pos.setY(pos.getY() + 0.0000001);
            mMap.setMapPosition(pos);
        }
    }

    // TrackMapFragment iekšējās funkcijas -----------------------------------------------

    private void drawControlPointLocationsToMap() {

        if(controlPointLayer == null) return;
        if(mControlPoints == null ) return;

        controlPointLayer.removeAllItems();

        if(mControlPoints == null || mControlPoints.size() == 0) return;
        List<MarkerItem> cpList = new ArrayList<>();
        for(ControlPoint point : mControlPoints){
            GeoPoint pLoc = point.getPosition();
            if( pLoc != null )
                cpList.add(new MarkerItem(mPointText, mPointText, pLoc));
        }
        if( cpList. size() > 0 )
            controlPointLayer.addItems(cpList);
    }


    private void addTrackPoint(Location loc, boolean new_point, int part) {

        if(loc != null ) {

            //Timber.i("LocReceiver map lat: " + loc.getLatitude() + " lng: " + loc.getLongitude());

            // display accuracy and point on map
            if (mMapView != null) {

                MultiGeoPoint point = new MultiGeoPoint(loc.getLatitude(), loc.getLongitude(), part);
                currentLocationLayer.removeAllItems();
                currentLocationLayer.addItem(new MarkerItem(mPositionText, mPositionText, point.getPoint()));
                setMapPosition(loc);
                updateUI(loc);
                mBufferlayer.addLinePoint( point, new_point);
                Timber.i("Add Track point");
            }
        }

    }

    private void addControlPoint(GeoPoint point) {
        controlPointLayer.addItem(new MarkerItem(mPointText, mPointText, point));
    }

    private void clearMapLayers(){

//        pathLayer.clearPath();
        for(Layer layer : mMap.layers()){
            if( layer instanceof PathLayer || layer instanceof BufferLayer ){
//                if( layer instanceof PathLayer )
//                    ((PathLayer)layer).clearPath();
//                else
                    if( layer instanceof BufferLayer )
                        ((BufferLayer)layer).clearBuffer();
            }
        }
    }

    // this will be called on loadFinished (if any)
    private void initTrackData(Assignment currentTask){

        clearMapLayers();
        mCurrentTask = currentTask;

        if(currentTask == null) return;

        // TODO šito vajag novākt
        mControlPoints = currentTask.getControlPoints();

        List<MultiGeoPoint> trackPoints = currentTask.getTrackPoints();

        // TODO multipart šeit!!
        if( trackPoints != null && trackPoints.size() > 0) {

            //int currentPart = currentTask.getCurrentPart();
            //BufferLayer newBuffer = new BufferLayer(mMap, new LineStyle(JeoUtils.color(RGB.gray), 2.0f), getAreaStyle(), buffSize);
            //mMap.layers().add(newBuffer);
            //newBuffer.addLineBuffer(trackPoints, true);

            mBufferlayer.addLineBuffer(trackPoints, true);

            updateStartStop();  // set start point
            //setMapPosition(currentPoints);
        }

        drawControlPointLocationsToMap();
        Timber.i("TrackInit ok");

        if(mCurrentTask != null)
            setMapPosition(mCurrentTask.getTaskLocation());
    }

    private void updateStartStop(){

        // novāc start/stop markerus
        markerLayer.removeAllItems();

        if(mCurrentTrack!= null && mCurrentTrack.getTrackPoints().size() > 0) {
            MultiGeoPoint startPoint = mCurrentTrack.getStartPoint();
            if(startPoint != null) {
                //Timber.i("Sakums: " + startPoint.getLatitude() + ", " + startPoint.getLongitude());
                markerLayer.addItem(new MarkerItem(mStartText, mStartText, startPoint.getPoint()));
            }
            MultiGeoPoint stopPoint = mCurrentTrack.getStopPoint();
            if(stopPoint != null) {
                //Timber.i("Beigas: " + stopPoint.getLatitude() + ", " + stopPoint.getLongitude());
                markerLayer.addItem(new MarkerItem(mStopText, mStopText, stopPoint.getPoint()));
            }
        }
    }

    private void updateUI(Location loc) {

        if(loc != null) {

            // TODO vajag pareizo lap!!!!
            int lap = -1;
            MultiGeoPoint point = new MultiGeoPoint(loc.getLatitude(), loc.getLongitude(), lap);
            //mPtsTrack.add(new GeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
            if(mCurrentTrack != null)
                mCurrentTrack.addTrackPoint(point);
//            if(pathLayer != null)
//                pathLayer.addPoint(point.getPoint());

            updateStartStop();
            setMapPosition(point.getPoint().getLatitude(),point.getPoint().getLongitude(), -1);
        }

    }

    private void doMapInit(View parent) {

        // ja parent nav MapView, tad vajag parent.findViewById(R.id.mapView);
        mMapView = (MapView) parent;
        mMap = mMapView.map();

        VectorTileLayer baseLayer = null;
        String file = myApp.preferences.getString(Constants.MAP_PATH, "");
        if (file != "") { //
            mTileSource = new MapFileTileSource();
            if (mTileSource.setMapFile(file)) {
                baseLayer = mMap.setBaseMap(mTileSource);
            }
        }
        if (baseLayer == null) { // something wrong - use online map
            Toast.makeText(getActivity(), "Bezsaistes karte nav pieejama", Toast.LENGTH_SHORT).show();
            baseLayer = mMap.setBaseMap(new OSciMap4TileSource());
        }

        mMap.layers().add(new BuildingLayer(mMap, baseLayer));
        mMap.layers().add(new LabelLayer(mMap, baseLayer));

        // pagaidam arcgis tests --------------------------------------------------------------

        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean useDetails = myApp.preferences.getBoolean("use_details_map", false);
        if (useDetails) {
            BitmapTileSource.Builder ARCGIS_VAAD = (BitmapTileSource.Builder) ((BitmapTileSource.builder().url(Constants.ARCGIS_URL)).tilePath("{Z}/{Y}/{X}")).zoomMin(Constants.ARCGIS_MINZOOM).zoomMax(Constants.ARCGIS_MAXZOOM);
            mBitmapTileSource = ARCGIS_VAAD.build();

            if (USE_CACHE) {
                mCache = new TileCache(getActivity(), null, mBitmapTileSource.getClass().getSimpleName());
                mCache.setCacheSize(512 * (1 << 10));
                mBitmapTileSource.setCache(mCache);
            }
            mBitmapLayer = new BitmapTileLayer(mMap, mBitmapTileSource);
            mMap.layers().add(mBitmapLayer);
        }
        //----------------------------------------------------------------------
        // --------------------

        if (myApp.preferences.getBoolean(Constants.BUFFER_FILL_PREF, false))
            areaStyle = this.getAreaStyle();

        buffSize = myApp.preferences.getInt(Constants.BUFFER_SIZE_PREF, Constants.BUFFER_SIZE_M);// * Constants.BUFFER_SIZE_TO_MAP_SIZE;

        mBufferlayer = new BufferLayer(mMap, this.getLineStyle(), areaStyle, buffSize);
        mMap.layers().add(mBufferlayer);

        //-------------------------------------------------------

        useMapRotation = myApp.preferences.getBoolean("use_map_bearing", false);

//        pathLayer = new PathLayer(mMap, Color.RED, 8);
//        mMap.layers().add(pathLayer);

        Bitmap bitmap = drawableToBitmap(getResources(), R.drawable.btn_radio_to_on_mtrl_000);
        MarkerSymbol symbol = new MarkerSymbol(bitmap, MarkerItem.HotspotPlace.CENTER);

        markerLayer = new ItemizedLayer<MarkerItem>(mMap, new ArrayList<MarkerItem>(), symbol, null);
        mMap.layers().add(markerLayer);

        Bitmap bitmapTask = drawableToBitmap(getResources(), R.drawable.location_marker);
        MarkerSymbol symbolTask = new MarkerSymbol(bitmapTask, MarkerItem.HotspotPlace.CENTER);
        taskLayer = new ItemizedLayer<MarkerItem>(mMap, new ArrayList<MarkerItem>(), symbolTask, null);
        mMap.layers().add(taskLayer);

        Bitmap bitmapPoints = drawableToBitmap(getResources(), R.drawable.btn_radio_to_off_mtrl_000);
        MarkerSymbol symbolPoint = new MarkerSymbol(bitmapPoints, MarkerItem.HotspotPlace.CENTER);
        controlPointLayer = new ItemizedLayer<MarkerItem>(mMap, new ArrayList<MarkerItem>(), symbolPoint, null);
        mMap.layers().add(controlPointLayer);

        bitmap = drawableToBitmap(getResources(), R.drawable.btn_radio_on_holo_dark);
        symbol = new MarkerSymbol(bitmap, MarkerItem.HotspotPlace.CENTER);

        currentLocationLayer = new ItemizedLayer<MarkerItem>(mMap, new ArrayList<MarkerItem>(), symbol, null);
        mMap.layers().add(currentLocationLayer);

        mMap.layers().add(new MapScaleBar(mMapView));

        mMap.setTheme(VtmThemes.OSMARENDER);

        if(mCurrentTask != null)
            setMapPosition(mCurrentTask.getTaskLocation());
        else
            setMapPosition(myApp.getLastLocation());

        EventBus.getDefault().post(new MapReadyEvent(true));

    }


    // servisa funkcijas --------------------------------------------------------------

    public void zoomMap(boolean zoomIn){

        if( mMap == null ) return;
        MapPosition p = mMap.getMapPosition();
        int iZoom = p.getZoomLevel();
        if(zoomIn)
            p.setZoomLevel(iZoom + 1);
        else
            p.setZoomLevel(iZoom - 1);
        mMap.setMapPosition(p);

    }

    public void setMapPosition( double lat, double lng, int zoomLevel ){

        if( mMap == null ) return;

        MapPosition p = mMap.getMapPosition();
        p.setPosition(new GeoPoint(lat, lng));
        if(zoomLevel != -1 )
            p.setZoomLevel(zoomLevel);
            //p.setScale(1<<zoomLevel);
        //p.setTilt((float) (Math.random() * 60));
        p.setTilt( 3 );
        mMapView.map().animator().animateTo(1000, p);
    }

    public void setMapPosition(Location loc){

        if( mMapView != null) {

            MapPosition p = mMap.getMapPosition();
            p.setPosition(new GeoPoint(loc.getLatitude(), loc.getLongitude()));
            if(p.getZoomLevel() < 12)
                p.setZoomLevel(17);
            mMapView.map().animator().animateTo(1000, p);
        }
    }

    public void setLocMarker(Location loc){

        taskLayer.removeAllItems();
        if( loc != null )
            taskLayer.addItem(new MarkerItem("Currnt location", "aha", new GeoPoint(loc.getLatitude(), loc.getLongitude())));
    }

    private void setMapPosition(List<GeoPoint> points) {

        // add the polyline to the map
        if(points != null && points.size()> 0 ) {

            MapPosition p = new MapPosition();
            p.setByBoundingBox(BoundingBox.fromGeoPoints(points),
                    Tile.SIZE,
                    Tile.SIZE);
            mMapView.map().animator().animateTo(1000, p);
        }
    }

    // override, lifecycle  --------------------------------------------------------------

    public static TrackMapFragment newInstance(long runId) {
        Bundle args = new Bundle();
        args.putLong(ARG_RUN_ID, runId);
        TrackMapFragment rf = new TrackMapFragment();
        rf.setArguments(args);
        return rf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = ApplicationController.getInstance();

        mPositionText = getResources().getString(R.string.marker_pos_text);
        mStartText = getResources().getString(R.string.marker_start_text);
        mStopText = getResources().getString(R.string.marker_stop_text);
        mPointText = getResources().getString(R.string.marker_point_text);

        currentArgs = getArguments();

        // TODO pie orientācijas maiņas - vajag saglabāt, pārrēķināt, ....
//        if (savedInstanceState != null) {
//
//            // ierakstam
////            if(pathLayer != null ) {
////                ArrayList<GeoPoint> points = (ArrayList<GeoPoint>) savedInstanceState.getSerializable(PATH_LAYER);
////                pathLayer.setPoints(points);
////                Timber.e("punktu skaits nr=" + points.size());
////            }
//        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        EventBus.getDefault().register(this);
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        if(initMap == false) {
            doMapInit(parent);
            initMap = true;
        }
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

//        try {
//            mCallback = (AreaChanged) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " nav implementets AreaChanged");
//        }
    }

//    public void broadcastArea(double area){
//        if(mCallback != null)
//            mCallback.setArea(area*Constants.BUFFER_AREA_TO_HA);
//    }

    @Override
    public void onDetach() {
        //mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
//        getActivity().registerReceiver(mLocationReceiver,
//                new IntentFilter(TrackManager.ACTION_LOCATION));
    }

    @Override
    public void onStop() {
//        getActivity().unregisterReceiver(mLocationReceiver);
        // probably is necessary to save some params
        super.onStop();
    }

    @Override
    public void onPause() {
        //isPaused = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        //isPaused = false;
        // vajag reload cursoru, lai pareizi sazīmē
        //startLoader();

        super.onResume();
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    // laukuma interfeiss - TODO šito vajag atstāt TaskDetailsFragment -----------------------------

    public static AreaStyle getAreaStyle() {
       return new AreaStyle(JeoUtils.color(new RGB( 50, 50, 50, 60)));
    }

    public static LineStyle getLineStyle() {

       return new LineStyle(JeoUtils.color(RGB.red), 4.0f);//2.0f);
    }


}
