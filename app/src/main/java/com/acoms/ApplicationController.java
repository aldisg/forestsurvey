package com.acoms;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.acoms.account.AccountGeneral;
import com.acoms.account.User;
import com.acoms.database.AssignmentDatabaseContract;
import com.acoms.database.ClassifsDAO;
import com.acoms.database.ClassifsDatabaseContract;
import com.acoms.model.Assignment;
import com.acoms.model.Constants;
import com.acoms.model.UserCredentials;
import com.acoms.utils.FileLoggingTree;
import com.acoms.utils.GPSService;

import timber.log.Timber;


/**
 * Created by AldisGrauze on 10.02.2016.
 */
public class ApplicationController extends Application {

    private static ApplicationController instance;

    public static SharedPreferences preferences;
    public static ClassifsDAO classifs;

    private static User user = null;
    private Account mConnectedAccount;

    public static String APP_PREFERENCES_NAME = Constants.APP_PREFERENCES;
    private Assignment lastTask;
    private Location lastLocation;

    private boolean bGPSStarted;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        preferences = this.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        classifs = ClassifsDAO.getInstance(this);
        classifs.initClassifs(null);

        user = new User("", new UserCredentials());

//        if(BuildConfig.DEBUG)
//            Timber.plant(new Timber.DebugTree());

        Timber.plant(new FileLoggingTree(this));
        bGPSStarted = false;
    }

    public boolean startGPSService(){

        if( !bGPSStarted ) {
            Intent intent = new Intent(this, GPSService.class);
            startService(intent);
            bGPSStarted = true;
            return true;
        }
        return false;
    }

    public boolean stopGPSService(){
        if( bGPSStarted ) {
            Intent intent = new Intent(this, GPSService.class);
            stopService(intent);
            bGPSStarted = false;
            return true;
        }
        return false;
    }

    public static synchronized ApplicationController getInstance() {
        return instance;
    }

    public User getUser() {
        return user;
    }

//    public void setUser(String name, String id) {
//
//        user.setUsername(name);
//        user.setObjectId(id);
//    }

    public Account getConnectedAccount() {
        return mConnectedAccount;
    }

    public void setConnectedAccount(Account mConnectedAccount) {
        this.mConnectedAccount = mConnectedAccount;
        if (mConnectedAccount != null) {
            //user.setUsername(AccountManager.get(this).getUserData(mConnectedAccount, AccountGeneral.ACCOUNT_NAME));
            user.setUsername(mConnectedAccount.name);
            user.setObjectId(AccountManager.get(this).getUserData(mConnectedAccount, AccountGeneral.USERDATA_USER_OBJ_ID));
            schedulePeriodicSync();
        }
    }

    public void schedulePeriodicSync() {

        ContentResolver resolver = getContentResolver();
        Account mAccount = ApplicationController.getInstance().getConnectedAccount();
        if (mAccount != null)

            //Turn on periodic syncing
            cancelOtherAccounts(mAccount);

            resolver.setIsSyncable(mAccount, AssignmentDatabaseContract.AUTHORITY, 1);
            resolver.setSyncAutomatically(mAccount, AssignmentDatabaseContract.AUTHORITY, true);

            resolver.addPeriodicSync(
                    mAccount,
                    AssignmentDatabaseContract.AUTHORITY,
                    Bundle.EMPTY,
                    getPrefInt("sync_frequency", Constants.DEF_SYNC_FREQUENCY_MINS) * 60);

            resolver.setIsSyncable(mAccount, ClassifsDatabaseContract.AUTHORITY, 1);
            resolver.setSyncAutomatically(mAccount, ClassifsDatabaseContract.AUTHORITY, true);

            resolver.addPeriodicSync(
                    mAccount,
                    ClassifsDatabaseContract.AUTHORITY,
                    Bundle.EMPTY,
                    getPrefInt("sync_frequency", Constants.DEF_SYNC_FREQUENCY_MINS) * 60);
    }

    private void cancelOtherAccounts(Account newAccount) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            Timber.e("Missing GET_ACCOUNTS permission");
            return;
        }
        Account[] accounts = AccountManager.get(this).getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
        for (Account account : accounts) {
            if(!TextUtils.equals(account.name, newAccount.name ))
                cancelSync(account);
        }
    }

    private void cancelSync(Account account){

        ContentResolver.cancelSync(account, AssignmentDatabaseContract.AUTHORITY);
        ContentResolver.cancelSync(account, ClassifsDatabaseContract.AUTHORITY);
        int syncOnOff = 0;
        ContentResolver.setIsSyncable(account, AssignmentDatabaseContract.AUTHORITY, syncOnOff);
        ContentResolver.setIsSyncable(account, ClassifsDatabaseContract.AUTHORITY, syncOnOff);
    }

    private int getPrefInt(String key, int defVal){

        int iRet;
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String value = prefs.getString(key, null);
            if( value != null )
                iRet = Integer.valueOf(value);
            else iRet =defVal;

            return iRet;
        }
        catch (Exception e ){
            e.printStackTrace();
        }
        return defVal;
    }

    private String getLogDirectory(){

        return getExternalFilesDir(null) + "/logs";
    }

    public void saveLastLocation(Location loc) {

        preferences. edit().putFloat(Constants.LAST_POS_LAT, (float)loc.getLatitude()).apply();
        preferences. edit().putFloat(Constants.LAST_POS_LNG, (float)loc.getLongitude()).apply();
        this.lastLocation = loc;
    }
    public Location getLastLocation() {
        Location loc = new Location("");
        loc.setLatitude(preferences.getFloat(Constants.LAST_POS_LAT, Constants.DEF_LAT));
        loc.setLongitude(preferences.getFloat(Constants.LAST_POS_LNG, Constants.DEF_LNG));
        loc.setAccuracy(250.0f); // lai nepieliktu punktu apsekojumā - tb. nesavienotu poligonus ar last known location.
        return  loc;
    }

    public float getLastLattitude(){
        return preferences.getFloat(Constants.LAST_POS_LAT, Constants.DEF_LAT);
    }

    public float getLastLongitude(){
        return preferences.getFloat(Constants.LAST_POS_LAT, Constants.DEF_LNG);
    }


//    public void setLastTask(Assignment lastTask) {
//        this.lastTask = lastTask;
//    }
//    public Assignment getLastTask() {
//        return this.lastTask;
//    }
}