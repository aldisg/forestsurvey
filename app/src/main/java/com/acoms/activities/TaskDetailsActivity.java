package com.acoms.activities;

import android.support.v4.app.Fragment;

import com.acoms.fragments.TaskDetailsFragment;
import com.acoms.koanketa.R;
import com.acoms.koanketa.TrackMapFragment;
import com.acoms.koanketa.SingleFragmentActivity;

/**
 * Created by A on 4/14/2016.
 */
public class TaskDetailsActivity extends SingleFragmentActivity
//        implements TrackMapFragment.AreaChanged
{

    /**
     * A key for passing a run ID as a longrunData
     */
    public static final String EXTRA_RUN_ID = "RUN_ID";
    private TaskDetailsFragment mTaskDetailsFragment;

    @Override
    protected Fragment createFragment() {

        long runId = getIntent().getLongExtra(EXTRA_RUN_ID, -1);
        mTaskDetailsFragment = TaskDetailsFragment.newInstance(runId);
        return mTaskDetailsFragment;
    }

    @Override
    protected int getLayoutResId(){
        return R.layout.test_layout;
    }


//    @Override
//    public void setArea(double area) {
//        if(mTaskDetailsFragment != null)
//            mTaskDetailsFragment.setArea(area);
//    }
}
