package com.acoms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.acoms.model.Classifs;
import com.acoms.model.ControlTypes;
import com.acoms.model.DangerousSpecie;
import com.acoms.model.Samples;
import com.acoms.model.Specie;
import com.acoms.model.SpeciesControl;
import com.acoms.model.Units;
import com.acoms.utils.StringIntArrayUtils;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aldis Grauze on 15.18.12.
 */
public class ClassifsDAO {

    private static ClassifsDAO sInstance = null;
    private DatabaseHelper dbHelper;

    private List<ControlTypes> controlTypes;
    private List<DangerousSpecie> dangerousSpecies;
    private List<Specie> cSpecies;
    private List<Units> unitTypes;
    private List<Samples> sampleTypes;

    public static ClassifsDAO getInstance(Context context){
        if( sInstance == null && context != null ){
            sInstance = new ClassifsDAO(context);
        }
        return sInstance;
    }

    private ClassifsDAO(Context context){
        dbHelper = DatabaseHelper.getHelper(context);
    }

    public boolean initClassifs(Classifs newClassifs){
        if(newClassifs != null ) {

            List<ControlTypes> cNewTypes = newClassifs.getControlList();
            if( cNewTypes!=null ) storeControlTypes(cNewTypes);

            List<DangerousSpecie> dSpecies = newClassifs.getDangerousSpeciesList();
            if( dSpecies != null ) storeDangerousSpecies(dSpecies);

            List<Specie> species = newClassifs.getSpeciesList();
            if( species != null ) storeSpecies(species);

            List<Units> units = newClassifs.getUnitsList();
            if( units != null ) storeUnitTypes(units);

            List<Samples> samples = newClassifs.getSamplesList();
            if( samples != null ) storeSampleTypes(samples);

        }

        controlTypes = readControlTypes();
        dangerousSpecies = readDangerousSpecies();
        cSpecies = readSpecies();
        unitTypes = readUnitTypes();
        sampleTypes = readSampleTypes();

        return true;
    }

    // Species funkcijas ------------------------------------
    private boolean storeSpecies(List<Specie> speciesList ){

        try {
            List<Specie> oldSpecies = readSpecies();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();

            for (Specie ds : speciesList) {

                boolean isInDB = false;
                boolean keepInDB = true;
                for(Specie dsOld : oldSpecies ) {
                    if (dsOld.getId() == ds.getId()){
                        isInDB = true;
                        keepInDB = ds.isActive();  //
                        break;
                    }
                }
                if( !isInDB && ds.isActive()){

                    ContentValues cv = new ContentValues();
                    cv.put(ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ID, ds.getId());
                    cv.put(ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_NAME, ds.getName());
                    cv.put(ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ACTIVE, ds.getActive());

                    // store related ids as ; separated string
                    String dangerousSpecIds = "";
                    if (ds.speciesControlList.size() > 0) {
                        String[] speciesIds = new String[ds.speciesControlList.size()];
                        int curr = 0;
                        for (SpeciesControl contr : ds.speciesControlList) {
                            //if (contr.active == 1) {
                                String sID = Integer.toString(contr.dangerousID);
                                if (!StringIntArrayUtils.contains(speciesIds, sID)) {
                                    speciesIds[curr++] = sID;
                                }
                            //}
                        }
                        dangerousSpecIds = StringIntArrayUtils.join(";", speciesIds);
                    }
                    cv.put(ClassifsDatabaseContract.SpeciesClassifTable.DANGEROUS_SPECIES_IDS, dangerousSpecIds);
                    //db.insertWithOnConflict(ClassifsDatabaseContract.SpeciesClassifTable.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                    db.replace(ClassifsDatabaseContract.SpeciesClassifTable.TABLE_NAME, null, cv);
                }
                else {
                    if(!keepInDB)
                        db.delete(ClassifsDatabaseContract.SpeciesClassifTable.TABLE_NAME, ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ID + "=" + String.valueOf(ds.getId()), null);
                }
            }

            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
        catch (Exception e){
            return false;
        }
        return true;
    }

    private List<Specie> readSpecies(){

        List<Specie> list = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(ClassifsDatabaseContract.SpeciesClassifTable.TABLE_NAME, null, null, null, null, null, null );

        cursor.moveToFirst();
        while(cursor.moveToNext()){

            int active  = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ACTIVE));
            //if( active == 1 ) { // should not be

                int id = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ID));
                String name = cursor.getString(cursor.getColumnIndex(ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_NAME));
                String dsIds = cursor.getString(cursor.getColumnIndex(ClassifsDatabaseContract.SpeciesClassifTable.DANGEROUS_SPECIES_IDS));
                int[] dangerousPecies = StringIntArrayUtils.StringToIntArr(dsIds);
                list.add(new Specie(id, name, dangerousPecies));
            //}
        }
        cursor.close();
        db.close();


        return list;
    }

    // Uses SQLiteQueryBuilder to query multiple tables
    public ArrayList<SpeciesControl> getSpecieControlIds(SQLiteDatabase db, int specieID) {

        ArrayList<SpeciesControl> controlSpecies = new ArrayList<>();
        return controlSpecies;
    }

    // dangerousSpecies funkcijas ------------------------------------
    private boolean storeDangerousSpecies(List<DangerousSpecie> speciesList){

        try {
            List<DangerousSpecie> oldSpecies = readDangerousSpecies();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();

            for (DangerousSpecie ds : speciesList) {

                boolean isInDB = false;
                boolean keepInDB = true;
                for(DangerousSpecie dsOld : oldSpecies ) {
                    if (dsOld.getName().equals(ds.getName())){
                        isInDB = true;
                        keepInDB = ds.isActive();  //
                        break;
                    }
                }
                if( !isInDB && ds.isActive()) {
                    ContentValues cv = new ContentValues();
                    cv.put(ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_ID, ds.getId());
                    cv.put(ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_NAME, ds.getName());
                    //db.insert(ClassifsDatabaseContract.DangerousSpeciesClassifTable.TABLE_NAME, null, cv);
                    db.insertWithOnConflict(ClassifsDatabaseContract.DangerousSpeciesClassifTable.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                }
                else {
                   if(!keepInDB)
                        db.delete(ClassifsDatabaseContract.DangerousSpeciesClassifTable.TABLE_NAME, ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_ID + "=" + String.valueOf(ds.getId()), null);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
        catch (Exception e){
            return false;
        }
        return true;
    }

    private List<DangerousSpecie> readDangerousSpecies(){

        List<DangerousSpecie> list = new ArrayList<DangerousSpecie>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(ClassifsDatabaseContract.DangerousSpeciesClassifTable.TABLE_NAME, null, null, null, null, null, null );

        cursor.moveToFirst();
        while(cursor.moveToNext()){

            int id = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_ID));
            String name = cursor.getString(cursor.getColumnIndex(ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_NAME));
            DangerousSpecie specie = new DangerousSpecie(id, name);
            list.add(specie);
        }
        cursor.close();
        db.close();

        // TODO pagaidām helps nav, jo nevar deseralizēt

        return list;
    }

    // controlTypes funkcijas ------------------------------------
    private boolean storeControlTypes(List<ControlTypes> controlsList){

        try {
            List<ControlTypes> oldTypes = readControlTypes();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();

            for (ControlTypes ct : controlsList) {

                boolean isInDB = false;
                boolean keepInDB = true;
                for(ControlTypes ot : oldTypes ) {

                    if (ot.getName().equals(ct.getName())){
                        isInDB = true;
                        keepInDB = ct.isActive();  //
                        break;
                    }
                }
                if( !isInDB && ct.isActive()) {

                    ContentValues cv = new ContentValues();
                    cv.put(ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ID, ct.getId());
                    cv.put(ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_NAME, ct.getName());
                    cv.put(ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ACTIVE, ct.getActive());
                    //db.insert(ClassifsDatabaseContract.ControlTypesClassifTable.TABLE_NAME, null, cv);
                    db.insertWithOnConflict(ClassifsDatabaseContract.ControlTypesClassifTable.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                }
                else {
                    if(!keepInDB)
                        db.delete(ClassifsDatabaseContract.ControlTypesClassifTable.TABLE_NAME, ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ID + "=" + String.valueOf(ct.getId()), null);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
        catch (Exception e){
            return false;
        }
        return true;
    }

    private List<ControlTypes> readControlTypes(){

        List<ControlTypes> list = new ArrayList<ControlTypes>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(ClassifsDatabaseContract.ControlTypesClassifTable.TABLE_NAME, null, null, null, null, null, null );

        cursor.moveToFirst();
        while(cursor.moveToNext()){

            int id = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ID));
            String name = cursor.getString(cursor.getColumnIndex(ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_NAME));
            int active = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ACTIVE));
            //if( active == 1 ) {
                ControlTypes type = new ControlTypes(id, name, active);
                list.add(type);
            //}
        }
        cursor.close();
        db.close();

        return list;
    }

    // unitTypes funkcijas ------------------------------------
    private boolean storeUnitTypes(List<Units> unitsList){

        try {
            List<Units> oldUnits = readUnitTypes();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();

            for (Units un : unitsList) {

                boolean isInDB = false;
                boolean keepInDB = true;
                for(Units ot : oldUnits ) {
                    if (ot.getId() == un.getId()){
                        isInDB = true;
                        keepInDB = un.isActive();
                        break;
                    }
                }
                if( !isInDB && un.isActive()) {
                    ContentValues cv = new ContentValues();
                    cv.put(ClassifsDatabaseContract.UnitClassifTable.UNIT_ID, un.getId());
                    cv.put(ClassifsDatabaseContract.UnitClassifTable.UNIT_NAME, un.getName());
                    cv.put(ClassifsDatabaseContract.UnitClassifTable.UNIT_ACTIVE, un.getActive());
                    //db.insert(ClassifsDatabaseContract.UnitClassifTable.TABLE_NAME, null, cv);
                    db.insertWithOnConflict(ClassifsDatabaseContract.UnitClassifTable.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                }
                else {
                    if (!keepInDB)
                        db.delete(ClassifsDatabaseContract.UnitClassifTable.TABLE_NAME, ClassifsDatabaseContract.UnitClassifTable.UNIT_ID + "=" + String.valueOf(un.getId()), null);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
        catch (Exception e){
            return false;
        }
        return true;
    }

    private List<Units> readUnitTypes(){

        List<Units> list = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(ClassifsDatabaseContract.UnitClassifTable.TABLE_NAME, null, null, null, null, null, null );

        cursor.moveToFirst();
        while(cursor.moveToNext()){

            int id = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.UnitClassifTable.UNIT_ID));
            String name = cursor.getString(cursor.getColumnIndex(ClassifsDatabaseContract.UnitClassifTable.UNIT_NAME));
            int active = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.UnitClassifTable.UNIT_ACTIVE));
            //if( active == 1 ) {
                Units type = new Units(id, name, active);
                list.add(type);
            //}
        }
        cursor.close();
        db.close();

        return list;
    }

    // sampleTypes funkcijas ------------------------------------
    private boolean storeSampleTypes(List<Samples> samplesList){

        try {
            List<Samples> oldSamples = readSampleTypes();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();

            for (Samples un : samplesList) {

                boolean isInDB = false;
                boolean keepInDB = true;
                for(Samples ot : oldSamples ) {
                    if (ot.getId() == un.getId()){
                        isInDB = true;
                        keepInDB = un.isActive();
                        break;
                    }
                }
                if( !isInDB && un.isActive()) {
                    ContentValues cv = new ContentValues();
                    cv.put(ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ID, un.getId());
                    cv.put(ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_NAME, un.getName());
                    cv.put(ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ACTIVE, un.getActive());
                    //db.insert(ClassifsDatabaseContract.SamplesClassifTable.TABLE_NAME, null, cv);
                    db.insertWithOnConflict(ClassifsDatabaseContract.SamplesClassifTable.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                }
                else {
                    if (!keepInDB)
                        db.delete(ClassifsDatabaseContract.SamplesClassifTable.TABLE_NAME, ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ID + "=" + String.valueOf(un.getId()), null);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
        catch (Exception e){
            return false;
        }
        return true;
    }

    private List<Samples> readSampleTypes(){

        List<Samples> list = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(ClassifsDatabaseContract.SamplesClassifTable.TABLE_NAME, null, null, null, null, null, null );

        cursor.moveToFirst();
        while(cursor.moveToNext()){

            int id = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ID));
            String name = cursor.getString(cursor.getColumnIndex(ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_NAME));
            int active = cursor.getInt(cursor.getColumnIndex(ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ACTIVE));
            //if( active == 1 ) {
                Samples type = new Samples(id, name, active);
                list.add(type);
            //}
        }
        cursor.close();
        db.close();

        return list;
    }

    // public get set functions ------------------------------------------

    public List<DangerousSpecie> getDangerousSpecies() {
        return dangerousSpecies;
    }

    public List<ControlTypes> getControlTypes() {
        return controlTypes;
    }

    public List<Specie> getSpecies() {
        return cSpecies;
    }

    public List<Units> getUnits() {
        return unitTypes;
    }

    public List<Samples> getSamples() {
        return sampleTypes;
    }

    // testa json no assets
    public Classifs getTestSpecies(Context context, String jsonFile){

        String sJson = getMockJson(context, jsonFile);
        // deserializācija
//        Gson gson = new GsonBuilder()
//                //.disableHtmlEscaping()
//                //.registerTypeAdapter(SpeciesControl.class, new SoapDeserializer<>(SpeciesControl.class))
//                //.registerTypeAdapter(Classifs.class, new SoapDeserializer<>(Classifs.class))
//                //.registerTypeAdapter(SpeciesControl.class, new KOSpecieDeserializer())
//                  .create();

        Gson gson = new Gson();
        Classifs classifs = gson.fromJson(sJson, Classifs.class);

        initClassifs(classifs);
        return classifs;
    }

    // testa JSON no assets faila --------------------------------
    private String getMockJson(Context context, String filename){

        String json = null;
        try {

            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
