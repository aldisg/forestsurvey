package com.acoms.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Aldis Grauze on 15.18.12.
 */
public class ClassifsDatabaseHelper {//extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String FOREIGN_KEY = " INTEGER";
    private static final String COMMA = " ,";

    // Control Types ------------------------------------------------------------
    public static final String CREATE_CONTROLTYPES_CLASSIF_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + ClassifsDatabaseContract.ControlTypesClassifTable.TABLE_NAME + " ( "
            + ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ID + INTEGER_TYPE + PRIMARY_KEY + COMMA
            + ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_NAME + TEXT_TYPE + COMMA
            + ClassifsDatabaseContract.ControlTypesClassifTable.CONTROL_ACTIVE + INTEGER_TYPE
            +  " )";

    public static final String DROP_CONTROLTYPES_CLASSIF_TABLE = "DROP TABLE IF EXISTS "
            + ClassifsDatabaseContract.ControlTypesClassifTable.TABLE_NAME;

    // Dangerous species --------------------------------------------------------
    public static final String CREATE_DANGEROUS_SPECIES_CLASSIF_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + ClassifsDatabaseContract.DangerousSpeciesClassifTable.TABLE_NAME + " ("
            + ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_ID + INTEGER_TYPE + PRIMARY_KEY + COMMA
            + ClassifsDatabaseContract.DangerousSpeciesClassifTable.SPECIE_NAME + TEXT_TYPE
            + " )";

    public static final String DROP_DANGEROUS_SPECIES_CLASSIF_TABLE = "DROP TABLE IF EXISTS "
            + ClassifsDatabaseContract.DangerousSpeciesClassifTable.TABLE_NAME;

    public static final String CREATE_SPECIES_CLASSIF_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + ClassifsDatabaseContract.SpeciesClassifTable.TABLE_NAME + " ( "
            + ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ID + INTEGER_TYPE + PRIMARY_KEY + COMMA
            + ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_ACTIVE + INTEGER_TYPE +  COMMA
            + ClassifsDatabaseContract.SpeciesClassifTable.SPECIE_NAME + TEXT_TYPE +  COMMA
            + ClassifsDatabaseContract.SpeciesClassifTable.DANGEROUS_SPECIES_IDS + TEXT_TYPE
            + " )";

    public static final String CREATE_UNITS_CLASSIF_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + ClassifsDatabaseContract.UnitClassifTable.TABLE_NAME + " ( "
            + ClassifsDatabaseContract.UnitClassifTable.UNIT_ID + INTEGER_TYPE + PRIMARY_KEY + COMMA
            + ClassifsDatabaseContract.UnitClassifTable.UNIT_NAME + TEXT_TYPE +  COMMA
            + ClassifsDatabaseContract.UnitClassifTable.UNIT_ACTIVE + INTEGER_TYPE
            + " )";

    public static final String CREATE_SAMPLES_CLASSIF_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + ClassifsDatabaseContract.SamplesClassifTable.TABLE_NAME + " ( "
            + ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ID + INTEGER_TYPE + PRIMARY_KEY + COMMA
            + ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_NAME + TEXT_TYPE +  COMMA
            + ClassifsDatabaseContract.SamplesClassifTable.SAMPLE_ACTIVE + INTEGER_TYPE
            + " )";

//    private static final String DROP_SPECIES_CONTROL_CLASIF_TABLE ="DROP TABLE IF EXISTS "
//            + ClassifsDatabaseContract.SpeciesControlClassifTable.TABLE_NAME;
    public static final String DROP_SPECIES_CLASSIF_TABLE = "DROP TABLE IF EXISTS "
            + ClassifsDatabaseContract.SpeciesClassifTable.TABLE_NAME;

    public static final String DROP_SAMPLES_CLASSIF_TABLE = "DROP TABLE IF EXISTS "
            + ClassifsDatabaseContract.SamplesClassifTable.TABLE_NAME;

    public static final String DROP_UNITS_CLASSIF_TABLE = "DROP TABLE IF EXISTS "
            + ClassifsDatabaseContract.UnitClassifTable.TABLE_NAME;
    private final DatabaseHelper dbHelper;

    private ClassifsDatabaseHelper(Context context) {
        dbHelper = DatabaseHelper.getHelper(context);
    }

}
