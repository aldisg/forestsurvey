package com.acoms.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.acoms.model.Constants;

import timber.log.Timber;

/**
 * Created by A on 3/13/2016.
 */
public class AssignmentContentProvider  extends ContentProvider {

    public static final UriMatcher URI_MATCHER = buildUriMatcher();
    public static final String PATH = "assignments";
    public static final int PATH_TOKEN = 100;
    public static final String PATH_FOR_ID = "assignments/*";
    public static final int PATH_FOR_ID_TOKEN = 200;

    public static final String DETAILS_PATH = "taskdetails";
    public static final int PATH_TOKEN_DETAILS = 150;
    public static final String DETAILS_PATH_FOR_ID = "taskdetails/*";
    public static final int PATH_FOR_ID_TOKEN_DETAILS = 250;


    // Uri Matcher for the content provider
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = AssignmentDatabaseContract.AUTHORITY;
        matcher.addURI(authority, PATH, PATH_TOKEN);
        matcher.addURI(authority, PATH_FOR_ID, PATH_FOR_ID_TOKEN);
        matcher.addURI(authority, DETAILS_PATH, PATH_TOKEN_DETAILS);
        matcher.addURI(authority, DETAILS_PATH_FOR_ID, PATH_FOR_ID_TOKEN_DETAILS);
        return matcher;
    }

    // Content Provider stuff
    private DatabaseHelper dbHelper;

    @Override
    public boolean onCreate() {
        Context ctx = getContext();
        dbHelper = DatabaseHelper.getHelper(ctx); //new AssignmentDatabaseHelper(ctx);
        return true;
    }

    @Override
    public String getType(Uri uri) {
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case PATH_TOKEN:
                return AssignmentDatabaseContract.CONTENT_TYPE_DIR;
            case PATH_FOR_ID_TOKEN:
                return AssignmentDatabaseContract.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("URI " + uri + " is not supported.");
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            // retrieve tv shows list
            case PATH_TOKEN: {
                SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
                builder.setTables(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME);
                return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            }
            case PATH_FOR_ID_TOKEN: {
                int assignmentId = (int) ContentUris.parseId(uri);
                SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
                builder.setTables(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME);
                builder.appendWhere(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + assignmentId);
                return builder.query(db, projection, selection,selectionArgs, null, null, sortOrder);
            }
            case PATH_FOR_ID_TOKEN_DETAILS: {
                int assignmentId = (int) ContentUris.parseId(uri);
                SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

                //builder.setTables(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME);
                builder.setTables(
                        AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + " LEFT JOIN "
                                + AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME
                                + " ON ("
                                //+ "tblA.b_id"
                                + AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + "." + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID
                                + " = "
                                + AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME + "." + AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID
                                //+ "tblB.id"
                                + ")"
                );

                builder.appendWhere(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + "." + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + assignmentId);
                return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            }
            case PATH_TOKEN_DETAILS: {
                //int assignmentId = (int) ContentUris.parseId(uri);
                SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

                //builder.setTables(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME);
                builder.setTables(
                        AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + " LEFT JOIN "
                                + AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME
                                + " ON ("
                                //+ "tblA.b_id"
                                + AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + "." + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID
                                + " = "
                                + AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME + "." + AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID
                                //+ "tblB.id"
                                + ")"
                );

                //builder.appendWhere(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + "." + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + assignmentId);
                return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            }
            default:
                return null;
        }
    }

//    @Override
//    public Uri insert(Uri uri, ContentValues values) {
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        int token = URI_MATCHER.match(uri);
//        switch (token) {
//            case PATH_TOKEN: {
//                //long id = db.insert(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, null, values);
//                long id = db.insertWithOnConflict(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
//                if (id != -1)
//                    getContext().getContentResolver().notifyChange(uri, null);
//
//                return AssignmentDatabaseContract.CONTENT_URI.buildUpon().appendPath(String.valueOf(id)).build();
//            }
//            default: {
//                throw new UnsupportedOperationException("URI: " + uri + " not supported.");
//            }
//        }
//    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        Timber.i("AssignmentContentProvider insert");
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        // hack lai iegūtu _id
        int _id = (int) contentValues.get(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID);
        contentValues.remove(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID);
        String id = "0";
        if( _id != -1 ) id = String.valueOf(_id);
        else {
            long lid = db.insert(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, null, contentValues);
            id = String.valueOf(lid);
        }

        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case PATH_TOKEN:
                insertOrUpdateById(db, uri, AssignmentDatabaseContract.AssignmentTable.TABLE_NAME,
                        contentValues, AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR);
                getContext().getContentResolver().notifyChange(uri, null, false);
                Uri u = AssignmentDatabaseContract.CONTENT_URI.buildUpon().appendPath(id).build();
                return u;
                        //AssignmentDatabaseContract.CONTENT_URI.buildUpon().appendPath(id).build();

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * In case of a conflict when inserting the values, another update query is sent.
     *
     * @param db     Database to insert to.
     * @param uri    Content provider uri.
     * @param table  Table to insert to.
     * @param values The values to insert to.
     * @param column Column to identify the object.
     * @throws android.database.SQLException
     */
    private void insertOrUpdateById(SQLiteDatabase db, Uri uri, String table,
                                    ContentValues values, String column) throws SQLException {
        try {
            db.insertOrThrow(table, null, values);
        } catch (SQLiteConstraintException e) {
            int nrRows = update(uri, values, column + "=?",
                    new String[]{values.getAsString(column)});
            if (nrRows == 0)
                throw e;
        }
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int token = URI_MATCHER.match(uri);
        int rowsDeleted = -1;
        switch (token) {
            case (PATH_TOKEN):
                rowsDeleted = db.delete(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, selection, selectionArgs);
                break;
            case (PATH_FOR_ID_TOKEN):
                String tvShowIdWhereClause = AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR + "=" + uri.getLastPathSegment();
                if (!TextUtils.isEmpty(selection))
                    tvShowIdWhereClause += " AND " + selection;
                rowsDeleted = db.delete(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, tvShowIdWhereClause, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        // Notifying the changes, if there are any
        if (rowsDeleted != -1)
            getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int count;
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case PATH_TOKEN:
                count = db.update(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, values, where, whereArgs);
                //count = db.updateWithOnConflict(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, values, where, whereArgs, SQLiteDatabase.CONFLICT_IGNORE);
                break;
            case PATH_FOR_ID_TOKEN:
                String segment = uri.getLastPathSegment();
                String whereClause = AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + segment
                        + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : "");
                count = db.update(AssignmentDatabaseContract.AssignmentTable.TABLE_NAME, values, whereClause, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        if (count > 0) {
            // Notify the Context's ContentResolver of the change
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }
}