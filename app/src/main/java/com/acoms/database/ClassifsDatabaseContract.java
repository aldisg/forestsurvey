package com.acoms.database;

import android.net.Uri;

/**
 * Created by Aldis Grauze on 15.17.12.
 */
public class ClassifsDatabaseContract {

    // sinhronizācijas info
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.acoms.classifs";
    public static final String CONTENT_TYPE_DIR = "vnd.android.cursor.dir/vnd.acoms.classifs";

    public static final String AUTHORITY = "com.acoms.survey.provider.classifs";
    // content://<authority>/<path to type>
    //public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/classifs");

    public abstract class SpeciesClassifTable{

        public static final String TABLE_NAME = "species";
        public static final String SPECIE_ID = "_id";
        public static final String SPECIE_NAME = "name";
        public static final String SPECIE_ACTIVE = "active";
        public static final String DANGEROUS_SPECIES_IDS = "dangerous_ids";
    }

    public abstract class DangerousSpeciesClassifTable{

        public static final String TABLE_NAME = "dangerous_species";
        public static final String SPECIE_ID = "_id";
        public static final String SPECIE_NAME = "specie_name";
    }

    public abstract class ControlTypesClassifTable{

        public static final String TABLE_NAME = "control_types";
        public static final String CONTROL_ID = "_id";
        public static final String CONTROL_NAME = "name";
        public static final String CONTROL_ACTIVE = "active";
    }

    public abstract class UnitClassifTable {
        public static final String TABLE_NAME = "units";
        public static final String UNIT_ID = "_id";
        public static final String UNIT_NAME = "name";
        public static final String UNIT_ACTIVE = "active";
    }

    public abstract class SamplesClassifTable {
        public static final String TABLE_NAME = "samples";
        public static final String SAMPLE_ID = "_id";
        public static final String SAMPLE_NAME = "name";
        public static final String SAMPLE_ACTIVE = "active";
    }


}
