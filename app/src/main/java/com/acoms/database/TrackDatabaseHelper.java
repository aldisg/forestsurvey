package com.acoms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.location.Location;
import android.util.Log;

import com.acoms.model.Track;

import java.util.List;

import timber.log.Timber;

/**
 * Created by aldisgrauze on 13.05.2016.
 */
public class TrackDatabaseHelper {

    public static final String CREATE_TRACK_POINT_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + TrackDatabaseContract.TrackPointsTable.TABLE_NAME + " ( "
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_ID + DatabaseContract.INTEGER_TYPE + DatabaseContract.PRIMARY_KEY + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TIMESTAMP +  DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LATITUDE +  DatabaseContract.REAL_TYPE + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LONGITUDE +  DatabaseContract.REAL_TYPE + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_ALTITUDE +  DatabaseContract.REAL_TYPE + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_PROVIDER +  DatabaseContract.CHAR_TYPE + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LEG +  DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TASK_ID  +  DatabaseContract.INTEGER_TYPE
            +  " )";

    public static final String DROP_TRACK_POINT_TABLE = "DROP TABLE IF EXISTS "
            + TrackDatabaseContract.TrackPointsTable.TABLE_NAME;
    private final DatabaseHelper dbHelper;

    public TrackDatabaseHelper(Context context){
        dbHelper = DatabaseHelper.getHelper(context);
    }

    public long insertLocation(long runId, int leg, Location location) {

        ContentValues cv = new ContentValues();
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LATITUDE, location.getLatitude());
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LONGITUDE, location.getLongitude());
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_ALTITUDE, location.getAltitude());
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TIMESTAMP, location.getTime());
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_PROVIDER, location.getProvider());
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LEG, leg);
        cv.put(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TASK_ID, runId);
        return dbHelper.getWritableDatabase().insert(TrackDatabaseContract.TrackPointsTable.TABLE_NAME, null, cv);
    }

    public int insertLocations(long runId, List<Location> locations) {

        int retVal = -1;
        SQLiteDatabase mDb = dbHelper.getWritableDatabase();
        try {
            mDb.beginTransaction();
            // TODO pagaidām nav _id - nevar būt vairāki save!
            SQLiteStatement insert = mDb.compileStatement(TrackDatabaseContract.TrackPointsTable.INSERT_LOCATION_PARAMS);
            for (int i = 0; i < locations.size(); i++) {

                Location item = locations.get(i);
                insert.bindLong(1, item.getTime());
                insert.bindDouble(2, item.getLatitude());
                insert.bindDouble(3, item.getLongitude());
                insert.bindDouble(4, item.getAltitude());
                insert.bindString(5, item.getProvider());
                insert.bindLong(6, runId);
                insert.bindLong(7, Math.round(item.getBearing()));
                insert.execute();
            }
            retVal = locations.size();
            mDb.setTransactionSuccessful();
        }
        catch (Exception e) {
            Timber.e(e, "Insert batch kļūda" );
        }
        finally {
            mDb.endTransaction();
        }
        return retVal;

    }

    public LocationCursor queryLastLocationForRun(long runId) {
        Cursor wrapped = dbHelper.getReadableDatabase().query(TrackDatabaseContract.TrackPointsTable.TABLE_NAME,
                null, // all columns
                TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TASK_ID + " = ?", // limit to the given run
                new String[]{ String.valueOf(runId) },
                null, // group by
                null, // having
                TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TIMESTAMP + " desc", // order by latest first
                "1"); // limit 1
        return new LocationCursor(wrapped);
    }

    public LocationCursor queryLocationsForRun(long runId) {
        Cursor wrapped = dbHelper.getReadableDatabase().query(TrackDatabaseContract.TrackPointsTable.TABLE_NAME,
                null,
                TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TASK_ID + " = ?", // limit to the given run
                new String[]{ String.valueOf(runId) },
                null, // group by
                null, // having
                TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TIMESTAMP + " asc"); // order by timestamp
        Timber.i("vaad queryLocations for runId " + Long.toString(runId) + " nr points = " + wrapped.getCount());
        return new LocationCursor(wrapped);
    }


    public static class LocationCursor extends CursorWrapper {

        public LocationCursor(Cursor c) {
            super(c);
        }

        public Location getLocation() {
            if (isBeforeFirst() || isAfterLast())
                return null;
            // first get the provider out so we can use the constructor
            String provider = getString(getColumnIndex(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_PROVIDER));
            Location loc = new Location(provider);
            // populate the remaining properties
            loc.setLongitude(getDouble(getColumnIndex(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LONGITUDE)));
            loc.setLatitude(getDouble(getColumnIndex(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LATITUDE)));
            loc.setAltitude(getDouble(getColumnIndex(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_ALTITUDE)));
            loc.setTime(getLong(getColumnIndex(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TIMESTAMP)));
            loc.setBearing((float)getLong(getColumnIndex(TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_LEG)));
            return loc;
        }
    }

    public int deleteRun(int parentId) {
        if(parentId == -1 ) return 0;
        int ret = dbHelper.getWritableDatabase().delete(TrackDatabaseContract.TrackPointsTable.TABLE_NAME, TrackDatabaseContract.TrackPointsTable.COLUMN_LOCATION_TASK_ID + " ="+String.valueOf(parentId), null);
        return ret;
    }


}
