package com.acoms.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.acoms.model.Constants;

import timber.log.Timber;

/**
 * Created by aldisgrauze on 13.05.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 3; // 2 ir versijai bez picture!!
    private static final String DB_NAME = "assignments.sqlite";

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    private static DatabaseHelper instance;
    public static synchronized DatabaseHelper getHelper(Context context) {
        if (instance == null)
            instance = new DatabaseHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(AssignmentDatabaseHelper.CREATE_ASSIGNMENTS_TABLE);
        db.execSQL(AssignmentDatabaseHelper.CREATE_CONTROL_POINTS_TABLE);
        db.execSQL(TrackDatabaseHelper.CREATE_TRACK_POINT_TABLE);

        db.execSQL(ClassifsDatabaseHelper.CREATE_SPECIES_CLASSIF_TABLE);
        db.execSQL(ClassifsDatabaseHelper.CREATE_CONTROLTYPES_CLASSIF_TABLE);
        db.execSQL(ClassifsDatabaseHelper.CREATE_DANGEROUS_SPECIES_CLASSIF_TABLE);
        db.execSQL(ClassifsDatabaseHelper.CREATE_UNITS_CLASSIF_TABLE);
        db.execSQL(ClassifsDatabaseHelper.CREATE_SAMPLES_CLASSIF_TABLE);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {}



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // secīgi upgrade pēc kārtas
        switch (oldVersion)
        {
            case 1:
                Timber.i("DB upgrade uz 1.versiju");
                db.execSQL("ALTER TABLE " + AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + " ADD COLUMN " + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_AREA + DatabaseContract.FLOAT_TYPE);
            case 2:
                Timber.i("DB upgrade uz 2.versiju");
                db.execSQL("ALTER TABLE " + AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME + " ADD COLUMN " + AssignmentDatabaseContract.ControlPointsTable.CONTROL_PICTURES + DatabaseContract.TEXT_TYPE);
        }

    }
}
