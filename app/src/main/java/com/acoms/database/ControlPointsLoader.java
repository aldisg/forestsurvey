package com.acoms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.CursorLoader;

/**
 * Created by AldisGrauze on 16.20.4.
 */
public class ControlPointsLoader extends CursorLoader {

    private DatabaseHelper dbHelper;
    private int id;

    public ControlPointsLoader(Context context, int id) {
        super(context);
        dbHelper = DatabaseHelper.getHelper(context);//new AssignmentDatabaseHelper(context);
        this.id = id;
    }

    @Override
    public Cursor loadInBackground() {

        Cursor cursor = null;
        try {
            String where = AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID + "=" + String.valueOf(this.id);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            cursor = db.query(AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME,
                                     AssignmentDatabaseContract.ControlPointsProjectionAll,
                    where, null, null, null, null );
            //TimeUnit.SECONDS.sleep(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }

}