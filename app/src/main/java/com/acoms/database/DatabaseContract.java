package com.acoms.database;

/**
 * Created by aldisgrauze on 14.05.2016.
 */
public abstract class DatabaseContract {

    public static final String CHAR_TYPE = " CHAR(52)";
    public static final String TEXT_TYPE = " TEXT";
    public static final String INTEGER_TYPE = " INTEGER";
    public static final String DATE_TYPE = " INTEGER"; // NB date is integer - miliseconds from ...
    public static final String FLOAT_TYPE = " FLOAT";
    public static final String PRIMARY_KEY = " PRIMARY KEY";
    public static final String FOREIGN_KEY = " INTEGER";
    public static final String COMMA = " ,";
    public static final String REAL_TYPE = " REAL";
}
