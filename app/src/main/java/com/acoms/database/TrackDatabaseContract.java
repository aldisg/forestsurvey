package com.acoms.database;

/**
 * Created by aldisgrauze on 13.05.2016.
 */
public class TrackDatabaseContract {

    public abstract class TrackPointsTable{

        public static final String TABLE_NAME = "location";
        public static final String COLUMN_LOCATION_ID = "_id";

        public static final String COLUMN_LOCATION_TIMESTAMP = "timestamp";

        public static final String COLUMN_LOCATION_LATITUDE = "latitude";
        public static final String COLUMN_LOCATION_LONGITUDE = "longitude";
        public static final String COLUMN_LOCATION_ALTITUDE = "altitude";

        public static final String COLUMN_LOCATION_PROVIDER = "provider";
        public static final String COLUMN_LOCATION_TASK_ID = "run_id";
        public static final String COLUMN_LOCATION_LEG = "leg";

        public static final String INSERT_LOCATION_PARAMS = "Insert or Replace into "
                + TABLE_NAME
                + " ("
                + COLUMN_LOCATION_TIMESTAMP
                + DatabaseContract.COMMA
                + COLUMN_LOCATION_LATITUDE
                + DatabaseContract.COMMA
                + COLUMN_LOCATION_LONGITUDE
                + DatabaseContract.COMMA
                + COLUMN_LOCATION_ALTITUDE
                + DatabaseContract.COMMA
                + COLUMN_LOCATION_PROVIDER
                + DatabaseContract.COMMA
                + COLUMN_LOCATION_TASK_ID
                + DatabaseContract.COMMA
                + COLUMN_LOCATION_LEG
                + ") "
                +" values(?,?,?,?,?,?,?)";
    }


}
