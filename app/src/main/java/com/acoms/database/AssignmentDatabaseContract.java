package com.acoms.database;

import android.net.Uri;

/**
 * Created by A on 3/13/2016.
 */
public class AssignmentDatabaseContract {

    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.acoms.assignment";
    public static final String CONTENT_TYPE_DIR = "vnd.android.cursor.dir/vnd.acoms.assignment";

    public static final String AUTHORITY = "com.acoms.survey.provider";
    // content://<authority>/<path to type>
    public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/assignments");
    public static final Uri CONTENT_ITEM_URI = Uri.parse("content://"+AUTHORITY+"/assignments/");

    public static final String CONTENT_DETAILS_ITEM_TYPE = "vnd.android.cursor.item/vnd.acoms.taskdetails";
    public static final String CONTENT_DETAILS_TYPE_DIR = "vnd.android.cursor.dir/vnd.acoms.taskdetails";


//    0 - jauns
//    1 - atkārtots
//    2 - izpildē
//    3 - izpildīts
//    4 - pieņemts
//    10 - Anulēts


    public static final int ASSIGNMENT_RECIEVED = 0;
    public static final int ASSIGNMENT_REASSIGNED = 1;
    public static final int ASSIGNMENT_IN_PROCESSING = 2;
    public static final int ASSIGNMENT_DONE = 3;
    public static final int ASSIGNMENT_SYNCED = 4;
    public static final int ASSIGNMENT_CREATED = 5;
    public static final int ASSIGNMENT_CANCELED = 10;


    public static final String DB_NAME="assignments.sqlite";

    public abstract class AssignmentTable{

        public static final String TABLE_NAME = "assignments";
        public static final String ASSIGNMENT_ID = "_id";
        public static final String ASSIGNMENT_NR = "number";
        public static final String ASSIGNMENT_SEQUENCE_NR = "currentnumber";
        public static final String ASSIGNMENT_NAME = "name";
        public static final String ASSIGNMENT_STATE = "state";
        public static final String ASSIGNMENT_TYPE = "type";
        public static final String ASSIGNMENT_TRACK = "track";
        public static final String ASSIGNMENT_DESCR = "description";
        public static final String ASSIGNMENT_DATE = "date";
        public static final String ASSIGNMENT_DATETO = "dateTo";
        public static final String ASSIGNMENT_ASSIGNER = "assigner";
        public static final String ASSIGNMENT_EXECUTOR = "executor";
        public static final String ASSIGNMENT_AREA = "area";
        //        public static final String SPECIE_ACTIVE = "active";
    }

    public static final String[] AssignmentTableProjectionList = {
              AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK
    };

    public static final String[] AssignmentTableProjectionAll = {
              AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_SEQUENCE_NR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATE
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ASSIGNER
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_EXECUTOR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DESCR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TYPE
    };

    public static final String[] AssignmentTableProjectionAllDetails = {
            AssignmentTable.TABLE_NAME + "."+ AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_SEQUENCE_NR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATE
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ASSIGNER
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_EXECUTOR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DESCR
            , AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TYPE
    };


    public abstract class ControlPointsTable{

        public static final String TABLE_NAME = "control_points";
        //public static final String CONTROL_ID = "_id";
        public static final String CONTROL_ID = "id";
        public static final String CONTROL_ASSIGNMENT_ID = "assignment_id";
        public static final String CONTROL_TYPE = "sampletype";       // tipa Meza apsekojumi
        public static final String CONTROL_SPECIE = "specie";
        public static final String CONTROL_SAMPLE = "sample";   // parauga veids
        public static final String CONTROL_AMOUNT = "amount";
        public static final String CONTROL_UNITS = "units";
        public static final String CONTROL_COMMENTS = "comments";
        public static final String CONTROL_LAT = "lat";
        public static final String CONTROL_LNG = "lng";
        public static final String CONTROL_DANGEROUS_SPECIES = "dangerous_species"; // comma separated dangerous species
        public static final String CONTROL_PICTURES = "pictures";
        //        public static final String SPECIE_ACTIVE = "active";
    }

    public static final String[] ControlPointsProjectionAll = {
            AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_SPECIE
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_TYPE
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_UNITS
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_AMOUNT
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_COMMENTS
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_DANGEROUS_SPECIES
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_LAT
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_LNG
            , AssignmentDatabaseContract.ControlPointsTable.CONTROL_PICTURES
    };

}


