package com.acoms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by A on 3/13/2016.
 */
//public class AssignmentDatabaseHelper extends SQLiteOpenHelper {
public class AssignmentDatabaseHelper{

    //private static final int DB_VERSION = 1;


//    id INTEGER PRIMARY KEY AUTO_INCREMENT,
//    player_id INTEGER REFERENCES players ON DELETE CASCADE,
//    map_id INTEGER REFERENCES maps ON DELETE CASCADE,

    // Assignments ------------------------------------------------------------
    public static final String CREATE_ASSIGNMENTS_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + AssignmentDatabaseContract.AssignmentTable.TABLE_NAME + " ( "
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + DatabaseContract.INTEGER_TYPE + DatabaseContract.PRIMARY_KEY + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NR + DatabaseContract.CHAR_TYPE + " UNIQUE " +  DatabaseContract.COMMA // UNIQUE ?
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_SEQUENCE_NR + DatabaseContract.CHAR_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TYPE + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ASSIGNER + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DESCR + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATE + DatabaseContract.DATE_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO + DatabaseContract.DATE_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_EXECUTOR + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_AREA + DatabaseContract.FLOAT_TYPE //+ DatabaseContract.COMMA
//            + AssignmentDatabaseContract.AssignmentTable.CONTROL_ACTIVE + INTEGER_TYPE
            +  " )";

    public static final String DROP_ASSIGNMENTS_TABLE = "DROP TABLE IF EXISTS "
            + AssignmentDatabaseContract.AssignmentTable.TABLE_NAME;

    // Control Points ------------------------------------------------------------
    public static final String CREATE_CONTROL_POINTS_TABLE = "CREATE TABLE IF NOT EXISTS "//"CREATE TABLE "
            + AssignmentDatabaseContract.ControlPointsTable.TABLE_NAME + " ( "
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_ID + DatabaseContract.INTEGER_TYPE + DatabaseContract.PRIMARY_KEY + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_ASSIGNMENT_ID + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_TYPE + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_SPECIE + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_SAMPLE + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_AMOUNT + DatabaseContract.FLOAT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_UNITS + DatabaseContract.INTEGER_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_COMMENTS + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_DANGEROUS_SPECIES + DatabaseContract.TEXT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_LAT + DatabaseContract.FLOAT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_LNG + DatabaseContract.FLOAT_TYPE + DatabaseContract.COMMA
            + AssignmentDatabaseContract.ControlPointsTable.CONTROL_PICTURES + DatabaseContract.TEXT_TYPE
            +  " )";


    public static final String DROP_CONTROL_POINTS_TABLE = "DROP TABLE IF EXISTS "
            + AssignmentDatabaseContract.AssignmentTable.TABLE_NAME;

    private final DatabaseHelper dbHelper;

    public AssignmentDatabaseHelper(Context context) {
        dbHelper = DatabaseHelper.getHelper(context);
    }




}