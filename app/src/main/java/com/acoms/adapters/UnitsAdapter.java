package com.acoms.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.acoms.model.Specie;
import com.acoms.model.Units;

import java.util.List;

/**
 * Created by A on 3/31/2016.
 */
public class UnitsAdapter extends ArrayAdapter<Units> {

    Context mContext;
    int layoutResourceId;
    //ControlPoint data[] = null;
    List<Units> data = null;

    public UnitsAdapter(Context mContext, int layoutResourceId, List<Units> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){

            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        Units objectItem = data.get(position);
        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(android.R.id.text1);
        textViewItem.setText(String.valueOf(objectItem.getName()));

        textViewItem.setTag(objectItem);
        return convertView;
    }

}
