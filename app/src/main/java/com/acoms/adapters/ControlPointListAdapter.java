package com.acoms.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.acoms.koanketa.R;
import com.acoms.model.ControlPoint;

import java.util.List;

/**
 * Created by aldisgrauze on 22.02.2016.
 */
    public class ControlPointListAdapter extends ArrayAdapter<ControlPoint> {

        Context mContext;
        int layoutResourceId;
        //ControlPoint data[] = null;
        List<ControlPoint> data = null;

        public ControlPointListAdapter(Context mContext, int layoutResourceId, List<ControlPoint> data) {
            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null){

                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            ControlPoint objectItem = data.get(position);
            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
            //textViewItem.setText(String.valueOf(objectItem.getMainSpecieName()) + " (" + String.valueOf(objectItem.getSampleSpecieName()) + ")");
            textViewItem.setText(objectItem.getMainSpecieName());

            TextView textViewItemNr = (TextView) convertView.findViewById(R.id.textViewItemNr);
            textViewItemNr.setText(String.valueOf(objectItem.getSelectedDangerousCount()));

            convertView.setTag(objectItem);
            return convertView;
        }

    }


