package com.acoms.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.acoms.model.ControlTypes;
import com.acoms.model.Units;

import java.util.List;

/**
 * Created by AldisGrauze on 16.20.4.
 */
public class ControlTypesAdapter extends ArrayAdapter<ControlTypes> {

    Context mContext;
    int layoutResourceId;
    //ControlPoint data[] = null;
    List<ControlTypes> data = null;

    public ControlTypesAdapter(Context mContext, int layoutResourceId, List<ControlTypes> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){

            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        ControlTypes objectItem = data.get(position);
        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(android.R.id.text1);
        textViewItem.setText(String.valueOf(objectItem.getName()));


        textViewItem.setTag(objectItem);
        return convertView;
    }

}

