package com.acoms.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.acoms.koanketa.R;
import com.acoms.model.ControlPoint;
import com.acoms.model.Specie;

import java.util.List;

/**
 * Created by A on 3/28/2016.
 */


public class SpeciesAdapter extends ArrayAdapter<Specie> {

    Context mContext;
    int layoutResourceId;
    //ControlPoint data[] = null;
    List<Specie> data = null;

    public SpeciesAdapter(Context mContext, int layoutResourceId, List<Specie> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){

            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        Specie objectItem = data.get(position);
        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(android.R.id.text1);
        textViewItem.setText(String.valueOf(objectItem.getName()));

        textViewItem.setTag(objectItem);
        return convertView;
    }

}