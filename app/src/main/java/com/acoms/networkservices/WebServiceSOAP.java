package com.acoms.networkservices;

import android.accounts.AuthenticatorException;
import android.text.TextUtils;

import com.acoms.koanketa.BuildConfig;
import com.acoms.model.Assignment;
import com.acoms.model.Constants;
import com.acoms.model.ControlPoint;
import com.acoms.model.ControlPointPicture;
import com.acoms.model.DangerousSpecieSample;
import com.acoms.model.UserCredentials;
import com.acoms.utils.StringDateUtils;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by A on 4/3/2016.
 */
public class WebServiceSOAP {

    private final static String SOAP_SERVER_URL = "http://arejasparbaudesserv.vaad.gov.lv/";
    static String NAME_SPACE = "http://arejasparbaudesserv.vaad.gov.lv/";
    static String URL = "https://mob.vaad.gov.lv/KLASIF_WS/KUVIS_Service.asmx";
    static String TEST_URL = "https://mob.vaad.gov.lv/klasif_ws_test/KUVIS_Service.asmx";

    private final static int SOAP_TIMEOUT = 50000;

    private final String OPEN_SESSION = "AtvertSesiju";
    private final String GET_ASSIGNMENTS = "IzgutDarbaUzdevumus";
    private final String GET_CLASSIFICS = "IzgutMezaParbauzuKlasifikatorusJSON";
    private final String SEND_ASSIGNMENTS = "IesniegtdarbaUzdevumus";

    public static final int TOKEN_EXPIRED = 2040;
    public static final int VALIDATION_ERROR = 102;
    public static final int VALIDATION_ERROR_RANGE = 99;
    public static final int RESPONSE_OK = -1;

    public  SoapObject sendAssignment(String token, Assignment task) throws IOException, AuthenticatorException{

        if(token.equals(Constants.DEMO_USERTOKEN)) return null;

        SoapObject request = new SoapObject(NAME_SPACE, SEND_ASSIGNMENTS);
        request.addProperty("Token", token);
        SoapObject parbaudes = new SoapObject(NAME_SPACE, "Parbaudes");

        SoapObject parbaude = new SoapObject(NAME_SPACE, "Parbaude");
        parbaude.addProperty("DU_ID", task.getIdentifier());
        parbaude.addProperty("Stavoklis", task.getStatus());
        parbaude.addProperty("ParbaudesTips", task.getControlType());
        parbaude.addProperty("AktaNr", task.getTaskNr());

        String cD = StringDateUtils.getFormatedDate(task.getDate(),"yyyy-MM-dd");
        parbaude.addProperty("ParbaudesDatums", StringDateUtils.getFormatedDate(task.getDate(),"yyyy-MM-dd"));
        parbaude.addProperty("ParbaudesVeids", task.getType());

        double area = task.getTrackArea();
        parbaude.addProperty("Platiba", String.valueOf(area));
        String geomString = task.getGeometryAsString();
        parbaude.addProperty("Teritorija", geomString);

        SoapObject paraugi = new SoapObject(NAME_SPACE, "Paraugi");

        List<ControlPoint> controlPoints = task.getControlPoints();
        for(int i = 0; i < controlPoints.size(); i++) {
            ControlPoint current = controlPoints.get(i);
            if(current != null) {

                SoapObject paraugs = new SoapObject(NAME_SPACE, "Paraugs");

                // veido parauga numuru no apsekojuma numura + parauga kārtas numurs
                String sampleNum = String.format("%s/%03d", task.getTaskNr(), i+1);
                paraugs.addProperty("Numurs", sampleNum);
                String geoJson = current.getGeoJSON();
                paraugs.addProperty("AtrasanasVieta", geoJson);
                paraugs.addProperty("ParbauditaProdukcijaID", current.getMainSpecieCode());

                paraugs.addProperty("Lielums", String.valueOf(current.getAmount()));
                paraugs.addProperty("MervienibaID", current.getUnits());
                paraugs.addProperty("VeidsID", current.getSampleTypeCode());

                paraugs.addProperty("Piezimes", current.getDecision());

                SoapObject organismi = new SoapObject(NAME_SPACE,"Organismi");
                DangerousSpecieSample[] ds = current.getDangerous();
                if( ds != null ) {
                    for (int k = 0; k < ds.length; k++) {

                        if (ds[k] != null && ds[k].isSelected()) {
                            SoapObject organisms = new SoapObject(NAME_SPACE, "NosakamaisOrganisms");
                            organisms.addProperty("OrganismsID", ds[k].getSpecieID());
                            organismi.addSoapObject(organisms);
                        }
                    }
                }
                paraugs.addSoapObject(organismi);

                //SoapObject pictures = new SoapObject(NAME_SPACE,"Foto");
                ArrayList<ControlPointPicture> pics = current.getPictures();
                for (ControlPointPicture p: pics){
                    SoapObject bilde = new SoapObject(NAME_SPACE, "Bilde");
                    bilde.addProperty("BildesNosaukums", p.getFullPath());
                    paraugs.addSoapObject(bilde);
                }
                //paraugs.addSoapObject(pictures);

                paraugi.addSoapObject(paraugs);
            }
        }
        parbaude.addSoapObject(paraugi);

        parbaudes.addSoapObject(parbaude);

        request.addSoapObject(parbaudes);
        SoapObject resp = sendRequest(request, SEND_ASSIGNMENTS);
        //processError(resp);

        return resp;
    }


    public  SoapObject getAssignments(String token, String lastDate) throws IOException, AuthenticatorException{

        if(token.equals(Constants.DEMO_USERTOKEN)) return null;

        SoapObject request = new SoapObject(NAME_SPACE, GET_ASSIGNMENTS);

        request.addProperty("Token", token);
        request.addProperty("DatumsNo", lastDate);
        //request.addProperty("DatumsNo", "2016-01-01");//"1901-01-01");
        return sendRequest(request, GET_ASSIGNMENTS);
    }

    public  SoapObject getClassifics(String token, String lastDate) throws IOException, AuthenticatorException{

        if(token.equals(Constants.DEMO_USERTOKEN)) return null;

        SoapObject request = new SoapObject(NAME_SPACE, GET_CLASSIFICS);

        request.addProperty("Token", token);
        request.addProperty("DatumsNo", lastDate);
        return sendRequest(request, GET_CLASSIFICS);
    }

    public  SoapObject getToken(String name, String password) throws IOException, AuthenticatorException{

        if(name.equals(Constants.DEMO_USERNAME)) return null;

        SoapObject request = new SoapObject(NAME_SPACE, OPEN_SESSION);
        return sendRequest(request, OPEN_SESSION);
    }

    private SoapObject sendRequest(SoapObject request, String methodName) throws IOException, AuthenticatorException{

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.implicitTypes = true;
        envelope.setAddAdornments(false);
        envelope.setOutputSoapObject(request);

        String serverUrl = URL;
        if(BuildConfig.PROD == false) serverUrl = TEST_URL;

        HttpTransportSE httpTransportSe = new HttpTransportSE(serverUrl, SOAP_TIMEOUT);
        httpTransportSe.debug = true;

        try {

            httpTransportSe.call(SOAP_SERVER_URL + methodName, envelope);

            if( envelope.bodyIn  instanceof SoapFault) {
                SoapFault fault = (SoapFault) envelope.bodyIn;
                String err = getSoapErrorMessage(fault);
                SoapObject ret = new SoapObject();
                ret.addProperty("ErrText", err);
                ret.addProperty("ErrCode", 2);
                return ret;
            }

            SoapObject result = (SoapObject) envelope.bodyIn;//.getResponse();
            processError(result);
            return result;

        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Network error");

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Method to retrieve the errorMessage from the given SoapFault.
     * @param soapFault
     * @return String representing the errorMessage found in the given SoapFault.
     */
    private static String getSoapErrorMessage (SoapFault soapFault) {
        String errorMessage;
        try {
            Node detailNode = soapFault.detail;
            Element faultDetailElement = (Element)detailNode.getElement(0).getChild(1);
            Element errorMessageElement = (Element)faultDetailElement.getChild(0);
            errorMessage =  errorMessageElement.getText(0);
        }
        catch (Exception e) {
            e.printStackTrace();
            errorMessage = "Could not determine soap error.";
        }
        return errorMessage;
    }

    public int getResponseErrCode(SoapObject soapData){

        int iErr = -1;
        try {
            if (soapData != null) {
                if (soapData.hasProperty("ErrCode")) {

//                    Object err = soapData.getPropertySafely("ErrCode");
//                    String errText  = err.toString();

                    // Note - if error code is not present then will be exception here and return value will be -1
                    SoapPrimitive sp = (SoapPrimitive) soapData.getPrimitiveProperty("ErrCode");
                    Object o = sp.getValue();
                    iErr = Integer.valueOf((String) o);
                }
            }
        }
        catch (Exception e){

        }
        return iErr;
    }

    public UserCredentials getCredentials(String name, String pass) throws IOException, AuthenticatorException{

        Timber.i("getCredentials sent");

        if(name.equals(Constants.DEMO_USERNAME)) return new UserCredentials(Constants.DEMO_USERNAME,Constants.DEMO_USERTOKEN);

        SoapObject request = new SoapObject(NAME_SPACE, OPEN_SESSION);

        request.addProperty("Lietotajvards", name);
        request.addProperty("Parole", pass);

        UserCredentials cred = new UserCredentials();
        SoapObject resp = sendRequest(request, OPEN_SESSION);
        //processError(resp);

        Timber.i("getCredentials recieved");

        if( resp.hasProperty("Token")) cred.token = resp.getPrimitivePropertyAsString("Token");
        else Timber.e("Authentication error - missing token!");
        if( resp.hasProperty("InspektoraKods")) cred.code = resp.getPrimitivePropertyAsString("InspektoraKods");
        return cred;
    }

    private void processError(SoapObject resp) throws AuthenticatorException, IOException {

        // error handling
        if(resp==null) throw new IOException("Can't connect to server!");
        if(getResponseErrCode(resp)>100) {
            String err = resp.getPropertySafely("ErrText").toString();

            String errtext = "Unknown error!";
            if (TextUtils.isEmpty(err) == false) errtext = err;
            throw new AuthenticatorException(errtext);
        }

    }
}
