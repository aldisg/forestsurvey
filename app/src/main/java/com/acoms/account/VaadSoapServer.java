package com.acoms.account;

import android.util.Log;

import com.acoms.koanketa.R;
import com.acoms.model.Constants;
import com.acoms.model.UserCredentials;
import com.acoms.networkservices.WebServiceSOAP;

import timber.log.Timber;

/**
 * Created by A on 2/28/2016.
 */
public class VaadSoapServer implements ServerAuthenticate {
    @Override
    public User userSignUp(String name, String email, String pass, String authType) throws Exception {
        return null;
    }

    @Override
    public User userSignIn(String name, String pass, String authType) throws Exception {
       Timber.d("VaadSoapServer userSignIn");

        if(name.equals(Constants.DEMO_USERNAME)) return new User(Constants.DEMO_USERNAME, new UserCredentials(Constants.DEMO_USERNAME,Constants.DEMO_USERTOKEN));

        WebServiceSOAP service = new WebServiceSOAP();
        UserCredentials credentials = service.getCredentials(name, pass);

        if (credentials.token.isEmpty() )
//            throw new Exception(getResources().getString(R.string.wrong_user));
            throw new Exception("Wrong user or password!");
        return new User(name, credentials);

    }
}
