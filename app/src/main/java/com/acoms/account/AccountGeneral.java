package com.acoms.account;

/**
 * Created by aldisgrauze on 27.02.2016.
 */
public class AccountGeneral {

    /**
     * Account tips
     */
    public static final String ACCOUNT_TYPE = "com.acoms.survey";

    /**
     * Account name
     */
    //public static final String ACCOUNT_NAME = "VAAD";

    /**
     * Usera data lauki
     */
    public static final String USERDATA_USER_OBJ_ID = "userObjectId";   //Parse.com object id
    public static final String USERDATA_USER_PASS = "userData";   //Parse.com object id
    public static final String INSPECTOR_ID = "inspectorId";

    /**
     * Auth token veidi
     */
    public static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    public static final String AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only VAAD pieeja";

    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access VAAD pieeja";

    public static final ServerAuthenticate sServerAuthenticate = new VaadSoapServer();
}