package com.acoms.account;

/**
 * Created by A on 2/28/2016.
 */
public interface ServerAuthenticate {
    public User userSignUp(final String name, final String email, final String pass, String authType) throws Exception;

    public User userSignIn(final String user, final String pass, String authType) throws Exception;
}
