package com.acoms.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by A on 2/28/2016.
 */
public class AppAuthenticatorService extends Service {

    private AppAuthenticator mAuthenticator;
    @Override
    public void onCreate() {
        mAuthenticator = new AppAuthenticator(this);
    }
    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }

}
