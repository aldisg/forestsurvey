package com.acoms.account;

import android.text.format.DateFormat;

import com.acoms.model.Constants;
import com.acoms.model.UserCredentials;

import java.io.Serializable;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by aldisgrauze on 27.02.2016.
 */
public class User implements Serializable {

    private String username;
    private String objectId;
    public String sessionToken;
    private String userDocumentTemplate;

    public User(String name, UserCredentials credentials){
        username = name;

        this.objectId = credentials.code;
        this.sessionToken = credentials.token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
        userDocumentTemplate = setUserFormatString(objectId);
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }


//        AKD akta numura piemērs: 163-AKA-079-16
//        Pirmās 3 zīmes - inspoktora kods
//        AKA - akta tips
//        079 - numurs pēc kārtas (ļaut inspektoram ar roku ievadīt)
//        16 - esošā gada pēdējie divi cipari
    private String setUserFormatString(String userId ) {

        DateFormat sdf = new DateFormat();
        String lastY = sdf.format("yy", new Date()).toString();
        return userId + "-" + Constants.TASK_TYPE + "-%03d-" + lastY;
    }

    public String getUserDocumentTemplate(){
        return userDocumentTemplate;
    }
}
