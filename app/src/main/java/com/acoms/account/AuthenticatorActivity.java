package com.acoms.account;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.acoms.ApplicationController;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;
import com.acoms.utils.ConnectionUtils;
import com.acoms.utils.StringIntArrayUtils;

import timber.log.Timber;

import static com.acoms.account.AccountGeneral.USERDATA_USER_OBJ_ID;
import static com.acoms.account.AccountGeneral.USERDATA_USER_PASS;
import static com.acoms.account.AccountGeneral.sServerAuthenticate;

/**
 * Created by aldisgrauze on 27.02.2016.
 */
public class AuthenticatorActivity extends AccountAuthenticatorActivity {

    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";

    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";

    public final static String PARAM_USER_PASS = "USER_PASS";

    private final int REQ_SIGNUP = 1;

    private final String TAG = this.getClass().getSimpleName();

    private AccountManager mAccountManager;
    private String mAuthTokenType;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
        mAuthTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);

        Account account = ApplicationController.getInstance().getConnectedAccount();
        if(account!=null)
            accountName = account.name;

        if (mAuthTokenType == null)
            mAuthTokenType = AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS;

        mAccountManager = AccountManager.get(ApplicationController.getInstance());

        if (accountName != null)
            ((TextView) findViewById(R.id.accountName)).setText(accountName);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // The sign up activity returned that the user has successfully created an account
        if (requestCode == REQ_SIGNUP && resultCode == RESULT_OK) {
            finishLogin(data);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    public void submit() {

        final String userName = ((TextView) findViewById(R.id.accountName)).getText().toString();
        final String userPass = ((TextView) findViewById(R.id.accountPassword)).getText().toString();

//        final String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);
        findViewById(R.id.login_progress).setVisibility(View.VISIBLE);

        new AsyncTask<String, Void, Intent>() {

            @Override
            protected Intent doInBackground(String... params) {

                Timber.d(TAG + "> Started authenticating");

                Bundle data = new Bundle();
                try {
                    // TODO varbūt vajag niknāk šifrēt?
                    String safePass = StringIntArrayUtils.md5(userPass);

                    User user = sServerAuthenticate.userSignIn(userName, safePass, mAuthTokenType);
                    data.putString(AccountManager.KEY_ACCOUNT_NAME, userName);
                    data.putString(AccountManager.KEY_ACCOUNT_TYPE,  AccountGeneral.ACCOUNT_TYPE);
                    data.putString(AccountManager.KEY_AUTHTOKEN, user.getSessionToken());

                    // User id var noderēt vēlāk tokenam
                    Bundle userData = new Bundle();
                    userData.putString(USERDATA_USER_OBJ_ID, user.getObjectId());
                    data.putBundle(AccountManager.KEY_USERDATA, userData);

                    data.putString(USERDATA_USER_PASS, safePass);
                    //data.putString(PARAM_USER_PASS, safePass);

                } catch (Exception e) {
                    data.putString(KEY_ERROR_MESSAGE, e.getMessage());
                    Timber.e(e, TAG + "> Started authenticating");
                }

                final Intent res = new Intent();
                res.putExtras(data);
                return res;
            }

            @Override
            protected void onPostExecute(Intent intent) {
                findViewById(R.id.login_progress).setVisibility(View.GONE);
                if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
                    Toast.makeText(getBaseContext(), intent.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                } else {
                    finishLogin(intent);
                }
            }
        }.execute();
    }

    private void finishLogin(Intent intent) {
        Timber.d(TAG + "> finishLogin");

        String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String accountPassword = intent.getStringExtra(PARAM_USER_PASS);
        final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

//        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            Timber.d(TAG + "> finishLogin > addAccountExplicitly");
            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String authtokenType = mAuthTokenType;
            String pass= intent.getStringExtra(USERDATA_USER_PASS);

            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword, intent.getBundleExtra(AccountManager.KEY_USERDATA));
            mAccountManager.setAuthToken(account, authtokenType, authtoken);
            mAccountManager.setPassword(account, pass);
//        } else {
//            Log.d(Constants.APP_NAME, TAG + "> finishLogin > setPassword");
//            mAccountManager.setPassword(account, accountPassword);
//            mAccountManager.invalidateAuthToken(account.type, null);
//        }

        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

}