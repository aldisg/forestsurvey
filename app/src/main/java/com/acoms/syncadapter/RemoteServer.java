package com.acoms.syncadapter;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;

import com.acoms.model.Assignment;
import com.acoms.model.Classifs;

import java.io.IOException;
import java.util.List;

/**
 * Created by AldisGrauze on 22.07.2016.
 */

public interface RemoteServer {

    String getToken(String name, String pass) throws IOException, AuthenticatorException;
    List<Assignment> getAssignmentsWithToken(Account account, String lastDate)  throws AuthenticatorException, OperationCanceledException, IOException;
    Classifs getClassifsWithToken(Account account, String lastDate) throws AuthenticatorException, OperationCanceledException, IOException;
    List<Assignment> getAssignments(String token, String lastDate) throws IOException, AuthenticatorException;
    int putAssignment(Account account, Assignment assignmentToAdd) throws Exception;
}
