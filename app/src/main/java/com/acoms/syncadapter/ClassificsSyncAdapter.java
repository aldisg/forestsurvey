package com.acoms.syncadapter;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.os.Bundle;

import com.acoms.koanketa.R;
import com.acoms.model.Classifs;
import com.acoms.model.Constants;
import com.acoms.utils.StringDateUtils;

import java.io.IOException;
import java.util.Date;

import timber.log.Timber;

/**
 * Created by A on 3/19/2016.
 */
public class ClassificsSyncAdapter extends BaseSyncAdapter {

    private static final String TAG = "VaadClassifsSyncAdapter";
    private static final String LAST_CLASIFS_SYNC_TIME = "classifs_sync_date";

    public ClassificsSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        boolean errMsg = false;

        Timber.d(TAG + "> onPerformSync");
        // Building a print of the extras we got
        StringBuilder sb = new StringBuilder();
        if (extras != null) {
            for (String key : extras.keySet()) {
                sb.append(key + "[" + extras.get(key) + "] ");
            }
        }

        Timber.d(TAG + "> onPerformSync for account[" + account.name + "]. Extras: "+sb.toString());
        try {

            VaadServerAccessor remoteServer = new VaadServerAccessor(mAccountManager, account);

            SharedPreferences prefs = getGlobalPrefs();
            String lastDate = prefs.getString(LAST_CLASIFS_SYNC_TIME, Constants.LAST_TASK_SYNC_TIME); // Constants.LAST_TASK_SYNC_TIME;//
            Timber.d(TAG + "> Get remote classifs");

            String syncMsg = getContext().getResources().getString(R.string.no_classifs_recieved);
            Classifs classifs = remoteServer.getClassifsWithToken(account, lastDate );
            if( classifs != null && mApp != null && mApp.classifs != null) {

                syncMsg = BuildNotifyString(classifs);
                String msgTitle = getContext().getResources().getString(R.string.app_name) + " " + getContext().getResources().getString(R.string.data_sync_service);
                sendNotification(getContext(), msgTitle, syncMsg);

                mApp.classifs.initClassifs(classifs);
                prefs.edit().putString(LAST_CLASIFS_SYNC_TIME, StringDateUtils.getFormatedDate(new Date(), "yyyy-MM-dd")).apply();
                Timber.i("Classifs were updated getDangerousSpeciesList=" + String.valueOf(classifs.getDangerousSpeciesList().size()) );

            }
            else Timber.i("Can't save sync classifs data in DB");

            Timber.d(TAG + "> Finished.");

        } catch (OperationCanceledException e) {
            e.printStackTrace();
            Timber.e(e, "onPerformSync error");
            errMsg = true;
        } catch (IOException e) {
            syncResult.stats.numIoExceptions++;
            e.printStackTrace();
            Timber.e(e, "onPerformSync error");
            errMsg = true;
        } catch (AuthenticatorException e) {
            syncResult.stats.numAuthExceptions++;
            e.printStackTrace();
            Timber.e(e, "onPerformSync error");
            errMsg = true;
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e, "onPerformSync error");
            errMsg = true;
        }
        if(errMsg){
            sendNotification(getContext(), getContext().getResources().getString(R.string.app_name) + " " + getContext().getResources().getString(R.string.error),
                    getContext().getResources().getString(R.string.no_classifs_recieved));
        }
    }

    private String BuildNotifyString(Classifs classifs) {

        String response = getContext().getResources().getString(R.string.no_classifs_recieved);

        int iControls = classifs.getControlList().size();
        int iDangSpecies = classifs.getDangerousSpeciesList().size();
        int iSamples = classifs.getSamplesList().size();
        int iSpecies = classifs.getSpeciesList().size();
        int iUnits = classifs.getUnitsList().size();

        // ir vismaz viens jauns klasifs
        if((iControls + iDangSpecies + iSamples + iSpecies + iUnits) > 0) {

            response = "";
            if( iControls > 0 )
                response = String.format(getContext().getResources().getString(R.string.control_types_classifs), iControls) + System.getProperty("line.separator");
            if( iDangSpecies > 0 )
                response = response + String.format(getContext().getResources().getString(R.string.dang_species_classifs), iDangSpecies) + System.getProperty("line.separator");
            if( iSamples > 0 )
                response = response + String.format(getContext().getResources().getString(R.string.sample_types_classifs), iSamples) + System.getProperty("line.separator");
            if( iSpecies > 0 )
                response = response + String.format(getContext().getResources().getString(R.string.species_classifs), iSpecies) + System.getProperty("line.separator");
            if( iUnits > 0 )
                response = response + String.format(getContext().getResources().getString(R.string.units_classifs), iUnits);
        }

        return response;
    }
}
