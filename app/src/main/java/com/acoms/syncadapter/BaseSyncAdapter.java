package com.acoms.syncadapter;

import android.accounts.AccountManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.NotificationCompat;

import com.acoms.ApplicationController;
import com.acoms.koanketa.MainActivity;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;

/**
 * Created by aldisgrauze on 01.08.2017.
 */

public abstract class BaseSyncAdapter extends AbstractThreadedSyncAdapter {

    public Context mContext;
    public ApplicationController mApp;
    public final AccountManager mAccountManager;

    public SharedPreferences getGlobalPrefs() {
        return mContext.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void sendNotification(Context ctx, String titleMessage, String message)
    {
        Intent notificationIntent = new Intent(ctx, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, notificationIntent, 0);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(ctx)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titleMessage)
                .setContentText(message)
                //.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.FLAG_SHOW_LIGHTS)
                .setLights(0xff00ff00, 300, 100)
                .setPriority(Notification.PRIORITY_DEFAULT)//;
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 , notificationBuilder.build());

    }

    public BaseSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mAccountManager = AccountManager.get(context);
        mApp = (ApplicationController) context;
        mContext = context;
    }
}
