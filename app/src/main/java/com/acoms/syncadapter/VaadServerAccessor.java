package com.acoms.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.os.Bundle;
import android.util.Log;

import com.acoms.account.AccountGeneral;
import com.acoms.model.Assignment;
import com.acoms.model.Classifs;
import com.acoms.model.Constants;
import com.acoms.networkservices.WebServiceSOAP;
import com.acoms.utils.SoapDeserializer;
import com.google.gson.Gson;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.acoms.account.AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS;
import static com.acoms.networkservices.WebServiceSOAP.TOKEN_EXPIRED;
import static com.acoms.networkservices.WebServiceSOAP.VALIDATION_ERROR;
import static com.acoms.networkservices.WebServiceSOAP.VALIDATION_ERROR_RANGE;


/**
 * Created by A on 3/13/2016.
 */
public class VaadServerAccessor implements RemoteServer{

    private AccountManager accountManager;
    public VaadServerAccessor(){
        //this.accountManager = accountManager;
    }

    public VaadServerAccessor(AccountManager accountManager, Account account) throws AuthenticatorException{
        //if(TextUtils.isEmpty(accountManager.getPassword(account) )) throw new AuthenticatorException("Missing password!");
        this.accountManager = accountManager;
    }

    public String getToken(String name, String pass) throws IOException, AuthenticatorException {

        Timber.e("name =" + name + " pass=" + pass);
        WebServiceSOAP service = new WebServiceSOAP();
        SoapObject obj = service.getToken(name, pass);

        if(obj != null && obj.hasProperty("Token")){
            return obj.getPrimitivePropertyAsString("Token");
        }
        return "";
    }

    private List<Assignment> parseAssignments(SoapObject obj) throws IOException, AuthenticatorException{

        if( obj == null ) return null;
        if(obj.hasProperty("DarbaUzdevumi")){
            List<Assignment> newTasks = SoapDeserializer.parseAssignments(obj);
            return newTasks;
        }
        return null;
    }

    public List<Assignment> getAssignmentsWithToken(Account account, String lastDate) throws AuthenticatorException, OperationCanceledException, IOException {

        String authToken = accountManager.blockingGetAuthToken(account, AUTHTOKEN_TYPE_FULL_ACCESS, true);
        WebServiceSOAP service = new WebServiceSOAP();
        SoapObject obj = null;
        List<Assignment> newTasks = null;

        int iErrCode = VALIDATION_ERROR;
        try {
            obj = service.getAssignments(authToken, lastDate);
            newTasks = parseAssignments(obj);
            iErrCode = service.getResponseErrCode(obj);
        }
        catch (AuthenticatorException ex) {
            Timber.e(ex, "Authentication error!");
        }

        if( iErrCode == TOKEN_EXPIRED || iErrCode == VALIDATION_ERROR ) {        // is token expired?

            authToken = renewToken(accountManager, account, authToken);
            if( authToken != null ) {
                obj = service.getAssignments(authToken, lastDate);
                if (service.getResponseErrCode(obj) > VALIDATION_ERROR_RANGE) // is password changed?
                    return null;
                else
                    return parseAssignments(obj);
            }
        }

        if( newTasks == null ) newTasks = new ArrayList<>();
        return newTasks;
    }

    private String renewToken(AccountManager accountManager, Account account, String token) throws AuthenticatorException, OperationCanceledException, IOException {

        if (accountManager != null) {
            accountManager.invalidateAuthToken(account.type, token);
            return accountManager.blockingGetAuthToken(account, AUTHTOKEN_TYPE_FULL_ACCESS, true);
        }
        return null;
    }

    private Classifs parseClassifics(SoapObject soapData){

        Classifs classifs = null;

        try {
            if (soapData!=null && soapData.hasProperty("IzgutMezaParbauzuKlasifikatorusJSONResult")) {

                SoapPrimitive sp = (SoapPrimitive) soapData.getProperty("IzgutMezaParbauzuKlasifikatorusJSONResult");
                Object o = sp.getValue();
                String sJson = o.toString();

                Gson gson = new Gson();
                classifs = gson.fromJson(sJson, Classifs.class);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Timber.e(e, "parseClassifics error");
        }
        return classifs;
    }

    public Classifs getClassifsWithToken(Account account, String lastDate) throws AuthenticatorException, OperationCanceledException, IOException {

        String authToken = accountManager.blockingGetAuthToken(account, AUTHTOKEN_TYPE_FULL_ACCESS, true);

        WebServiceSOAP service = new WebServiceSOAP();
        SoapObject obj = service.getClassifics(authToken, lastDate);

        int iErrCode = service.getResponseErrCode(obj);
        //if( iErrCode == service.TOKEN_EXPIRED ) {
        if( iErrCode > VALIDATION_ERROR_RANGE ) {
            authToken = renewToken(accountManager, account, authToken);
            if( authToken != null ) {
                obj = service.getClassifics(authToken, lastDate);
                if (service.getResponseErrCode(obj) > VALIDATION_ERROR_RANGE) // is password changed?
                    return null;
            }
        }

        return parseClassifics(obj);
    }


    public List<Assignment> getAssignments(String token, String lastDate) throws IOException, AuthenticatorException{

        WebServiceSOAP service = new WebServiceSOAP();
        SoapObject obj = service.getAssignments(token, lastDate);

        // is token expired?
        if( obj == null ) return null;

        if(obj.hasProperty("DarbaUzdevumi")){
            List<Assignment> newTasks = SoapDeserializer.parseAssignments(obj);
            return newTasks;
        }
        return null;
    }

    // nodzēš darbu un visu info saistītajās tabulās tikai tad, ja ir sinhronizēts
    public void deleteAssignment(){

        // TODO - nodzēš maršrutu

        // TODO - nodzēš kontrolpunktus

    }

    public int putAssignment(Account account, Assignment assignmentToAdd) throws Exception {

        String authToken = accountManager.blockingGetAuthToken(account, AUTHTOKEN_TYPE_FULL_ACCESS, true);
        //Timber.d("putAssignment ["+assignmentToAdd.toString()+"]");

        WebServiceSOAP service = new WebServiceSOAP();
        SoapObject obj = service.sendAssignment(authToken, assignmentToAdd);

        int iErrCode = service.getResponseErrCode(obj);
        if( iErrCode != TOKEN_EXPIRED && iErrCode != VALIDATION_ERROR ) return iErrCode;

        // is token expired?
        authToken = renewToken(accountManager, account, authToken);
        if( authToken != null ) {
            obj = service.sendAssignment(authToken, assignmentToAdd);
            if (service.getResponseErrCode(obj) > VALIDATION_ERROR_RANGE) // is password changed?
                return VALIDATION_ERROR;
        }

        return WebServiceSOAP.RESPONSE_OK;
    }

}
