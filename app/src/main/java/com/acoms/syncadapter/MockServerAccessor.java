package com.acoms.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.text.TextUtils;

import com.acoms.model.Assignment;
import com.acoms.model.Classifs;

import java.util.List;

/**
 * Created by AldisGrauze on 22.07.2016.
 */

public class MockServerAccessor implements RemoteServer {

    private AccountManager accountManager;

    public MockServerAccessor(AccountManager accountManager, Account account) throws AuthenticatorException{
        this.accountManager = accountManager;
    }

    @Override
    public String getToken(String name, String pass) {
        if(TextUtils.equals(name, "demo") && TextUtils.equals(pass,"demo") )
            return "abcd";
        //throw new AuthenticatorException("Wrong password!");
        return null;
    }

    @Override
    public List<Assignment> getAssignmentsWithToken(Account account, String lastDate) {



        return null;
    }

    @Override
    public Classifs getClassifsWithToken(Account account, String lastDate) {

        return null;
    }

    @Override
    public List<Assignment> getAssignments(String token, String lastDate) {

        return null;
    }

    @Override
    public int putAssignment(Account account, Assignment assignmentToAdd) {

        return 0;
    }
}
