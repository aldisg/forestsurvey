package com.acoms.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.acoms.ApplicationController;
import com.acoms.account.AccountGeneral;
import com.acoms.database.AssignmentDatabaseContract;
import com.acoms.koanketa.MainActivity;
import com.acoms.koanketa.R;
import com.acoms.model.Assignment;
import com.acoms.model.Constants;
import com.acoms.networkservices.WebServiceSOAP;
import com.acoms.utils.StringDateUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import timber.log.Timber;


public class AssignmentSyncAdapter extends BaseSyncAdapter {

    private static final String TAG = "AssignmentsSyncAdapter";
    private static final String LAST_TASK_SYNC_TIME = "task_sync_date";

    public AssignmentSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {

        Timber.d(TAG + "> onPerformSync");
        boolean errMsg = false;

        // Building a print of the extras we got
        StringBuilder sb = new StringBuilder();
        if (extras != null) {
            for (String key : extras.keySet()) {
                sb.append(key + "[" + extras.get(key) + "] ");
            }
        }
        Timber.d(TAG + "> onPerformSync for account[" + account.name + "]. Extras: "+sb.toString());

        try {

            VaadServerAccessor remoteAssignmentService = new VaadServerAccessor(mAccountManager, account);

            SharedPreferences prefs = getGlobalPrefs();
            String lastDate =  prefs.getString(LAST_TASK_SYNC_TIME, Constants.LAST_TASK_SYNC_TIME); //"2016-01-01";
            List<Assignment> remoteAssignments = remoteAssignmentService.getAssignmentsWithToken(account, lastDate);

            if(remoteAssignments != null )
                Timber.d(TAG + "> Get remote assignments since " + lastDate + " number: " + String.valueOf(remoteAssignments.size()));

            // Get shows from local
            ArrayList<Assignment> localAssignments = new ArrayList<>();
            Uri itemUri = AssignmentDatabaseContract.CONTENT_URI;
            Cursor curAssignments = provider.query(itemUri, null, null, null, null);

            if (curAssignments != null) {
                while (curAssignments.moveToNext()) {
                    localAssignments.add(Assignment.fromCursor(curAssignments));
                }
                curAssignments.close();
            }
            Timber.d(TAG + "> Get local works" + " number: " + String.valueOf(localAssignments.size()));

            // padarītos darbus statusā ASSIGNMENT_STATUS_DONE nosūta serverim
            ArrayList<Assignment> sendToServer = new ArrayList<>();
            for (Assignment localAssignment : localAssignments) {
                //if (!remoteAssignments.contains(localAssignment))
                if(localAssignment.getStatus() == AssignmentDatabaseContract.ASSIGNMENT_DONE)
                    sendToServer.add(localAssignment);
            }

            // aktualizē lokālo darbu sarakstu
            ArrayList<Assignment> assignmentsToSave = new ArrayList<>();
            if(remoteAssignments != null ) {
                for (Assignment remoteAssignment : remoteAssignments) {
                    if (!localAssignments.contains(remoteAssignment)) // TODO ko darīt ja darbs jau ir un iesākts/pabeigts?
                        assignmentsToSave.add(remoteAssignment);
                }
            }

            String msgBody = "";

            if (sendToServer.size() == 0) {
                Timber.d(TAG + "> No local changes to update server");
            } else {
                Timber.d(TAG + "> Updating remote server with local changes");

                int iSynced = 0, iErrors = 0, lastErr = 0;
                // Updating remote assignments
                for (Assignment assignmentDone : sendToServer) {
                    Timber.d(TAG + "> Local -> Remote [" + assignmentDone.toString() + "]");

                    // ielasa pilnu Assignment info - abas tabulas un track
                    String itemUriStr = "content://"+ AssignmentDatabaseContract.AUTHORITY + "/taskdetails/" +  String.valueOf(assignmentDone.getId());
                    itemUri = Uri.parse(itemUriStr);
                    Cursor cursor = provider.query(itemUri, null, null, null, null);
                    if(cursor != null) {

                        Assignment fullAssignment = Assignment.fromFullCursor(getContext(),cursor);
                        int iResponse = remoteAssignmentService.putAssignment(account, fullAssignment);
                        if( iResponse == WebServiceSOAP.RESPONSE_OK ) {

                            ContentValues values = new ContentValues();
                            values.put(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE, AssignmentDatabaseContract.ASSIGNMENT_SYNCED);
                            int rec = provider.update(AssignmentDatabaseContract.CONTENT_URI, values, AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID + "=" + Integer.valueOf(assignmentDone.getId()), null);

                            //Timber.e("state changed =" + rec);
                            iSynced++;
                        }
                        else {
                            iErrors++;
                            lastErr = iResponse;
                        }

                    }

                }

                msgBody = String.format(getContext().getResources().getString(R.string.num_surveys_sent), iSynced) + System.getProperty("line.separator");
                if(iErrors>0) {
                    msgBody = msgBody + String.format(getContext().getResources().getString(R.string.num_surveys_errors), iErrors, lastErr) + System.getProperty("line.separator");
                }
            }

            if (assignmentsToSave.size() == 0) {
                msgBody = msgBody + getContext().getResources().getString(R.string.no_new_assignments);
                Timber.d(TAG + "> No server changes to update local database");
            } else {
                Timber.d(TAG + "> Updating local database with remote changes");
                final String userid = mApp.getUser().getObjectId();
                // Updating local tv shows
                int i = 0;
                ContentValues recievedAssignmetValues[] = new ContentValues[assignmentsToSave.size()];
                for (Assignment assignment : assignmentsToSave) {
                    Timber.d(Constants.APP_NAME, TAG + "> Remote -> Local [" + assignment.toString() + "]" + " for user =" + userid);
                    assignment.setInspector(userid); // !!! - replace original - this is necessary for later user filtering
                    recievedAssignmetValues[i++] = assignment.getContentValues();
                }

                int iRecs = provider.bulkInsert(AssignmentDatabaseContract.CONTENT_URI, recievedAssignmetValues);
                if( iRecs > 0 ) {
                    msgBody = msgBody + String.format(getContext().getResources().getString(R.string.num_surveys_recieved), iRecs);
                }
            }
            String msgTitle = getContext().getResources().getString(R.string.app_name) + " " + getContext().getResources().getString(R.string.data_sync_service);
            sendNotification(getContext(), msgTitle, msgBody);

            prefs.edit().putString(LAST_TASK_SYNC_TIME, StringDateUtils.getFormatedDate(new Date(), "yyyy-MM-dd")).apply();
            Timber.d(TAG + "> Finished.");

        } catch (OperationCanceledException e) {
            e.printStackTrace();
            Timber.e(e, "onPerformSync assignments error");
            errMsg = true;
        } catch (IOException e) {
            syncResult.stats.numIoExceptions++;
            e.printStackTrace();
            Timber.e(e, "onPerformSync assignments error");
            errMsg = true;
        } catch (AuthenticatorException e) {
            syncResult.stats.numAuthExceptions++;
            e.printStackTrace();
            Timber.e(e, "onPerformSync assignments error");
            errMsg = true;
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e, "onPerformSync assignments error");
            errMsg = true;
        }

        if(errMsg){
            sendNotification(getContext(), getContext().getResources().getString(R.string.app_name) + " " + getContext().getResources().getString(R.string.error),
                    getContext().getResources().getString(R.string.no_assignments_recieved));
        }
    }
}

