package com.acoms.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.acoms.ApplicationController;
import com.acoms.adapters.ControlTypesAdapter;
import com.acoms.koanketa.R;
import com.acoms.model.ControlTypes;
import com.acoms.utils.ClickToSelectEditText;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by A on 4/18/2016.
 */
public class NewTaskDialogFragment extends DialogFragment implements EditText.OnEditorActionListener {

    private EditText mEditText;
    private int mSelectedType;
    private String mNrTemplate;
    NewTaskDialogListener parent;

    private static final String LAST_ID = "lastID";
    private static final String TASK_TEMPLATE = "taskNr";
    private ClickToSelectEditText typesList = null;

    public NewTaskDialogFragment() {
        // Empty constructor required for DialogFragment
        parent = (NewTaskDialogListener) getTargetFragment();
    }

    public static NewTaskDialogFragment newInstance(int lastID, String taskNr) {
        NewTaskDialogFragment frag = new NewTaskDialogFragment();
        Bundle args = new Bundle();
        args.putInt(LAST_ID, lastID);
        args.putString(TASK_TEMPLATE, taskNr);
        frag.setArguments(args);
        return frag;
    }

    // Defines the listener interface
    public interface NewTaskDialogListener {
        void onFinishEditDialog(int surveyType, int lastId, String taskNr);
        void onCancelEditDialog();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            // Return input text to activity
            int lastId = Integer.parseInt(mEditText.getText().toString());
            String taskNr = String.format(mNrTemplate, lastId);
            if(parent != null)
                parent.onFinishEditDialog(mSelectedType, lastId, taskNr);
            dismiss();
            return true;
        }
        return false;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int lastID = getArguments().getInt(LAST_ID);
        mNrTemplate = getArguments().getString(TASK_TEMPLATE);

        parent = (NewTaskDialogListener) getTargetFragment();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.new_task_dialog, null);

        mEditText = (EditText) view.findViewById(R.id.taskUnique);
        mEditText.setText(String.valueOf(lastID));

        //Adapter must implement ListAdapter (e.g. extend ArrayAdapter<MyModel>)
        typesList = (ClickToSelectEditText)view.findViewById(R.id.controlTypes);
        List<ControlTypes> uList = ApplicationController.classifs.getControlTypes();
        ControlTypesAdapter a = new ControlTypesAdapter(getContext(), android.R.layout.simple_list_item_1, uList);
        typesList.setAdapter(a);
        typesList.setOnItemSelectedListener(new ClickToSelectEditText.OnItemSelectedListener<ControlTypes>() {
            @Override
            public void onItemSelectedListener(ControlTypes item, int selectedIndex) {
                typesList.setError( null);
                typesList.setText(item.getName());
                mSelectedType = item.getId();
            }
        });
        return new AlertDialog.Builder(getActivity())
                //.setIcon(R.drawable.alert_dialog_dart_icon)
                .setView(view)
                .setTitle(R.string.newTask)
                .setPositiveButton(R.string.alert_dialog_ok,null)
                .setNegativeButton(R.string.alert_dialog_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                if(parent != null)
                                    parent.onCancelEditDialog();
                            }
                        }).create();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // this is necessary for validation to override buton handler!!
        final AlertDialog alertDialog = (AlertDialog) getDialog();
        Button okButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String str = typesList.getText().toString();
                if(ApplicationController.classifs.getControlTypes().size() > 0 && str.equalsIgnoreCase("")) {
                    typesList.setError( getResources().getString(R.string.please_select_type ));
                }
                else {
                    int lastId = Integer.parseInt(mEditText.getText().toString());
                    String taskNr = String.format(mNrTemplate, lastId);
                    if (parent != null)
                        parent.onFinishEditDialog(mSelectedType, lastId, taskNr);
                    alertDialog.dismiss();
                }
            }
        });
    }



    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if(parent != null)
            parent.onCancelEditDialog();
    }
}
