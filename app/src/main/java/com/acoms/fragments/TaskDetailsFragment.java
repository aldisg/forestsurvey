package com.acoms.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.acoms.ApplicationController;
import com.acoms.adapters.ControlPointListAdapter;
import com.acoms.database.AssignmentDatabaseContract;
//import com.acoms.koanketa.LastLocationLoader;
import com.acoms.koanketa.BaseActivity;
import com.acoms.koanketa.ControlPointActivity;
import com.acoms.koanketa.ControlPointFragment;
import com.acoms.koanketa.LocationReceiver;
import com.acoms.koanketa.R;
import com.acoms.koanketa.TrackManager;
import com.acoms.koanketa.TrackMapFragment;
import com.acoms.model.Assignment;
import com.acoms.model.BasicBufferSize;
import com.acoms.model.BufferSize;
import com.acoms.model.Constants;
import com.acoms.model.ControlPoint;
import com.acoms.model.MapReadyEvent;
import com.acoms.model.NewControlPoint;
import com.acoms.model.NewLocation;
import com.acoms.model.NewTrackPoint;
import com.acoms.model.TaskDetailsLoaded;
import com.acoms.utils.StringDateUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import timber.log.Timber;

/**
 * Created by A on 4/14/2016.
 */
public class TaskDetailsFragment extends Fragment implements NewTaskDialogFragment.NewTaskDialogListener, BaseActivity.OnBackPressedListener  {

    private static final String TAG = "vaad TaskDetails";
    private static final String ARG_RUN_ID = "RUN_ID";
    private static final int LOAD_RUN = 0;
    //private static final int LOAD_CONTROLPOINTS = 1;
    //private static final int LOAD_LOCATIONS = 2;

    private static final int NEW_CONTROL_POINT = 1;
    private static final int EDIT_CONTROL_POINT = 2;
    private static final String LAST_NEW_ASSIGNMENT = "last_new_assignment";
    private static final String NEW_TASK = "new_task";
    private static final String TRACK_STARTED = "track_active";
    private static final String ACTIVE_TRACK_ID = "track_active_id";

    private Assignment mTask;

    private TextView duName, duDate, duDateTo, duDescr, mControlPointView, mGPSAccuracy;
    private ListView mControlPointList;

    private Toolbar toolbar;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private FloatingActionButton mPointButton, mSaveButton, mStartStopButton;

    private ApplicationController myApp;
    private boolean started;
    private TrackManager mTrackManager;
    //private Run mRun;
    private TextView mDistance;

    private CursorLoader cursorLoader;
    private Cursor mCursor = null;

    private boolean hasChanges;
    private boolean isDialogActive;
    private boolean needToSave;
    private TextView mDuName;
    private TextView mDuComments;

    private List<Location> mLocations = new ArrayList<>();

    private boolean isEditable;
    private FloatingActionButton zoomInBtn;
    private FloatingActionButton zoomOutBtn;
    private FloatingActionButton gpsBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        myApp = ApplicationController.getInstance();
        mTrackManager = TrackManager.get(getActivity());
        isDialogActive = false;
        isEditable = false;

        Bundle args = getArguments();
        if (args != null) {
            long runId = args.getLong(ARG_RUN_ID, -1);
            if (runId != -1) {
                Timber.i("Details create: init loader for id=" + Long.toString(runId));
                LoaderManager lm = getLoaderManager();
                lm.initLoader(LOAD_RUN, args, new TaskDetailsLoaderCallback());
            }
        }
        else {
            int lastnr = myApp.preferences.getInt( LAST_NEW_ASSIGNMENT, 0) + 1;
            myApp.preferences.edit().putInt( LAST_NEW_ASSIGNMENT, lastnr).commit();
            mTask = Assignment.createEmptyAssignment(lastnr, getResources().getString(R.string.new_survey_title), getResources().getString(R.string.new_survey_descr), myApp.getUser().getObjectId()); // current inspector id
            checkTaskNumber();
        }
        //}
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putBoolean(NEW_TASK, isDialogActive);
        outState.putBoolean(TRACK_STARTED, started);
        //outState.putSerializable(CURRENT_TASK, mTask);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        EventBus.getDefault().register(this);
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);

        // ---------------------------------------------------------------------------------------------------------
        View view = super.onCreateView(inflater, container, savedInstanceState);

        toolbar = (Toolbar) container.findViewById(R.id.toolbar);

        //set toolbar appearance
        //toolbar.setBackground(R.color.material_blue_grey_800);

        //for crate home button
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getFragmentManager().findFragmentByTag("mapsis") == null)
        {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.mapView, mRunMap, "mapsis").commit();
        }
        initControls(container);

        setHasOptionsMenu(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) container.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");
        AppBarLayout appBarLayout = (AppBarLayout) container.findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    if(mTask != null )
                    collapsingToolbarLayout.setTitle(mTask.getTaskNr());
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle("");
                    isShow = false;
                }
            }
        });

        // fix for maps paning
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(AppBarLayout appBarLayout) {
                return false;
            }
        });
        params.setBehavior(behavior);

        final ControlPointListAdapter cpAdapter = new ControlPointListAdapter(getActivity(), R.layout.control_pint_item, new ArrayList<ControlPoint>());
        mControlPointList.setAdapter(cpAdapter);

        mControlPointList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ControlPoint point = cpAdapter.getItem(i);
                Intent intent = new Intent(getActivity(), ControlPointActivity.class);
                intent.putExtra(ControlPointActivity.CURRENT_POINT, point);
                intent.putExtra(ControlPointActivity.CURRENT_POINT_EDITABLE, mTask.isEditable());
                startActivityForResult(intent, EDIT_CONTROL_POINT);
            }
        });

        mControlPointList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final ControlPoint point = cpAdapter.getItem(position);
                int status = mTask.getStatus();
                if(status == AssignmentDatabaseContract.ASSIGNMENT_DONE || status == AssignmentDatabaseContract.ASSIGNMENT_DONE){
                    Toast.makeText(getActivity(), getResources().getString(R.string.sorry_survey_is_closed), Toast.LENGTH_SHORT).show();
                    return true;
                }
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getResources().getString(R.string.dlg_delete_title))
                        .setMessage(getResources().getString(R.string.dlg_delete_point) + " " + point.getMainSpecieName() + " ?")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hasChanges = true;
                                mTask.deleteControlPoint(point);
                                updateControlPointList();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();

                return true;
            }
        });
        mPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // pedējais punkts būs punkta pozīcija, ja useris akceptēs
                Intent i = new Intent(getActivity(), ControlPointActivity.class);

                ControlPoint point = new ControlPoint(-1,mTask.getPointNumber() + 1, 0, 0, 0.0f, 0, "", null, myApp.getLastLattitude(), myApp.getLastLongitude(), null);

                i.putExtra(ControlPointActivity.CURRENT_POINT, point);
                i.putExtra(ControlPointActivity.CURRENT_POINT_EDITABLE, true);
                startActivityForResult(i, NEW_CONTROL_POINT);
            }
        });

        mStartStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toogleTracking();
            }
        });

        //
        //if (mTask != null) initTaskDetails();

        //probably orientation change
        if (savedInstanceState != null) { // orientation change

            started = savedInstanceState.getBoolean(TRACK_STARTED);
            // TODO uzzīmē buferi!!
            if(started) {


            }
            setStartStopButton(started);
            isDialogActive = savedInstanceState.getBoolean(NEW_TASK);
            if(isDialogActive){
                checkTaskNumber();
            }
        }

        //hasChanges = false;
        return view;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_CONTROL_POINT || requestCode == EDIT_CONTROL_POINT) { // kas ir atlidojis?
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {

                hasChanges = true;

                // aktualizē punktu skaitu un uzzīmē šo
                ControlPoint confirmedPoint = data.getParcelableExtra(ControlPointFragment.NEW_POINT);
                if(mTask != null) {

                    confirmedPoint.setPosition(myApp.getLastLocation());

                    if (requestCode == NEW_CONTROL_POINT) mTask.addControlPoint(confirmedPoint);
                    else mTask.updateControlPoint(confirmedPoint);

                    saveTask(false, true);

                    // parādās sarkanajā aplī kā kontrolpunktu skaits
                    mControlPointView.setText(String.valueOf(mTask.getPointNumber()));
                    updateControlPointList();

                    startLoader(mTask, true);

                    // jauns pievienots apsekošanas punkts, kurs vēl nav saglabats
                    //if (mRunMap != null) mRunMap.addControlPoint(confirmedPoint.getPosition());
                    EventBus.getDefault().post(new NewControlPoint(confirmedPoint.getPosition()));
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(isEditable)
            inflater.inflate(R.menu.point_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onAskToCloseDetails();
                //getActivity().onBackPressed();
                break;
            case R.id.actionSave:
                saveTask(true, false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void onEvent(MapReadyEvent map) {

        if(map.isReady()) {
            // TODO
            //updateStaticUI();
            Timber.i("MapInit ok");
        }
    }

    @Override
    public void onResume() {

        needToSave = true;
        mTrackManager.stopBackgroundMonitoring();

        super.onResume();
        View view = getView();
        if(view != null ) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                        // handle back button's click listener
                        onAskToCloseDetails();
                        return true;
                    }
                    return false;
                }
            });
        }

        // TODO pārlādē esošo tracku (ja ir aktīvs) - citādi pazudīs
        startLoader(mTask, false);

    }

    private void startLoader(Assignment task, boolean restart) {

        if( task == null ) return;

        if(getLoaderManager().hasRunningLoaders()) return;

        int currId = task.getId(); //myApp.preferences.getInt(ACTIVE_TRACK_ID, -1);//preferences.getLong(ACTIVE_TRACK_ID, -1);
        if (currId != -1) {
            Timber.i("onResume of TaskDetailsFragment - reload track id=" + Long.toString(currId));
            Bundle args = new Bundle();
            args.putLong(ARG_RUN_ID, currId);
            if( !restart )
                getLoaderManager().initLoader(LOAD_RUN, args, new TaskDetailsLoaderCallback());
            else
                getLoaderManager().restartLoader(LOAD_RUN, args, new TaskDetailsLoaderCallback());

        }
    }

    @Override
    public void onPause() {

        if( needToSave ) {

            saveTask(false, true); // klusais save
            if(started)
                mTrackManager.startBackgroundMonitoring(mTask.getCurrentPart());
        }
        super.onPause();
    }

    // ---------------------- private functions -------------------------------------------------------

    private void initControls(View view){

        duName = (TextView) view.findViewById(R.id.duName);
        duDate = (TextView) view.findViewById(R.id.duDate);
        duDateTo = (TextView) view.findViewById(R.id.duDateTo);
        duDescr = (TextView) view.findViewById(R.id.duDescr);
        mPointButton = (FloatingActionButton)view.findViewById(R.id.addPoint);
        mSaveButton = (FloatingActionButton)view.findViewById(R.id.saveTask);

        mSaveButton.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               saveTask(false, false);
                                           }
                                       });

        mControlPointView = (TextView)view.findViewById(R.id.cp_nums);
        mControlPointList = (ListView) view.findViewById(R.id.ko_points_list);

        mStartStopButton = (FloatingActionButton)view.findViewById(R.id.startSurvey);
        mGPSAccuracy = (TextView)view.findViewById(R.id.run_gpsAccuracy);
        mDistance = (TextView) view.findViewById(R.id.run_distanceTextView);
        mDuName = (TextView) view.findViewById(R.id.duName);
        mDuName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return !isEditable;
            }
        });
        mDuComments = (TextView) view.findViewById(R.id.duDescr);
        mDuComments.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return !isEditable;
            }
        });

        zoomInBtn = (FloatingActionButton)view.findViewById(R.id.mapZoomPlus);
        zoomInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRunMap.zoomMap(true);
            }
        });
        zoomOutBtn = (FloatingActionButton)view.findViewById(R.id.mapZoomMinus);
        zoomOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRunMap.zoomMap(false);
            }
        });
        gpsBtn = (FloatingActionButton)view.findViewById(R.id.actualGPS);
        gpsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gpsBtn.setImageResource(R.drawable.ic_gps_fixed_white_36dp);
                mRunMap.setMapPosition(myApp.getLastLocation());
                mRunMap.setLocMarker(myApp.getLastLocation());
                if(!mTrackManager.isStartRequested())
                    mTrackManager.startLocationUpdates();
                locationRequested = true;
            }
        });
    }

    private void saveTask(boolean finish, boolean silent) {

        Timber.i("SaveTask");

        hasChanges = false;

        int status = mTask.getStatus();
        if(status != AssignmentDatabaseContract.ASSIGNMENT_DONE && status != AssignmentDatabaseContract.ASSIGNMENT_DONE) {

            // save task name and desription
            if (mDuName != null) mTask.setName(mDuName.getText().toString());
            if (mDuComments != null) mTask.setDescr(mDuComments.getText().toString());

            if(mLocations.size() > 1) // viens punkts nav apsekojums?
                mTrackManager.insertLocations(mTask.getId(), mLocations);

            // jauniem apsekojumiem ir jauns nr
            int iTaskId = mTask.saveData(getActivity(), mCursor);
            if (iTaskId > 0 && !silent)
                    Toast.makeText(getActivity(), getResources().getString(R.string.survey_saved), Toast.LENGTH_SHORT).show();
        }

        if (finish)
            getActivity().finish();
    }

    private void onAskToCloseDetails(){

        if(started) {
            new AlertDialog.Builder(getActivity())
//                .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getResources().getString(R.string.dlg_survey_title))
                    .setMessage(getResources().getString(R.string.q_stop_survey))
                    .setPositiveButton(android.R.string.ok  , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toogleTracking();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            needToSave = false; // nenotiks save pie OnPause
                            return;
                        }
                    })
                    .show();
        }
        else {

            if(hasChanges) {

                new AlertDialog.Builder(getActivity())
    //                .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getResources().getString(R.string.dlg_changes_title))
                        .setMessage(getResources().getString(R.string.q_save_changes))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveTask(true, false);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                needToSave = false; // nenotiks save pie OnPause
                                getActivity().finish();
                            }
                        })
                        .show();
            }
            else getActivity().finish();
        }
    }

    private void updateStaticUI(){

        if(mTask != null ) {

            duName.setText(mTask.getName());
            duDateTo.setText(StringDateUtils.getFormatedDate(mTask.getDateTo(), "dd.MM.yyyy"));
            duDate.setText(StringDateUtils.getFormatedDate(mTask.getDate(), "dd.MM.yyyy"));
            duDescr.setText(mTask.getDescription());

            updateControlPointList();

            // TODO ????
            //if (mRunMap != null) mRunMap.setTaskToMap(mTask);
            mControlPointView.setText(String.valueOf(mTask.getPointNumber()));
        }
    }

    private void updateControlPointList(){

        ControlPointListAdapter cpa = (ControlPointListAdapter) mControlPointList.getAdapter();
        cpa.clear();

        //if( mTask != null ) // ja ir kāds apsekošanas punkts, tad rāda punktus
        cpa.addAll(mTask.getControlPoints());
        cpa.notifyDataSetChanged();
        setListViewHeightBasedOnChildren(mControlPointList);

    }

    // !! this eliminiates ListView problems inside ScrollView, but better is to use RecyclerView
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        if (listAdapter.getCount() == 0) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight() ;
        }

        totalHeight += listAdapter.getCount()*24;

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void showNewTaskDialog(int lastId, String docTemplate) {

        isDialogActive = true;
        FragmentManager fm = getChildFragmentManager();
        NewTaskDialogFragment editNameDialogFragment = NewTaskDialogFragment.newInstance(lastId, docTemplate);
        // SETS the target fragment for use later when sending results
        editNameDialogFragment.setTargetFragment(TaskDetailsFragment.this, 300);
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    @Override
    public void onFinishEditDialog(int surveyType, int newDocId, String taskNr) {

        isDialogActive = false;
        hasChanges = true;
        myApp.preferences.edit().putInt(Constants.LAST_ID, newDocId).commit();
        mTask.setType(surveyType);
        mTask.setTaskNr(taskNr);
        mTask.setStatus(AssignmentDatabaseContract.ASSIGNMENT_IN_PROCESSING);
        mTask.setDate(new Date());
        if(mTask.getId() == -1)
            mTask.insertTask(getContext());
        updateStaticUI();
    }

    @Override
    public void onCancelEditDialog() {
        isDialogActive = false;
        hasChanges = false;
        needToSave = false;
        getActivity().finish();
    }

    @Override
    public void doBack() {
        onAskToCloseDetails();
    }

    // map was created new buffer area - set it to task and screen
//    public void setArea(double area) {
//
//        //if( area > 0.01 ) {
//            if (mTask != null) {
//                mTask.setTrackArea(area);
//
//                if (mDistance != null)
//                    mDistance.setText(String.format("%.2f", mTask.getTrackArea()) + " ha");
//            }
//        //}
//    }

    // loader callbacks ------------------------------------------------------------------------------------------
    private class TaskDetailsLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor>{

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            long taskId = args.getLong(ARG_RUN_ID);
            Timber.i("TaskDetails loader started id=" + String.valueOf(taskId));

            String itemUriStr = "content://"+ AssignmentDatabaseContract.AUTHORITY + "/taskdetails/" +  String.valueOf(taskId);
            Uri itemUri = Uri.parse(itemUriStr);
            cursorLoader = new CursorLoader(getActivity(), itemUri, null, null, null, null);

            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            try {
                mCursor = data;
                Assignment task = Assignment.fromFullCursor(getActivity(), data);
                if(task != null) {
                    mTask = task;
                    checkTaskNumber();
                    initTaskDetails();
                    EventBus.getDefault().post(new TaskDetailsLoaded(mTask));
                }
            }
            catch(CursorIndexOutOfBoundsException ex){
                Timber.e(ex, "TaskDetails loader kļūda");
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    }

    // inicializē Assignment details
    private void initTaskDetails() {

        if( mTask == null ) return;

        updateStaticUI();

        mDistance.setText(String.format("%.2f", mTask.getTrackArea()) + " ha");

        isEditable = mTask.isEditable();
        if(isEditable == false ) {

            mSaveButton.setVisibility(View.INVISIBLE);
            mStartStopButton.setVisibility(View.INVISIBLE);
            mPointButton.setVisibility(View.INVISIBLE);
        }
    }

    private static TrackMapFragment mRunMap;
    public static TaskDetailsFragment newInstance(long runId) {

        Timber.i("Fragment with ID:" + runId);
        TaskDetailsFragment rf = new TaskDetailsFragment();
        mRunMap = new TrackMapFragment();
        if( runId != -1 ) {
            Bundle args = new Bundle();
            args.putLong(ARG_RUN_ID, runId);
            rf.setArguments(args);
            mRunMap.setArguments(args);
        }
        return rf;
    }

    // tracking activities -------------------------------------------------------
    private void toogleTracking() {

        if(started || mTrackManager.isStartRequested()) {
            mTrackManager.stopLocationUpdates();
            started = false;
        }
        else {
            started = true;
            mTrackManager.startTrackingRun(mTask.getId());
            mTask.addPartCounter(); // katrs starts pievieno jaunu poligonu
        }
        setStartStopButton(started);
    }

    private void setStartStopButton(boolean start){


        if( start ) {
            if (mGPSAccuracy != null)
                mGPSAccuracy.setText(getResources().getString(R.string.no_gps_location));
            mStartStopButton.setImageResource(R.drawable.ic_pause_white_48dp);
            mSaveButton.setVisibility(View.INVISIBLE);
        }
        else {
            mStartStopButton.setImageResource(R.drawable.ic_play_arrow_white_48dp);
            mSaveButton.setVisibility(View.VISIBLE);
            if(mGPSAccuracy!=null) mGPSAccuracy.setText("");
        }

    }

    // -------------- bufera aprēķins ir pabeigts ------------------------------------------------

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(BufferSize size) {

        //if (started) {
            double actualArea = size.getArea();
            mTask.setTrackArea(actualArea);
            mTask.setGeometry(size.getWkt());

            BufferSize newSize = new BufferSize(actualArea, "");
            if (mDistance != null)
                mDistance.setText(newSize.getAreaString());
            Timber.i("set area = " + Double.toString(actualArea));
        //}
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(BasicBufferSize area){
//
//        if (mDistance != null)
//            mDistance.setText(area.getAreaString());
//
//        mTask.setTrackArea(area.getArea());
//        Timber.i("set area = " + Double.toString(area.getArea()));
//    }

    // -------------------------------------------------------------------------------------------

    //private Location mLastLocation;
    private boolean locationRequested;
    private BroadcastReceiver mLocationReceiver = new LocationReceiver() {

        @Override
        protected void onLocationReceived(Context context, Location loc) {

            if( loc != null ) {
                if(mRunMap!=null)
                    mRunMap.setLocMarker(null);

                if (mGPSAccuracy != null)
                    mGPSAccuracy.setText(String.format("%.1f m",loc.getAccuracy()));

                if(locationRequested) {

                    if(mRunMap!=null) {
                        mRunMap.setMapPosition(loc);
                        mRunMap.setLocMarker(loc);
                    }

                    //
                    if( !started && !mTrackManager.isStartRequested() ) {
                        mTrackManager.startLocationUpdates();
                        gpsBtn.setImageResource(R.drawable.ic_gps_fixed_white_36dp);
                    }
                    else {
                        locationRequested = false;
                        if(!started) {
                            mTrackManager.stopLocationUpdates();
                        }
                        gpsBtn.setImageResource(R.drawable.ic_gps_not_fixed_white_36dp);
                    }
                }

                // ja nu kas
//                if(loc.getAccuracy() > Constants.GPS_MIN_ACCURACY) return;

                started = true;
                mStartStopButton.setImageResource(R.drawable.ic_pause_white_48dp);

                loc.setBearing(mTask.getCurrentPart());
                mLocations.add(loc);

                hasChanges = true;

                EventBus.getDefault().post(new NewTrackPoint(loc));

                // TODO - tikai šis events palielina laukumu


            }
        }

        @Override
        protected void onProviderEnabledChanged(boolean enabled) {

            int toastText = enabled ? R.string.gps_enabled : R.string.gps_disabled;
            Toast.makeText(getActivity(), toastText, Toast.LENGTH_LONG).show();

            if( enabled == false ) {
//                started = true;
                mStartStopButton.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                if(mGPSAccuracy!=null) mGPSAccuracy.setText("");
                gpsBtn.setImageResource(R.drawable.ic_gps_not_fixed_white_36dp);
            }
            else {
                if(started) {
                    mStartStopButton.setImageResource(R.drawable.ic_pause_white_48dp);
                }
                gpsBtn.setImageResource(R.drawable.ic_gps_fixed_white_36dp);

            }

        }

    };

    // acitivity starp stop handlers ---------------------------------------------------------------------------
    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(mLocationReceiver,
                new IntentFilter(TrackManager.ACTION_LOCATION));
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(mLocationReceiver);
        // probably is necessary to save some params
        super.onStop();
    }

    @Override
    public void onDestroy() {

        //mTrackManager.stopRun(); // šitais apturēs un ierakstīs tracku
        super.onDestroy();
    }

    private void checkTaskNumber() {                        // DEMO versijai principā nevajag, ja klasifikatori ir tuksi
        if( mTask != null && (mTask.getTaskNr() == null)) { //|| mTask.getType() == 0 )){  // noteikti vajag tipu un numuru
            final String nrTemplate = myApp.getUser().getUserDocumentTemplate();
            final int lastDocId = myApp.preferences.getInt(Constants.LAST_ID,1) + 1;
            final int WHAT = 1;
            Handler handler = new Handler(){
                @Override
                public void handleMessage(Message msg) {
                    if(msg.what == WHAT) showNewTaskDialog(lastDocId, nrTemplate);
                }
            };
            handler.sendEmptyMessage(WHAT);
        }
    }

}
