package com.acoms.fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
//import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.acoms.ApplicationController;
import com.acoms.activities.TaskDetailsActivity;
import com.acoms.database.AssignmentDatabaseContract;
import com.acoms.database.ClassifsDatabaseContract;
import com.acoms.koanketa.R;
//import com.acoms.koanketa.SQLiteCursorLoader;
import com.acoms.model.Assignment;
import com.acoms.model.Constants;
import com.acoms.utils.CustomSwipeToRefresh;
import com.acoms.utils.MathUtils;
import com.acoms.utils.StringDateUtils;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.oscim.core.GeoPoint;

import java.util.Date;

import timber.log.Timber;

/**
 * Created by A on 4/14/2016.
 */
public class TaskListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeMenuListView.OnMenuStateChangeListener {

    private static final int REQUEST_NEW_RUN = 0;
    private static final int REQUEST_RUN = 1;
    public static final String USER_ID = "userId";

    private SwipeMenuListView mListView;
    private CustomSwipeToRefresh swipeContainer;
    private Object mContentProviderHandle;
    private FragmentActivity mActivity = null;
    private int mLoaderId = -1;
    private ApplicationController myApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        myApp = (ApplicationController) getActivity().getApplication();

        // initialize the loader to load the list of tasks
//        Bundle args = null;//new Bundle();
//        args.putString(TaskListFragment.USER_ID, myApp.getUser().getObjectId());
        getLoaderManager().initLoader(0, null, this);

        mActivity = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.task_list, null);


            SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                switch (menu.getViewType()) {
                    case 0:
                        // create menu of type 1
                        SwipeMenuItem closeItem = new SwipeMenuItem(getActivity());
                        closeItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,0xCE)));
                        closeItem.setWidth(dp2px(80));
                        // set item title
//                openItem.setTitle("Open");
//                // set item title fontsize
//                openItem.setTitleSize(18);
                        // set item title font color
//                openItem.setTitleColor(Color.WHITE);
                        // set a icon
                        closeItem.setIcon(R.drawable.ic_check_circle_white_24dp);

                        // add to menu
                        menu.addMenuItem(closeItem);
                        break;
                    case 1:
                        // create menu of type 1
                        SwipeMenuItem openItem = new SwipeMenuItem(getActivity());
                        openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,0xCE)));
                        openItem.setWidth(dp2px(80));
                        // set item title
                        openItem.setTitle(getResources().getString(R.string.survey_done));
//                // set item title fontsize
                openItem.setTitleSize(18);
                        // set item title font color
                openItem.setTitleColor(Color.WHITE);
                        // set a icon
//                        openItem.setIcon(R.drawable.ic_check_circle_white_24dp);

                        // add to menu
                        menu.addMenuItem(openItem);
                        break;
                }

                // delete menu is always
                SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                deleteItem.setWidth(dp2px(80));
                deleteItem.setIcon(R.drawable.ic_delete_white_24dp);
                menu.addMenuItem(deleteItem);

                // navigate menu is always
                SwipeMenuItem navigateItem = new SwipeMenuItem(getActivity());
                navigateItem.setBackground(new ColorDrawable(Color.rgb(0x1B, 0x76, 0x9F))); // 1B769F
                navigateItem.setWidth(dp2px(80));
                navigateItem.setIcon(R.drawable.ic_directions_white_24dp);
                menu.addMenuItem(navigateItem);
            }
        };

        mListView = (SwipeMenuListView)view.findViewById(android.R.id.list);
        mListView.setMenuCreator(creator);
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        closeTask(position);
                        break;
                    case 1:
                        deleteTask(position);
                        break;
                    case 2:
                        navigateToTask(position);
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

        // swipeRefresh -----------------------------------------------------------------------------------------
        swipeContainer = (CustomSwipeToRefresh) view.findViewById(R.id.swipeContainer);

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources( R.color.colorPrimaryDark );

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new CustomSwipeToRefresh.OnRefreshListener() {

            @Override
            public void onRefresh() {

                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                //if(mListView.sttisOpen())
                forceSyncTasks();
            }
        });

        // TODO tikai ja pirmais listview ir redzams, tad palaiž refresh
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if(mListView != null && mListView.getChildCount() > 0){
                    // check if the first item of the list is visible
                    boolean firstItemVisible = mListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = mListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipeContainer.setEnabled(enable);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.createNew);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mActivity, TaskDetailsActivity.class);
                i.putExtra(TaskDetailsActivity.EXTRA_RUN_ID, (long)-1);
                startActivityForResult(i, REQUEST_RUN);
            }
        });
        return view;
    }

    private void navigateToTask(int position) {

        TaskCursorAdapter adapter = (TaskCursorAdapter) getListAdapter();
        final Assignment task = adapter.getTaskWithIdAndState(position); // get task with only _id for DB actions

        Location loc = task.getTaskLocation();
        if(loc == null) {
        //if(MathUtils.areFloatsEqual((float)point.getLatitude(),Constants.DEF_LAT) && MathUtils.areFloatsEqual((float)point.getLongitude(), Constants.DEF_LNG)) {
            Toast.makeText(getActivity(), getResources().getString(R.string.new_survey), Toast.LENGTH_SHORT).show();
            return;
        }
        String geoUrl = "geo:0,0?q=" + String.valueOf(loc.getLatitude()) + "," + String.valueOf(loc.getLongitude());
        Timber.i("Navigate to: " + geoUrl);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(geoUrl));
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        mLoaderId = id;

        String selection = AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_EXECUTOR + "=?";
        String[] selectionArgs = {myApp.getUser().getObjectId()};

        CursorLoader cursorLoader = new CursorLoader(getActivity(),
                AssignmentDatabaseContract.CONTENT_URI, AssignmentDatabaseContract.AssignmentTableProjectionList, selection, selectionArgs, AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATE + " ASC");
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        swipeContainer.setRefreshing(false);
        // create an adapter to point at this cursor
        cursor.setNotificationUri(getContext().getContentResolver(), AssignmentDatabaseContract.CONTENT_URI);
        TaskCursorAdapter adapter = new TaskCursorAdapter(getActivity(), cursor);
        setListAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // stop using the cursor (via the adapter)
        //setListAdapter(null);
        swipeContainer.setRefreshing(false);
    }

    public void refreshList(Bundle args) {
        getLoaderManager().restartLoader(mLoaderId, args, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_refresh:
                //forceSyncTasks();
                forceSyncClassifs();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeTask(int position){

        TaskCursorAdapter adapter = (TaskCursorAdapter) getListAdapter();
        final Assignment task = adapter.getTaskWithIdAndState(position); // get task with only _id for DB actions
        if(task != null && task.getStatus() == AssignmentDatabaseContract.ASSIGNMENT_IN_PROCESSING) {

            new AlertDialog.Builder(getActivity())
//                .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.task_close_title)
                    .setMessage(R.string.task_close_title_question)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        int rec = task.setTaskStatus(getActivity(), AssignmentDatabaseContract.ASSIGNMENT_DONE);

                        String msg = getResources().getString(R.string.status_change_error);
                        if(rec>0)
                            msg = getResources().getString(R.string.status_change_ok);
                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();

                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();

        }
        else
            Toast.makeText(getActivity(), getResources().getString(R.string.can_not_close), Toast.LENGTH_SHORT).show();
    }

    private void deleteTask(int position){

        TaskCursorAdapter adapter = (TaskCursorAdapter) getListAdapter();
        final Assignment task = adapter.getTaskWithIdAndState(position); // get task with only _id for DB actions
        //if(task != null && task.getStatus() == AssignmentDatabaseContract.ASSIGNMENT_IN_PROCESSING) {

            new AlertDialog.Builder(getActivity())
//                .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.task_delete_title)
                    .setMessage(R.string.task_delete_question)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            task.deleteTask(getActivity());
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();

        //}
//        else
//            Toast.makeText(getActivity(), "Pabeigt var tikai aktīvu apsekojumu!", Toast.LENGTH_SHORT).show();
    }

    private void forceSyncTasks() {

        // TODO citādi nevar novāk profresu ja nav jaunu ierakstu notifikācija
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                swipeContainer.setRefreshing(false);
            }
        }, 1000);

        ApplicationController myApp = (ApplicationController) getActivity().getApplication();

        ContentResolver.cancelSync(myApp.getConnectedAccount(), AssignmentDatabaseContract.AUTHORITY);
        //ContentResolver.setSyncAutomatically(myApp.getConnectedAccount(), AssignmentDatabaseContract.AUTHORITY, true);

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(myApp.getConnectedAccount(), AssignmentDatabaseContract.AUTHORITY, bundle);
    }

    private void forceSyncClassifs() {

        ApplicationController myApp = (ApplicationController) getActivity().getApplication();

        ContentResolver.cancelSync(myApp.getConnectedAccount(), ClassifsDatabaseContract.AUTHORITY);

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(myApp.getConnectedAccount(), ClassifsDatabaseContract.AUTHORITY, bundle);
    }

    // saraksta dati ir saņemti
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (REQUEST_NEW_RUN == requestCode) {
            // restart the loader to get any new run available
            getLoaderManager().restartLoader(0, null, this);
        }
        else getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(mActivity, TaskDetailsActivity.class);
        i.putExtra(TaskDetailsActivity.EXTRA_RUN_ID, id);
        startActivityForResult(i, REQUEST_RUN);
    }

    @Override
    public void onActivityCreated(Bundle savedState) {

        super.onActivityCreated(savedState);
    }

    @Override
    public void onMenuOpen(int position) {

    }

    @Override
    public void onMenuClose(int position) {

    }

//    @Override
//    public void onStatusChanged(int which) {
//
////        swipeContainer.setRefreshing(false);
//    }

    private static class TaskCursorAdapter extends CursorAdapter {

        //private AssignmentContentProvider.RunCursor mRunCursor;
        private Cursor cursor;

        public TaskCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
            //mRunCursor = cursor;
            this.cursor = cursor;
        }

        @Override
        public int getItemViewType(int position) {
            // current menu type
            Assignment task  = getTaskWithIdAndState(position);

            int type = 0;
            switch (task.getStatus()){

                case AssignmentDatabaseContract.ASSIGNMENT_DONE:
                    type = 1;
                    break;
            }
            return type;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            // use a layout inflater to get a row view
            LayoutInflater inflater =
                    (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            return inflater.inflate(R.layout.track_item, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            String name = cursor.getString(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME));
            long msecsTo = cursor.getLong(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_DATETO));
            int state =  cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE));

            // set up the start date text view
            TextView nameTextView = (TextView)view.findViewById(R.id.workNr);
            nameTextView.setText(name);
            TextView startDateTextView = (TextView)view.findViewById(R.id.workDate);

            Date currentDate = new Date(System.currentTimeMillis());
            Date dateTo = new Date(msecsTo);
            if( currentDate.getTime() > dateTo.getTime() ) {
                startDateTextView.setTextColor(Color.RED);
            }

            startDateTextView.setText(StringDateUtils.getFormatedDate(dateTo, "dd.MM.yyyy"));
            TextView areaTextView = (TextView)view.findViewById(R.id.workArea);

            areaTextView.setText(getStatusString(state));
        }

        public Assignment getTaskWithIdAndState(int position){

            Cursor cursor = getCursor();
            if(cursor.moveToPosition(position)) {

                Assignment task = new Assignment();
                long id = cursor.getLong(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_ID));
                int state = cursor.getInt(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_STATE));
                String name = cursor.getString(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_NAME));
                String geojson = cursor.getString(cursor.getColumnIndex(AssignmentDatabaseContract.AssignmentTable.ASSIGNMENT_TRACK));
                task.setGeometry(geojson);
                task.setName(name);
                task.setId((int)id);
                task.setStatus(state);
                return task;
            }

            return null;
        }
    }

    private static String getStatusString(int state) {

        //getA
        String statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_added);
        switch(state) {

            case AssignmentDatabaseContract.ASSIGNMENT_RECIEVED:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_recieved);
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_REASSIGNED:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_repieted);
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_IN_PROCESSING:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_processing);
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_DONE:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_done);
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_SYNCED:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_synced);
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_CANCELED:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_canceled);
                break;
            case AssignmentDatabaseContract.ASSIGNMENT_CREATED:
                statuss = ApplicationController.getInstance().getResources().getString(R.string.survey_created);
                break;
        }
        return statuss;
    }

    private int dp2px(int dp) {
        float scale = getResources().getDisplayMetrics().density;
        int pixels = (int) (dp * scale + 0.5f);
        return pixels;
    }

    ////////////////////////////
//    @Override
//    public void onPause() {
//        super.onPause();
//        ContentResolver.removeStatusChangeListener(mContentProviderHandle);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mContentProviderHandle = ContentResolver.addStatusChangeListener(
//                ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE, this);
//    }

}
