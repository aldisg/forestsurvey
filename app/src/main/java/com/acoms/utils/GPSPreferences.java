package com.acoms.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.acoms.ApplicationController;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;

/**
 * Created by aldisgrauze on 28.04.2016.
 */
public class GPSPreferences extends DialogPreference {

    private SeekBar seekbarSamplingRate;
    private TextView txtSamplingRateSummary;
    private SeekBar seekbarBitRate;
    private TextView txtBitRateSummary;

    public GPSPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public GPSPreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void onBindDialogView(View view) {
        initViews(view);
        fillViews();
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            SharedPreferences.Editor prefEdit = ApplicationController.preferences.edit();
            prefEdit.putInt(Constants.GPS_FREQ, seekbarSamplingRate.getProgress());
            prefEdit.putInt(Constants.GPS_ACCURACY, seekbarBitRate.getProgress());
            prefEdit.commit();
        }
    }

    private void init(Context context) {
        setPersistent(false);
        setDialogLayoutResource(R.layout.gps_preferences_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }

    private final SeekBar.OnSeekBarChangeListener sliderChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (seekBar.getId() == R.id.seekbar_sampling_rate) {
                //if (fromUser) settings.setSamplingRate(SAMPLE_RATES[progress]);
                txtSamplingRateSummary.setText(String.format(ApplicationController.getInstance().getResources().getString(R.string.pref_gps_secs), progress));
            } else if (seekBar.getId() == R.id.seekbar_bit_rate) {
                //if (fromUser) settings.setBitRate(BIT_RATES[progress]);
                txtBitRateSummary.setText(String.format(ApplicationController.getInstance().getResources().getString(R.string.pref_gps_meters), progress));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    private void initViews(View root) {
        seekbarSamplingRate = (SeekBar) root.findViewById(R.id.seekbar_sampling_rate);
        txtSamplingRateSummary = (TextView) root.findViewById(R.id.txt_sampling_rate_summary);
        seekbarBitRate = (SeekBar) root.findViewById(R.id.seekbar_bit_rate);
        txtBitRateSummary = (TextView) root.findViewById(R.id.txt_bit_rate_summary);

        seekbarSamplingRate.setOnSeekBarChangeListener(sliderChangeListener);
        seekbarSamplingRate.setMax(60);

        seekbarBitRate.setOnSeekBarChangeListener(sliderChangeListener);
        seekbarBitRate.setMax(30);
    }

    private void fillViews() {

        int i = ApplicationController.preferences.getInt(Constants.GPS_FREQ, Constants.GPS_MIN_TIME);
        seekbarSamplingRate.setProgress(i);
        i = ApplicationController.preferences.getInt(Constants.GPS_ACCURACY, Constants.GPS_MIN_DIST);
        seekbarBitRate.setProgress(i);
    }
}