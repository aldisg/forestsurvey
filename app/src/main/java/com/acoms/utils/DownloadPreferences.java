package com.acoms.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.acoms.ApplicationController;
import com.acoms.koanketa.MainActivity;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;

/**
 * Created by aldisgrauze on 23.03.2017.
 */

public class DownloadPreferences extends DialogPreference {

//    private SeekBar seekbarSamplingRate;
    private TextView txtSamplingRateSummary;
//    private SeekBar seekbarBitRate;
    private TextView txtBitRateSummary;

    public DownloadPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public DownloadPreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void onBindDialogView(View view) {
        initViews(view);
        fillViews();
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {

            MainActivity.checkOfflineBasemap(true);

//            SharedPreferences.Editor prefEdit = ApplicationController.preferences.edit();
//            prefEdit.putInt(Constants.GPS_FREQ, seekbarSamplingRate.getProgress());
//            prefEdit.putInt(Constants.GPS_ACCURACY, seekbarBitRate.getProgress());
//            prefEdit.commit();
        }
    }

    private void init(Context context) {
        setPersistent(false);
        setDialogLayoutResource(R.layout.download_preferences_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }


    private void initViews(View root) {

    }

    private void fillViews() {

    }
}