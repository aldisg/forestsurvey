package com.acoms.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.acoms.ApplicationController;
import com.acoms.koanketa.MainActivity;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by aldisgrauze on 28.07.2017.
 */

public class SendReportPreferences extends DialogPreference {

    public SendReportPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public SendReportPreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void onBindDialogView(View view) {
        initViews(view);
        fillViews();
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {

            final String logFilePath = ApplicationController.preferences.getString(Constants.LOG_FILE_PATH, "");
            //Toast.makeText(getContext(), "Sūtam prom no " + logFilePath, Toast.LENGTH_SHORT).show();
            SendAsAttachment(logFilePath, Constants.LOG_LATEST_FILE_NAME, Constants.LOG_RECEIVER);
        }
    }

    private void init(Context context) {
        setPersistent(false);
        setDialogLayoutResource(R.layout.send_report_preferences_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }


    private void initViews(View root) {

    }

    private void fillViews() {

    }

    private void SendAsAttachment(String filePath, String fileName, String reciever){

        DateFormat df = new SimpleDateFormat("dd.mm.yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);

        File filelocation = new File(filePath, fileName);

        Uri path = Uri.fromFile(filelocation);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent .setType("vnd.android.cursor.dir/email");
        String to[] = {reciever};
        emailIntent .putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent .putExtra(Intent.EXTRA_STREAM, path);
        emailIntent .putExtra(Intent.EXTRA_SUBJECT, "VAAD logu žurnāls " + reportDate);
        emailIntent .putExtra(Intent.EXTRA_TEXT, "Pielikumā ir pēdējo LOG ierakstu žurnāls");
        //getContext().startActivity(Intent.createChooser(emailIntent , "Send email..."));
        getContext().startActivity(emailIntent);

    }
}
