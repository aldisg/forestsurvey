package com.acoms.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.acoms.ApplicationController;
import com.acoms.model.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.nio.charset.Charset;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;

import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.StatusPrinter;
import timber.log.Timber;

/**
 * Created by aldisgrauze on 27.07.2017.
 */

public class FileLoggingTree extends Timber.DebugTree {

    private static Logger mLogger = LoggerFactory.getLogger(FileLoggingTree.class);

    public FileLoggingTree(Context context) {

        final String logDirectory = context.getExternalFilesDir(null) + "/logs";
        File folder = new File(logDirectory);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if( success ) {
            final String latestLogName = configureLogger(logDirectory);

            SharedPreferences.Editor prefEdit = ApplicationController.preferences.edit();
            prefEdit.putString(Constants.LOG_FILE_PATH, folder.getAbsolutePath());
            prefEdit.commit();
        }
        else {
            Timber.e("Log file destination is not available!");
        }

    }

    private String configureLogger(String logDirectory) {

        String latestLogName = logDirectory + "/" + Constants.LOG_FILE_NAME + "-latest.csv";

        ch.qos.logback.classic.LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();

        RollingFileAppender<ILoggingEvent> rollingFileAppender = new RollingFileAppender<>();
        rollingFileAppender.setContext(loggerContext);
        rollingFileAppender.setAppend(true);
        rollingFileAppender.setFile(latestLogName);

        SizeAndTimeBasedFNATP<ILoggingEvent> fileNamingPolicy = new SizeAndTimeBasedFNATP<>();
        fileNamingPolicy.setContext(loggerContext);

        ApplicationController.preferences.getString(Constants.LOG_FILE_SIZE, Constants.MAX_LOG_SIZE);
        fileNamingPolicy.setMaxFileSize(Constants.MAX_LOG_SIZE);

        TimeBasedRollingPolicy<ILoggingEvent> rollingPolicy = new TimeBasedRollingPolicy<>();
        rollingPolicy.setContext(loggerContext);
        rollingPolicy.setFileNamePattern(logDirectory + "/" + Constants.LOG_FILE_NAME + ".%d{yyyy-MM-dd}.%i.csv");
        rollingPolicy.setMaxHistory(5);
        rollingPolicy.setTimeBasedFileNamingAndTriggeringPolicy(fileNamingPolicy);
        rollingPolicy.setParent(rollingFileAppender);  // parent and context required!
        rollingPolicy.start();

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(loggerContext);
        encoder.setCharset(Charset.forName("UTF-8"));
        encoder.setPattern("%date %level %msg%n");
        encoder.start();

        rollingFileAppender.setRollingPolicy(rollingPolicy);
        rollingFileAppender.setEncoder(encoder);
        rollingFileAppender.start();

        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.DEBUG);
        root.addAppender(rollingFileAppender);

        StatusPrinter.print(loggerContext);

        return latestLogName;
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.VERBOSE) {
            return;
        }

        String logMessage = tag + ": " + message;
        switch (priority) {
            case Log.DEBUG:
                mLogger.debug(logMessage);
                break;
            case Log.INFO:
                mLogger.info(logMessage);
                break;
            case Log.WARN:
                mLogger.warn(logMessage);
                break;
            case Log.ERROR:
                mLogger.error(logMessage);
                break;
        }
    }
}