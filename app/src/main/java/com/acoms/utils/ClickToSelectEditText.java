package com.acoms.utils;

//


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListAdapter;

/**
 * Created by A on 3/28/2016.
 */
public class ClickToSelectEditText <T> extends AppCompatEditText {

    CharSequence mHint;

    OnItemSelectedListener<T> onItemSelectedListener;
    ListAdapter mSpinnerAdapter;
    private boolean enabled;

    public ClickToSelectEditText(Context context) {
        super(context);

        mHint = getHint();
        enabled = true;
    }

    public ClickToSelectEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mHint = getHint();
        enabled = true;
    }

    public ClickToSelectEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mHint = getHint();
        enabled = true;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setFocusable(false);
        setClickable(enabled);
        setLongClickable(false);
    }

    public void setAdapter(ListAdapter adapter) {
        mSpinnerAdapter = adapter;

        if(enabled)
            configureOnClickListener();
    }

    public void disableInput(){
        this.enabled = false;
    }

    private void configureOnClickListener() {

        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle(mHint);
                builder.setAdapter(mSpinnerAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                        if (onItemSelectedListener != null) {
                            onItemSelectedListener.onItemSelectedListener((T)mSpinnerAdapter.getItem(selectedIndex), selectedIndex);
                        }
                    }
                });
                builder.setPositiveButton(android.R.string.cancel, null);
                builder.create().show();
            }
        });
    }

    public void setOnItemSelectedListener(OnItemSelectedListener<T> onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public interface OnItemSelectedListener<T> {
        void onItemSelectedListener(T item, int selectedIndex);
    }
}