package com.acoms.utils;

import com.acoms.model.Assignment;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aldisgrauze on 17.02.2016.
 */

public class SoapDeserializer {


    public static List<Assignment> parseAssignments(SoapObject data){

        List<Assignment> tasks = new ArrayList<>();
        if(data == null || data.hasProperty("DarbaUzdevumi") == false) return tasks;

        SoapObject works = (SoapObject) data.getProperty("DarbaUzdevumi");
        int length = works.getPropertyCount();

        for (int i = 0; i < length; ++i) {
            SoapObject workSoap = (SoapObject) works.getProperty(i);
            tasks.add(new Assignment(workSoap));
        }
        return tasks;
    }

}

//public class SoapDeserializer<T> implements JsonDeserializer<T> {
//
//    private Class<T> mClass;
//    private String mKey;
//
//    public SoapDeserializer(Class<T> targetClass) {
//        mClass = targetClass;
//        mKey = mClass.getSimpleName();
//    }
//
//    @Override
//    public T deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
//            throws JsonParseException {
//        JsonElement content = je.getAsJsonObject().get(mKey);
//        return new Gson().fromJson(content, mClass);
//
//    }
//}