package com.acoms.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.DialogPreference;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.acoms.ApplicationController;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;

/**
 * Created by aldisgrauze on 23.05.2016.
 */
public class BufferPreferences extends DialogPreference {

//    private SeekBar seekbarBufferSize;
//    private TextView txtBufferSizeSummary;
    private CheckBox checkBoxFill;
    private EditText editBufferSize;

    public BufferPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public BufferPreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void onBindDialogView(View view) {
        initViews(view);
        fillViews();
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {


        if (positiveResult) {

            if(editBufferSize.getError() != null) return;

            SharedPreferences.Editor prefEdit = ApplicationController.preferences.edit();
            prefEdit.putInt(Constants.BUFFER_SIZE_PREF, Integer.parseInt(editBufferSize.getText().toString()));
            prefEdit.putBoolean(Constants.BUFFER_FILL_PREF, checkBoxFill.isChecked());
            prefEdit.commit();
        }
        super.onDialogClosed(positiveResult);
    }

    private void init(Context context) {
        setPersistent(false);
        setDialogLayoutResource(R.layout.buffer_preferences_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }

    private void initViews(View root) {

        editBufferSize = (EditText) root.findViewById(R.id.seekbar_buffer_size);
        editBufferSize.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        editBufferSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value=s.toString();
                editBufferSize.setError(null);
                if(isNumeric(value) ){
                    int bufVal = Integer.parseInt(value);
                    if(bufVal == 0 || bufVal >9999 ){
                        editBufferSize.setError(getContext().getResources().getString(R.string.integer_error));
                    }
                }
                else
                    editBufferSize.setError(getContext().getResources().getString(R.string.integer_error));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //txtBufferSizeSummary = (TextView) root.findViewById(R.id.txt_buffer_size_summary);
        checkBoxFill = (CheckBox) root.findViewById(R.id.ckeckbox_fill_polygon);
    }

    private void fillViews() {

        int i = ApplicationController.preferences.getInt(Constants.BUFFER_SIZE_PREF, Constants.BUFFER_SIZE_M);
        editBufferSize.setText(Integer.toString(i));
        editBufferSize.setSelection(editBufferSize.getText().length());

        boolean bFill = ApplicationController.preferences.getBoolean(Constants.BUFFER_FILL_PREF, false);
        checkBoxFill.setChecked(bFill);
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(.\\d+)?");
    }
}
