package com.acoms.utils;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by A on 3/25/2016.
 */
public class StringIntArrayUtils {

    public static int[] StringToIntArr(String input){

        if(input==null) return new int[0];

        String[] strArray = input.split(";");
        int[] intArray = new int[strArray.length];
        int iRet = 0;
        for(int i = 0; i < strArray.length; i++) {
            if(strArray[i].equals("null") == false && strArray[i].equals("") == false) {
                intArray[i] = Integer.parseInt(strArray[i]);
                iRet++;
            }
        }
        int[] retInt = new int[iRet];
        for(int j = 0; j < iRet; j++)
           retInt[j] = intArray[j];
        return retInt;
    }

    public static <T> boolean contains(final T[] array, final T v) {
        if (v == null) {
            for (final T e : array)
                if (e == null)
                    return true;
        } else {
            for (final T e : array)
                if (e == v || v.equals(e))
                    return true;
        }

        return false;
    }

    public static boolean containsInt(final int[] array, final int v) {

        for (int e : array)
            if (e == v) return true;

        return false;
    }

    public static String join(String join, String... strings) {
        if (strings == null || strings.length == 0) {
            return "";
        } else if (strings.length == 1) {
            return strings[0];
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(strings[0]);
            for (int i = 1; i < strings.length; i++) {
                if( strings[i] != null )
                    sb.append(join).append(strings[i]);
            }
            return sb.toString();
        }
    }

    public static String md5(String s)
    {
        MessageDigest digest;
        try
        {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(Charset.forName("US-ASCII")),0,s.length());
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            String hash = String.format("%0" + (magnitude.length << 1) + "x", bi);
            return hash;
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean findIntInStringArray(int iTest, String[] strArray) {

        if( strArray == null  ) return false;
        for(String sTest:strArray){
            try {
                if (!sTest.isEmpty() && Integer.valueOf(sTest) == iTest) return true;
            }
            catch(NumberFormatException ex){
                ex.printStackTrace();
            }
        }

        return false;
    }
}
