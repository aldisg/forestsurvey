package com.acoms.utils;

/**
 * Created by aldisgrauze on 12.05.2016.
 */
public class MathUtils {

    private static final float delta = 0.00001f;
    private static final int EARTH_RADIUS = 6378137;

    private static final double LATVIA_LAT = 2.6378137;
    private static final double DEC_DEGREE_TO_HA = 676378.69491357834347807589879618;

    public static boolean areFloatsEqual(float f1, float f2){

        if(Math.abs(f1 - f2) < delta) return true;
        return false;
    }

    // angular_units * (PI/180) * 6378137
    public static double DecimalDegreesToMeters(double degrees){

        return degrees * (Math.PI/180) * EARTH_RADIUS;
    }

    public static double MetersToDecimalDegrees(double meters){

        return meters / ((Math.PI/180) * EARTH_RADIUS);
    }

    public static double DecimalDegreesToMetersArea(double area) {

        return area * DEC_DEGREE_TO_HA;// * MetersToDecimalDegrees(1) ;
    }

    public static double MetersToDecimalDegreesArea(double squaremeters){

        //12.2364 E+10 * (180/pi * (sin(f+0.1) - sin(f)) / 0.1)
        return 0.00000000122364 * (180/Math.PI * (Math.sin(LATVIA_LAT + 0.1) - Math.sin(LATVIA_LAT))/0.1);
    }
}
