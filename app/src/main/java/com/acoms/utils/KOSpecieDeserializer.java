package com.acoms.utils;

import com.acoms.model.SpeciesControl;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by aldisgrauze on 17.02.2016.
 */


//public class KOSpecieDeserializer implements JsonDeserializer {
//
//    @Override
//    public SpeciesControl deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
//            throws JsonParseException {
//        final JsonObject jsonObject = json.getAsJsonObject();
//
//        final SpeciesControl author = new SpeciesControl(jsonObject.get("KaitigaisOrganismsID").getAsInt(), jsonObject.get("PetamaisAugsID").getAsInt(), 1);
////        author.setDangerousID(jsonObject.get("KaitigaisOrganismsID").getAsInt());
////        author.setSpeciesID(jsonObject.get("PetamaisAugsID").getAsInt());
//        return author;
//    }
//}

public class KOSpecieDeserializer implements JsonDeserializer<SpeciesControl>
{
    @Override
    public SpeciesControl deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException
    {
        // Get the "content" element from the parsed JSON
        JsonElement content = je.getAsJsonObject().get("KaitigieOrganismi");

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion         // to this deserializer
        return new Gson().fromJson(content, SpeciesControl.class);

    }
}
