package com.acoms.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.acoms.ApplicationController;
import com.acoms.koanketa.R;
import com.acoms.model.Constants;

/**
 * Created by aldisgrauze on 21.08.2017.
 */

public class LogFilePreferences extends DialogPreference {

    private SeekBar seekbarBitRate;
    private TextView txtBitRateSummary;

    public LogFilePreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public LogFilePreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void onBindDialogView(View view) {
        initViews(view);
        fillViews();
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {

            String sMB = Integer.toString(seekbarBitRate.getProgress()) + "MB";
            SharedPreferences.Editor prefEdit = ApplicationController.preferences.edit();
            prefEdit.putString(Constants.LOG_FILE_SIZE, sMB);
            prefEdit.commit();
        }
    }

    private void init(Context context) {
        setPersistent(false);
        setDialogLayoutResource(R.layout.log_file_preferences_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }

    private final SeekBar.OnSeekBarChangeListener sliderChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            if(progress<1)
                seekbarBitRate.setProgress(1);

            progress = seekBar.getProgress();

            if (seekBar.getId() == R.id.seekbar_file_size) {
                txtBitRateSummary.setText(String.format(ApplicationController.getInstance().getResources().getString(R.string.log_file_size), progress));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    private void initViews(View root) {

        seekbarBitRate = (SeekBar) root.findViewById(R.id.seekbar_file_size);
        txtBitRateSummary = (TextView) root.findViewById(R.id.log_file_size);

        seekbarBitRate.setOnSeekBarChangeListener(sliderChangeListener);
        seekbarBitRate.setMax(10);
    }

    private void fillViews() {

        String prefMB =  ApplicationController.preferences.getString(Constants.LOG_FILE_SIZE, Constants.MAX_LOG_SIZE);

        int i = getMBInt(prefMB);
        seekbarBitRate.setProgress(i);

        txtBitRateSummary.setText(String.format(ApplicationController.getInstance().getResources().getString(R.string.log_file_size), i));
    }

    private int getMBInt(String sMB) {

        String sMBValue = sMB.replace("MB", "");
        int i = Integer.parseInt(sMBValue);
        return i;
    }

}