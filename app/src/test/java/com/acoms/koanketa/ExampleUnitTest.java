package com.acoms.koanketa;

import com.acoms.model.Assignment;
import com.acoms.utils.StringIntArrayUtils;
import com.google.gson.Gson;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testSoapService(){
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testHashPassword(){

        String pass = "XXX55";
        String md5 = StringIntArrayUtils.md5(pass);
        assertTrue(md5.equals("73c478f147518d64ab6d2857fbf49479"));
    }

    @Test
    public void testTaskId(){

//        Assignment mTask = new Assignment();
//        //String id = mTask.createTaskNr("333", 5, "AKA" );
//        //String id = mTask.getTaskNrFormatString("333", "AKA" );
//        assertEquals(id.length(), 10);
    }

}