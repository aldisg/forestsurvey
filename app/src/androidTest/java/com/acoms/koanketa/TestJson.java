package com.acoms.koanketa;

/**
 * Created by A on 3/20/2016.
 */
public class TestJson {

    public int intValue;
    public String stringValue;
    public InnerFoo[] innerFoo;

    public class InnerFoo {
        public String name;
    }
}