package com.acoms.koanketa;

import android.accounts.AuthenticatorException;
import android.app.Application;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;

import com.acoms.ApplicationController;
import com.acoms.database.AssignmentDatabaseContract;
import com.acoms.database.ClassifsDAO;
import com.acoms.model.Assignment;
import com.acoms.networkservices.WebServiceSOAP;
import com.acoms.syncadapter.VaadServerAccessor;
import com.cocoahero.android.geojson.GeoJSON;
import com.cocoahero.android.geojson.GeoJSONObject;

import org.ksoap2.serialization.SoapObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    private ApplicationController app;

    public ApplicationTest() {
        super(Application.class);
        app = (ApplicationController) getApplication();
    }

//    @SmallTest
//    public void testPreconditions() {
//        assertNotNull("MainActivity is null", getApplication());
//    }

    @MediumTest
    public void testSoapServices()  {

        VaadServerAccessor server = new VaadServerAccessor();
        String token = null;
        try {
            token = server.getToken("Aldis", "XXX55");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        assertFalse(token.length()==0);

        List<Assignment> tasks = null;
        try {
            tasks = server.getAssignments(token, "2016-01-01");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        assertFalse(tasks.size()==0);

        if(tasks.size() > 0 ) {
            Assignment ass = tasks.get(0);
            GeoJSONObject geom = ass.getGeoJsonGeometry();
            assertNotNull("geom Parse error", geom);
        }
    }

    @MediumTest
    public void testSynchroService()  {

        ApplicationController myApp = app;//(ApplicationController) getActivity().getApplication();
        //ContentResolver.setSyncAutomatically(myApp.getConnectedAccount(), AssignmentDatabaseContract.AUTHORITY, true);
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(myApp.getConnectedAccount(), AssignmentDatabaseContract.AUTHORITY, bundle);
        //ContentResolver.requestSync(myApp.getConnectedAccount(), ClassifsDatabaseContract.AUTHORITY, bundle);

//        VaadServerAccessor server = new VaadServerAccessor();
//        String token = server.getToken("Aldis", "XX55");
//        assertFalse(token.length()==0);
//
//        List<Assignment> tasks = server.getAssignments(token, "2016-01-01");
//        assertFalse(tasks.size()==0);
//
//        // Get shows from local
//        ArrayList<Assignment> localAssignments = new ArrayList<>();
//        Cursor curAssignments = provider.query(AssignmentDatabaseContract.CONTENT_URI, null, null, null, null);
//        if (curAssignments != null) {
//            while (curAssignments.moveToNext()) {
//                localAssignments.add(Assignment.fromCursor(curAssignments));
//            }
//            curAssignments.close();
//        }

    }
//    @MediumTest
//    public void testSoapClassifService()  {
//
//        WebServiceSOAP service = new WebServiceSOAP();
//        SoapObject obj = service.getObject("AtvertSesiju");
//
//        assertNotNull(obj);
//    }

//    @MediumTest
//    public void testSomething()  {
//
//        VaadServerAccessor server = new VaadServerAccessor();
//        String resptext = server.getClassifics(app);
//        assertEquals("Klasifikatori nolasīti", resptext);
//    }
}