package com.acoms.koanketa;

import android.content.Context;
import android.test.AndroidTestCase;

import com.acoms.database.ClassifsDAO;
import com.acoms.model.Classifs;
import com.acoms.model.DangerousSpecie;
import com.acoms.model.Specie;

import java.util.List;

/**
 * Created by A on 3/19/2016.
 */
public class ClassificParseTest extends AndroidTestCase {

    Context context;

    public void setUp() throws Exception {
        super.setUp();

        //context = new MockContext();
        context = getContext().createPackageContext("com.acoms.koanketa",
                Context.CONTEXT_IGNORE_SECURITY);

        setContext(context);

        assertNotNull(context);
    }

    // Fake failed test
    public void testSomething()  {

        Classifs classifs = ClassifsDAO.getInstance(context).getTestSpecies(context, "vaad_soap_new.json");
        List<Specie> dta = classifs.getSpeciesList();
        Specie r = dta.get(0);
        assertNotNull("Tukšs kaitigo organismu saraksts", r.speciesControlList);

        List<DangerousSpecie> dsl = classifs.getDangerousSpeciesList();
        DangerousSpecie s = dsl.get(1);
        //assertFalse("Tukšs kaitigo organismu helps", s.getDescriptionHtml().length()==0);


    }



}
